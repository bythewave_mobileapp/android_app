package com.bythewave.rideplus.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.enums.Sport;

import org.joda.time.DateTime;


/**
 * fr.reseauscet.scet_app_android.manager.PreferencesManager
 * <p/>
 * Singleton gérant les préférences partagées {@link SharedPreferences}.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 19/01/2018
 */
public class PreferencesManager {

    private static PreferencesManager mInstance;
    private static SharedPreferences prefs;

    public static PreferencesManager getInstance() {
        if (mInstance == null) {
            mInstance = new PreferencesManager(RidePlusApplication.applicationContext);
        }
        return mInstance;
    }

    private PreferencesManager(Context context) {
        if (context != null) {
            prefs = context.getSharedPreferences(Constants.PREFERENCES_ID, Context.MODE_PRIVATE);
        }
    }

    /**
     * @return vrai si le tutorial doit être affiché, faux sinon
     */
    public boolean shouldDisplayTutorial() {
        return prefs.getBoolean(Constants.PREF_DISPLAY_TUTORIAL, true);
    }

    /**
     * Fixe que le tutorial a été vu
     */
    public void setTutorialDisplayed() {
        prefs.edit().putBoolean(Constants.PREF_DISPLAY_TUTORIAL, false).apply();
    }


    /**
     * Renvoi la date de premier lancement de l'app
     *
     * @return la date de premier lancement
     */
    public DateTime getFirstLaunchDate() {
        return DateTime.parse(prefs.getString(Constants.PREF_1ST_LAUNCH_DATE, null));
    }

    /**
     * Fixe la date de premier lancement de l'app
     */
    public void setFirstLaunchDate() {
        String dateString = prefs.getString(Constants.PREF_1ST_LAUNCH_DATE, null);
        if (dateString == null) {
            prefs.edit().putString(Constants.PREF_1ST_LAUNCH_DATE, AndroidUtils.today().toString()).apply();
        }
    }

    /**
     * Renvoi la version locale du fichier de configuration
     *
     * @return la version locale du fichier de configuration
     */
    public String getConfigVersion() {
        return prefs.getString(Constants.PREF_LOCAL_CONFIG_VERSION, "");
    }

    /**
     * Fixe la version locale du fichier de configuration
     *
     * @param version la version locale du fichier de configuration
     */
    public void setConfigVersion(String version) {
        prefs.edit().putString(Constants.PREF_LOCAL_CONFIG_VERSION, version).apply();
    }

    /**
     * Renvoi le sport par défaut, Surf si pas de sport
     *
     * @return le sport par défaut
     */
    public Sport getDefaultSport() {
        return Sport.fromString(prefs.getString(Constants.PREF_DEFAULT_SPORT, Sport.SURF.value()));
    }

    /**
     * Fixe le sport par défaut
     *
     * @param currentSport le nouveau sport par défaut
     */
    public void setDefaultSport(Sport currentSport) {
        if (currentSport != null) {
            prefs.edit().putString(Constants.PREF_DEFAULT_SPORT, currentSport.value()).apply();
        }
    }


    /**
     * Indique si il faut migrer les anciennes sessions de l'utilisateur
     *
     * @return vrai si il faut migrer les anciennes sessions de l'utilisateur
     */
    public boolean shouldMigrateOldSession() {
        return prefs.getBoolean(Constants.PREF_SHOULD_MIGRATE_OLD_SESSIONS, true);
    }

    /**
     * Indique si il faut migrer les anciennes sessions de l'utilisateur
     *
     * @return vrai si il faut migrer les anciennes sessions de l'utilisateur
     */
    public void setOldSessionsMigrated() {
        prefs.edit().putBoolean(Constants.PREF_SHOULD_MIGRATE_OLD_SESSIONS, false).apply();
    }
}
