package com.bythewave.rideplus.manager;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Device;
import com.bythewave.rideplus.models.User;
import com.bythewave.rideplus.models.UserDeviceJoin;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.models.enums.Sport;
import com.bythewave.rideplus.models.enums.Stance;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.MetadataChanges;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

/**
 * com.by_the_wave.ride_plus_android.manager
 * <p/>
 * Classe simplifiant la gestion de l'utilisateur courrant
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 25/01/2018
 */
public class CurrentUserManager {

    private static CurrentUserManager instance;
    private String currentUserId;
    private User currentUser;
    private ArrayList<Device> devices = new ArrayList<>();


    public static CurrentUserManager getInstance() {
        if (instance == null) {
            instance = new CurrentUserManager();
        }
        return instance;
    }

    private CurrentUserManager() {
        reload();
    }


    /**
     * Renvoi vrai si l'utilisateur courant est loggué
     *
     * @return vrai si utilisateur courant, faux sinon
     */
    public boolean hasCurrentUser() {
        return currentUserId != null && currentUser != null;
    }

    /**
     * Renvoi l'ID local de l'utilisateur courant
     *
     * @return l'ID local de l'utilisateur courant
     */
    public String getCurrentUserId() {
        if (TextUtils.isEmpty(currentUserId))
            return "";
        else
            return currentUserId;
    }

    /**
     * Renvoi l'utilisateur courant
     *
     * @return l'utilisateur courant
     */
    public User getCurrentUser() {
        return currentUser;
    }


    /**
     * Efface les préférences stockées sur l'utilisateur courant
     */
    public void logout() {
        //on supprime les références du manager
        currentUserId = null;
        currentUser = null;


        FirebaseAuth.getInstance().signOut();

        BusMessage.post(BusMessageType.USER_LOGGED_OUT);
    }

    /**
     * Créé un nouvel utilisateur et le persiste sur la base distante
     */
    public void createUser() {
        //on récupère les infos de connexion de firebase auth
        FirebaseUser authUser = FirebaseAuth.getInstance().getCurrentUser();
        if (authUser != null) {
            currentUserId = authUser.getUid();
            currentUser = new User();
            if (TextUtils.isEmpty(authUser.getDisplayName()))
                currentUser.name = authUser.getEmail();
            else
                currentUser.name = authUser.getDisplayName();
            if (authUser.getPhotoUrl() != null)
                currentUser.avatarUrl = authUser.getPhotoUrl().toString();
            currentUser.stance = Stance.REGULAR;
            currentUser.id = authUser.getUid();

            //on sauve l'utilisateur sur la base
            save(BusMessageType.USER_CURRENT_SAVED, currentUser);

            //on ajoute une session de test pour chaque sport
            for (Sport sport : Sport.values())
                RidePlusApplication.sessionManager.addExampleSession(sport);
        }
    }

    /**
     * Persiste l'utilisateur courant sur la base distante
     *
     * @param successMessage le type de message à envoyer sur le bus système en cas de succès de la sauvegarde de l'utilisateur
     *                       cela permet finement gérer les ajouts / suppression / modification de device
     * @param successObject  l'objet à renvoyer avec le message en paramètre en cas de succès
     */
    public void save(BusMessageType successMessage, @Nullable Object successObject) {
        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_USERS)
                .document(currentUserId)
                .set(currentUser)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        BusMessage.post(successMessage, successObject);
                    } else {
                        KLog.e(task.getException());
                        BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                    }
                });
    }

    /**
     * Persiste l'appareil en paramètre sur la base distante
     *
     * @param device l'appareil à persister
     */
    private void saveDevice(Device device) {
        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_DEVICES)
                .document(device.serial)
                .set(device)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        BusMessage.post(BusMessageType.DEVICE_UPDATED, device);
                    } else {
                        KLog.e(task.getException());
                        BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                    }
                });

    }

    /**
     * Récupère l'utilisateur courant depuis la base distante
     */
    private void downloadCurrentUser() {
        if (currentUserId != null) {
            KLog.w(FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_USERS).document(currentUserId).getPath());
            DocumentReference currentUserReference = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_USERS).document(currentUserId);
            currentUserReference.get()
                    .addOnCompleteListener(getDocumentTask -> {
                        if (getDocumentTask.isSuccessful()) {
                            DocumentSnapshot document = getDocumentTask.getResult();
                            if (document != null && document.exists()) {
                                currentUser = document.toObject(User.class);
                                //on migre ses anciennes sessions si nécessaire
                                RidePlusApplication.sessionManager.migrateOldSession();
                                BusMessage.post(BusMessageType.USER_CURRENT_UPDATED, currentUser);
                            } else {
                                createUser();
                            }
                        } else {
                            KLog.e(getDocumentTask.getException());
                            if (getDocumentTask.getException() instanceof FirebaseNetworkException) {
                                BusMessage.post(BusMessageType.ERROR_OFFLINE);
                            } else if (getDocumentTask.getException() instanceof FirebaseFirestoreException) {
                                BusMessage.post(BusMessageType.ERROR_GENERIC_AUTH);
                            } else {
                                BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                            }
                        }
                    });
            //on ajoute un listener pour épier ses changements d'états
            currentUserReference.addSnapshotListener(MetadataChanges.INCLUDE, (snapshot, exception) -> {
                if (exception != null) {
                    KLog.w(exception);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    currentUser = snapshot.toObject(User.class);
                    BusMessage.post(snapshot.getMetadata().hasPendingWrites() && !AndroidUtils.isNetworkAvailable() ? BusMessageType.USER_CURRENT_LOCALLY_UPDATED : BusMessageType.USER_CURRENT_UPDATED);
                }

            });

        }
    }

    /**
     * Récupère l'utilisateur courant depuis la base distante
     */
    private void downloadCurrentUserDevices() {
        devices.clear();
        if (currentUserId != null) {
            //On récupère la liste des appareils de l'utilisateur
            FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_USER_DEVICE_JOINS)
                    .whereEqualTo(Constants.FIELD_USER_ID, currentUserId)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot joinDocument : Objects.requireNonNull(task.getResult())) {
                                if (joinDocument != null && joinDocument.exists()) {
                                    UserDeviceJoin join = joinDocument.toObject(UserDeviceJoin.class);

                                    if (join != null && join.deviceId != null) {
                                        //on récupère l'id de boitier de la jointure
                                        DocumentReference deviceReference = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_DEVICES).document(join.deviceId);
                                        deviceReference.get()
                                                .addOnCompleteListener(getDocumentTask -> {
                                                    if (getDocumentTask.isSuccessful()) {
                                                        DocumentSnapshot document = getDocumentTask.getResult();
                                                        if (document != null && document.exists()) {
                                                            Device newDevice = document.toObject(Device.class);
                                                            if (!hasDevice(newDevice))
                                                                devices.add(newDevice);
                                                            BusMessage.post(BusMessageType.DEVICE_UPDATED, currentUser);
                                                        }
                                                    } else {
                                                        KLog.e(getDocumentTask.getException());
                                                        if (getDocumentTask.getException() instanceof FirebaseNetworkException) {
                                                            BusMessage.post(BusMessageType.ERROR_OFFLINE);
                                                        } else if (getDocumentTask.getException() instanceof FirebaseFirestoreException) {
                                                            BusMessage.post(BusMessageType.ERROR_GENERIC_AUTH);
                                                        } else {
                                                            BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                                                        }
                                                    }
                                                });

                                        //on ajoute un listener pour épier ses changements d'états sur l'appareil
                                        deviceReference.addSnapshotListener(MetadataChanges.INCLUDE, (snapshot, exception) -> {
                                            if (exception != null) {
                                                KLog.w(exception);
                                                return;
                                            }

                                            if (snapshot != null && snapshot.exists()) {
                                                Device updatedDevice = snapshot.toObject(Device.class);
                                                if (updatedDevice != null) {
                                                    //updating the device list
                                                    for (int index = 0; index < devices.size(); index++) {
                                                        if (devices.get(index).serial.equals(updatedDevice.serial)) {
                                                            Device existingDevice = devices.get(index);
                                                            existingDevice.lastSyncAt = updatedDevice.lastSyncAt;
                                                            existingDevice.batteryLevel = updatedDevice.batteryLevel;
                                                            existingDevice.customName = updatedDevice.customName;
                                                            existingDevice.sport = updatedDevice.sport;
                                                            existingDevice.firmwareRev = updatedDevice.firmwareRev;
                                                            break;
                                                        }
                                                    }
                                                    BusMessage.post(snapshot.getMetadata().hasPendingWrites() && !AndroidUtils.isNetworkAvailable() ? BusMessageType.DEVICE_LOCALLY_UPDATED : BusMessageType.DEVICE_UPDATED);
                                                }
                                            }

                                        });

                                    }
                                }
                            }
                        }
                    });

        }
    }

    /**
     * Ajoute un appareil à l'utilisateur courant
     *
     * @param device l'appareil à ajouter
     */
    public void addDevice(Device device) {
        if (currentUser != null && device != null && device.serial != null) {
            if (!hasDevice(device)) {
                //on vérifie si l'appareil existe déjà
                FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_DEVICES)
                        .document(device.serial).get().addOnCompleteListener(getDocumentTask -> {
                    if (getDocumentTask.isSuccessful()) {
                        DocumentSnapshot document = getDocumentTask.getResult();
                        if (document == null || !document.exists()) {
                            devices.add(device);
                            //on ajoute l'appareil en bdd
                            saveDevice(device);
                            //on ajoute la relation
                            addDeviceUserJoin(device);
                        } else if (document.exists()) {
                            Device newDevice = document.toObject(Device.class);
                            if (newDevice != null) {
                                devices.add(newDevice);
                                //on ajoute la relation
                                addDeviceUserJoin(newDevice);
                            }
                        }
                    }
                }).addOnFailureListener(e -> {
                    devices.add(device);
                    //on ajoute l'appareil en bdd
                    saveDevice(device);
                    //on ajoute la relation
                    addDeviceUserJoin(device);
                });

            }
        }
    }

    /**
     * Ajoute une relation entre un utilisateur et un appareil.
     *
     * @param device l'appareil à ajouter à une relation
     */
    private void addDeviceUserJoin(Device device) {
        //on ajoute une relation entre l'utilisateur et l'appareil
        UserDeviceJoin newJoin = new UserDeviceJoin(currentUserId, device.serial);
        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_USER_DEVICE_JOINS)
                .document(UUID.randomUUID().toString())
                .set(newJoin)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        BusMessage.post(BusMessageType.DEVICE_ADDED, device);
                        // Firebase log
                        RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_DEVICE_ADDED, null);
                    } else {
                        KLog.e(task.getException());
                        BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                    }
                });
    }


    /**
     * Modifie l'appareil en paramètre dans la liste des appareils de l'utilisateur
     *
     * @param device l'appareil à modifier
     */
    public void setDevice(Device device) {
        if (currentUser != null && device != null && device.serial != null) {
            //on parcours la liste des appareil pour vérifier que l'appareil n'a pas déjà été ajouté
            for (Device existingDevice : devices) {
                if (existingDevice != null && existingDevice.serial != null && existingDevice.serial.equals(device.serial)) {
                    existingDevice.lastSyncAt = device.lastSyncAt;
                    existingDevice.batteryLevel = device.batteryLevel;
                    existingDevice.customName = device.customName;
                    existingDevice.sport = device.sport;
                    existingDevice.firmwareRev = device.firmwareRev;
                    saveDevice(existingDevice);
                    break;
                }
            }
        }
    }

    /**
     * Supprime l'appareil dont le numéro de série est fourni en paramètre
     */
    public void deleteDevice(Device device) {
        if (currentUser != null && device != null && device.serial != null) {
            int indexToDelete = -1;
            for (int index = 0; index < devices.size(); index++) {
                if (devices.get(index) != null && device.serial.equals(devices.get(index).serial)) {
                    indexToDelete = index;
                    break;
                }
            }
            if (indexToDelete > -1) {
                devices.remove(indexToDelete);

                //on supprime seulement la relation entre l'appareil et l'utilisateur
                FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_USER_DEVICE_JOINS)
                        .whereEqualTo(Constants.FIELD_USER_ID, currentUserId)
                        .get()
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                for (DocumentSnapshot joinDocument : Objects.requireNonNull(task.getResult())) {
                                    if (joinDocument != null && joinDocument.exists()) {
                                        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_USER_DEVICE_JOINS).document(joinDocument.getId()).delete().addOnCompleteListener(firestoreDeleteTask -> {
                                            // Firebase log
                                            RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_DEVICE_DELETED, null);
                                            BusMessage.post(BusMessageType.DEVICE_DELETED);
                                        }).addOnCanceledListener(() -> {
                                            KLog.e(task.getException());
                                            BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                                        });
                                    }
                                }
                            }
                        });

                BusMessage.post(BusMessageType.DEVICE_DELETED);

            }
        }
    }

    /**
     * Indique si l'utilisateur courant a des appareils enregistrés
     *
     * @return vrai si l'utilisateur courant a des appareils enregistrés, faux sinon
     */
    public boolean hasDevices() {
        return currentUser != null && !devices.isEmpty();
    }

    /**
     * Supprime le compte de l'utilisateur et toutes ses données
     */
    public void deleteUser() {
        // TODO supprimer l'utilisateur sitôt que les cloud function permettront de faire des requêtes sur les bases Firestore
        // on indique que l'utilisateur est à supprimer
        currentUser.toDelete = true;
        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_USERS)
                .document(currentUserId)
                .set(currentUser).addOnCompleteListener(firestoreDeleteTask -> {
            if (firestoreDeleteTask.isSuccessful()) {
                //on supprime en premier l'utilisateur sur fireauth
                FirebaseUser fireAuthUser = FirebaseAuth.getInstance().getCurrentUser();
                if (fireAuthUser != null) {
                    fireAuthUser.delete().addOnCompleteListener(fireAuthDeleteTask -> {
                        if (fireAuthDeleteTask.isSuccessful()) {
                            // Firebase log
                            RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_PROFILE_DELETED, null);

                            logout();
                        } else {
                            KLog.e(fireAuthDeleteTask.getException());
                            BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                        }
                    });
                }
            } else {
                KLog.e(firestoreDeleteTask.getException());
                BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
            }
        });


    }

    /**
     * Recharche l'utilisateur courant
     */
    public void reload() {
        FirebaseUser authUser = FirebaseAuth.getInstance().getCurrentUser();
        if (authUser != null) {
            currentUserId = authUser.getUid();
            downloadCurrentUser();
            downloadCurrentUserDevices();
        }
    }

    /**
     * Indique si l'utilisateur a déjà ajouté l'appareil en paramètre à ses appareils
     *
     * @param device l'appareil à potentiellement ajouter
     * @return vrai si l'utilisateur a déjà l'appareil en paramètre, faux sinon
     */
    boolean hasDevice(Device device) {
        if (devices != null && device != null && device.macAdress != null) {
            for (int index = 0; index < devices.size(); index++) {
                if (devices.get(index) != null && device.macAdress.equals(devices.get(index).macAdress)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * @return la liste des appareils de l'utilisateurs
     */
    public ArrayList<Device> getCurrentUserDevices() {
        return devices;
    }

}
