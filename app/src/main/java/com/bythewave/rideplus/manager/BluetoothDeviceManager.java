package com.bythewave.rideplus.manager;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.text.TextUtils;

import com.bythewave.rideplus.BuildConfig;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Device;
import com.bythewave.rideplus.models.Session;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.polidea.rxandroidble.NotificationSetupMode;
import com.polidea.rxandroidble.RxBleConnection;
import com.polidea.rxandroidble.RxBleDevice;
import com.polidea.rxandroidble.exceptions.BleAlreadyConnectedException;
import com.polidea.rxandroidble.exceptions.BleCannotSetCharacteristicNotificationException;
import com.polidea.rxandroidble.exceptions.BleCharacteristicNotFoundException;
import com.polidea.rxandroidble.exceptions.BleConflictingNotificationAlreadySetException;
import com.polidea.rxandroidble.exceptions.BleDisconnectedException;
import com.polidea.rxandroidble.exceptions.BleException;
import com.polidea.rxandroidble.exceptions.BleGattCallbackTimeoutException;
import com.polidea.rxandroidble.exceptions.BleGattCannotStartException;
import com.polidea.rxandroidble.exceptions.BleGattCharacteristicException;
import com.polidea.rxandroidble.exceptions.BleGattDescriptorException;
import com.polidea.rxandroidble.exceptions.BleGattException;
import com.polidea.rxandroidble.exceptions.BleScanException;
import com.polidea.rxandroidble.exceptions.BleServiceNotFoundException;
import com.polidea.rxandroidble.scan.ScanFilter;
import com.polidea.rxandroidble.scan.ScanSettings;
import com.socks.library.KLog;

import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rx.Subscription;


/**
 * com.by_the_wave.ride_plus_android.manager.DevicesManager
 * <p/>
 * Singleton gérant les interactions via Bluetooth avec les appareils
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 31/01/2018
 */
public class BluetoothDeviceManager {

    private static final int MESSAGE_TIMEOUT = 999;
    private static final int MESSAGE_STATE = 998;

    // souscription RxJava
    private Subscription scanSubscription;
    private Subscription connectionSubscription;
    private Subscription connectionStateSubscription;
    private RxBleConnection connection;
    private StringBuffer response;
    private Device device;
    private boolean deviceReady; // indique si l'appareil peut répondre aux commandes envoyées

    private static BluetoothDeviceManager instance;

    // contrôle de réception des notifications
    private LinkedList<String> commandsQueue;
    private Handler notificationHandler;
    private int retryCount;

    /**
     * Constructeur vide
     */
    @SuppressLint("HandlerLeak")
    public BluetoothDeviceManager() {
        commandsQueue = new LinkedList<>();

        retryCount = 0;

        //on créé un timer ou purge le timer existant
        notificationHandler = new Handler(message -> {
            if (message.what == MESSAGE_TIMEOUT) {
                if (retryCount < Constants.MAX_RETRY) {
                    retryCount++;
                    sendCommand(null);
                } else {
                    commandsQueue.clear();
                    retryCount = 0;
                    KLog.e("ble", "TIME OUT BLE");
                    BusMessage.post(BusMessageType.ERROR_BLE_COMMAND_TIMEOUT);
                    return true;
                }
            } else if (message.what == MESSAGE_STATE) {
                getDeviceState();
            }
            return false;
        });
    }

    public static BluetoothDeviceManager getInstance() {
        if (instance == null) {
            instance = new BluetoothDeviceManager();
        }
        return instance;
    }

    /**
     * Indique si une recherche d'appareil BLe est en cours
     *
     * @return vrai si une recherche est en cours, faux sinon
     */
    public boolean isScanning() {
        return scanSubscription != null && !scanSubscription.isUnsubscribed();
    }

    /**
     * Renvoi l'état de la connexion avec l'appareil en paramètre
     *
     * @param device l'appareil connecté
     * @return vrai si l'appareil est connecté,faux sinon
     */
    public RxBleConnection.RxBleConnectionState getConnectionState(Device device) {
        if (device != null)
            if (BuildConfig.BUILD_TYPE.equals("simulator")) {
                return RxBleConnection.RxBleConnectionState.DISCONNECTED;
            } else {
                if (!TextUtils.isEmpty(device.macAdress)) {
                    if (RidePlusApplication.rxBleClient.getBleDevice(device.macAdress).getConnectionState().equals(RxBleConnection.RxBleConnectionState.CONNECTED) && !deviceReady) {
                        return RxBleConnection.RxBleConnectionState.DISCONNECTED;
                    } else {
                        return RidePlusApplication.rxBleClient.getBleDevice(device.macAdress).getConnectionState();
                    }
                } else
                    return RxBleConnection.RxBleConnectionState.DISCONNECTED;
            }
        else
            return null;
    }


    /**
     * Scanne les appareils BLE à proximité et ajoute l'appareil BLE correspondant à un produit
     * RidePlus aux appareils de l'utilisateur courant ou se connecte à un appareil enregistré
     *
     * @param searched_device l'appareil auquel se connecter
     */
    public void startScan(Device searched_device) {
        cancelSubscriptions();

        ScanFilter scanFilter;
        if (searched_device != null && !TextUtils.isEmpty(searched_device.macAdress))
            scanFilter = new ScanFilter.Builder().setDeviceAddress(searched_device.macAdress).build();
        else
            scanFilter = new ScanFilter.Builder().setDeviceName(Constants.DEFAULT_DEVICE_NAME).build();

        KLog.d("ble", "Start scan");
        // on cherche uniquement les appareils dont le nom est celui d'un appareil RidePlus
        scanSubscription = RidePlusApplication.rxBleClient
                .scanBleDevices(
                        new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_BALANCED).build(),
                        scanFilter)
                .subscribe(scanResult -> {
                    KLog.d("ble", "Scan hooked a device");
                    RxBleDevice bleDevice = scanResult.getBleDevice();
                    if (searched_device == null)
                        this.device = new Device(bleDevice.getMacAddress());
                    else
                        this.device = searched_device;
                    // on récupère des informations supplémentaire et on l'ajoute
                    connectionSubscription = bleDevice.establishConnection(false).doOnNext(connection -> {
                        this.connection = connection;

                        setConnectionStateChangeListener(bleDevice);
                    }).flatMap(RxBleConnection::discoverServices)
                            .subscribe(services -> {
                                //on sette les notifications sans envoyer de commandes
                                setupNotifications();

                            }, throwable -> {
                                KLog.e("error discovering services");
                                handleBleException(throwable);
                            });
                    scanSubscription.unsubscribe();
                }, throwable -> {
                    KLog.e("ble", "error scan for device");
                    handleBleException(throwable);
                });
    }


    /**
     * Arrête la recherche d'appareil
     */
    public void cancelSubscriptions() {
        KLog.d("Cancel subscription");
        if (connectionSubscription != null) connectionSubscription.unsubscribe();
        if (scanSubscription != null) scanSubscription.unsubscribe();
        if (connectionStateSubscription != null) connectionStateSubscription.unsubscribe();
        if (connection != null) connection = null;

        clearCommandsQueue();

        device = null;
        deviceReady = false;
    }

    /**
     * Reinitialise
     */
    private void clearCommandsQueue() {
        //on purge la liste des commandes en attente
        commandsQueue.clear();
        retryCount = 0;
        //on purge le handler gérant les timeouts
        if (notificationHandler != null)
            notificationHandler.removeMessages(MESSAGE_TIMEOUT);
    }


    /**
     * Récupère l'état courant du boitier
     */
    private void getDeviceState() {
        KLog.d("ble", "get STATE");
        sendCommand(Constants.COMMAND_GET_STATE);
    }


    /**
     * Récupère les sessions de l'appareil connecté
     */
    public void getSessions() {
        KLog.d("ble", "get SESSIONS");
        sendCommand(Constants.COMMAND_GET_DATA);
    }

    /**
     * Efface les sessions sur l'appareil connecté
     */
    private void clearSessions() {
        KLog.d("ble", "CLEAR SESSIONS");
        sendCommand(Constants.COMMAND_CLEAR_DATA);
    }


    /**
     * Paramètre la réception de notifications sur la characteristic DATA
     */
    @SuppressLint("CheckResult")
    private void setupNotifications() {
        KLog.d("ble", "Setup notification");
        // on active les notifications sur le champ data pour recevoir les données
        connection.setupNotification(Constants.UUID_CHARACTERISTIC_DATA, NotificationSetupMode.QUICK_SETUP)
                .doOnNext(notificationObservable -> {
                    KLog.d("ble", "notification set");

                    clearCommandsQueue();

                    // on récupère l'état du boitier via le notificationhandler pour défèrer de quelques ms l'envoi de la commande
                    notificationHandler.sendMessageDelayed(notificationHandler.obtainMessage(MESSAGE_STATE), Constants.DEFAULT_BT_DELAY);

                })
                .flatMap(notificationObservable -> notificationObservable)
                .subscribe(bytes -> {
                            //on purge le handler gérant les timeouts
                            notificationHandler.removeMessages(MESSAGE_TIMEOUT);

                            //on extrait le texte des données brutes
                            String chunk = new String(bytes);
                            if (response != null)
                                response.append(chunk);
                            //si la chaine contient un \n, on analyse le message complet
                            if (chunk.endsWith("\n")) {
                                parseResponse();
                                response = new StringBuffer();
                            }
                        },
                        throwable -> {
                            KLog.e("ble", "error setup notification for device");
                            handleBleException(throwable);
                        }
                );

    }


    /**
     * Envoi la commande en paramètre à l'appareil.
     * Si une commande est déjà en cours d'envoi et en attente de réponse, l'envoi est mis en attente
     *
     * @param command la commande à envoyer
     */
    @SuppressLint("CheckResult")
    private void sendCommand(String command) {

        if (command != null) {
            //on ajoute la commande à la liste des commandes à envoyer
            commandsQueue.offer(command);
        }

        //si aucune commande n'est en cours d'envoi, on continue
        if (response == null || response.length() == 0 && !commandsQueue.isEmpty() && notificationHandler != null && !notificationHandler.hasMessages(MESSAGE_TIMEOUT)) {
            if (connection != null) {
                //on lance le timer
                notificationHandler.sendMessageDelayed(notificationHandler.obtainMessage(MESSAGE_TIMEOUT), Constants.TIMEOUT_BLE_COMMAND);

                response = new StringBuffer();

//                if (BuildConfig.BUILD_TYPE.equals("debug"))
//                    if (command != null && command.equals(Constants.COMMAND_GET_DATA))
//                        response.append("<DATA>{\"sessions\":[{\"date\":1518862840,\"lat\":43.697577,\"lon\":-1.439639,\"duration\":1320,\"waves\":[{\"direction\":\"left\",\"date\":1518862840,\"lat\":43.6974,\"lon\":-1.44042,\"level\":2,\"distance\":43.00,\"speed\":5.4,\"duration\":8.94,\"moments\":[{\"lat\":43.6974,\"lon\":-1.44042,\"type\":\"T\"},{\"lat\":43.69741,\"lon\":-1.44021,\"type\":\"R\"},{\"lat\":43.6975,\"lon\":-1.44018,\"type\":\"R\"},{\"lat\":43.69749,\"lon\":-1.44014,\"type\":\"R\"},{\"lat\":43.69756,\"lon\":-1.44009,\"type\":\"R\"},{\"lat\":43.69762,\"lon\":-1.44001,\"type\":\"F\"}]},{\"direction\":\"right\",\"date\":1518862870,\"lat\":43.69718,\"lon\":-1.44044,\"level\":3,\"distance\":23.00,\"speed\":3.83,\"duration\":6.94,\"moments\":[{\"lat\":43.69718,\"lon\":-1.44044,\"type\":\"T\"},{\"lat\":43.69727,\"lon\":-1.44031,\"type\":\"R\"},{\"lat\":43.69734,\"lon\":-1.44028,\"type\":\"R\"},{\"lat\":43.69736,\"lon\":-1.44022,\"type\":\"R\"},{\"lat\":43.69742,\"lon\":-1.44018,\"type\":\"F\"}]}]}]}</DATA>\n");
//                    else
//                        response.append("<STATE>{\"sport\":0,\"passcode\":\"1000\",\"auth\":\"3a7a1c84-d8ee-483b-85b2-0d349d66248f\"}</STATE>\n");
                String currentCommand = commandsQueue.peekFirst();


                KLog.d("SENDING COMMAND " + currentCommand);
                //si la commande dépasse la taille maximale de charactères, on l'envoi via un envoi segmenté
                if (Objects.requireNonNull(currentCommand).length() > Constants.MAX_COMMAND_SIZE) {
                    connection.createNewLongWriteBuilder()
                            .setCharacteristicUuid(Constants.UUID_CHARACTERISTIC_DATA).setBytes(Objects.requireNonNull(currentCommand).getBytes()).build()
                            .subscribe(
                                    writeModeResult -> KLog.d("ble", "Command sent by Long Write: " + currentCommand), throwable -> {
                                        KLog.e("ble", "error sending long command for device");
                                        handleBleException(throwable);
                                    });
                } else {
                    connection.writeCharacteristic(Constants.UUID_CHARACTERISTIC_DATA, Objects.requireNonNull(currentCommand).getBytes())
                            .subscribe(
                                    writeModeResult -> KLog.d("ble", "Command sent : " + currentCommand), throwable -> {
                                        KLog.e("ble", "error sending command for device");
                                        handleBleException(throwable);
                                    });
                }
            } else {
                BusMessage.post(BusMessageType.ERROR_DEVICE_DISCONNECTED);
            }
        }
    }

    /**
     * Génère un message d'erreur pour l'exception en paramètre
     *
     * @param throwable l'exception à afficher
     */
    private void handleBleException(Throwable throwable) {
        KLog.e("ble", throwable);
        if (RidePlusApplication.applicationContext != null) {
            BusMessageType type = BusMessageType.ERROR_BLE_CORE;
            int messageKey = R.string.error_message_ble_generic;
            if (throwable instanceof BleScanException) {
                //on envoi un message de type BLE_AUTH différent du BLE_CORE afin que l'app ne reprenne pas les communications.
                // en effet en cas d'erreur, l'app tente habituellement de relancer la comm' BT.
                // Mais sur les erreurs liées aux authentifications, cela conduit à une boucle infini d'erreur tant que l'utilisateur ne corrige pas le soucis.
                type = BusMessageType.ERROR_BLE_STOP;

                switch (((BleScanException) throwable).getReason()) {
                    case BleScanException.BLUETOOTH_NOT_AVAILABLE:
                        messageKey = R.string.error_message_ble_not_avalaible;
                        break;
                    case BleScanException.BLUETOOTH_DISABLED:
                        messageKey = R.string.error_message_ble_disabled;
                        break;
                    case BleScanException.LOCATION_PERMISSION_MISSING:
                        messageKey = R.string.error_message_ble_location_permission_missing;
                        break;
                    case BleScanException.LOCATION_SERVICES_DISABLED:
                        messageKey = R.string.error_message_ble_location_disabled;
                        break;
                    case BleScanException.SCAN_FAILED_FEATURE_UNSUPPORTED:
                        messageKey = R.string.error_message_ble_feature_unsupported;
                        break;
                    case BleScanException.SCAN_FAILED_OUT_OF_HARDWARE_RESOURCES:
                        messageKey = R.string.error_message_ble_feature_unsupported;
                        break;
                    case BleScanException.SCAN_FAILED_ALREADY_STARTED:
                    case BleScanException.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                    case BleScanException.SCAN_FAILED_INTERNAL_ERROR:
                    case BleScanException.UNDOCUMENTED_SCAN_THROTTLE:
                    case BleScanException.UNKNOWN_ERROR_CODE:
                    case BleScanException.BLUETOOTH_CANNOT_START:
                    default:
                        type = BusMessageType.ERROR_BLE_CORE;
                        messageKey = R.string.error_message_unknown_scan_exception;
                        break;
                }

            } else if (throwable instanceof BleServiceNotFoundException) {
                messageKey = R.string.error_message_ble_service_not_found;
            } else if (throwable instanceof BleCharacteristicNotFoundException) {
                messageKey = R.string.error_message_ble_characteristic_not_found;
            } else if (throwable instanceof BleAlreadyConnectedException) {
                messageKey = R.string.error_message_ble_already_connected;
            } else if (throwable instanceof BleGattCannotStartException) {
                messageKey = R.string.error_message_ble_characteristic_not_found;
            } else if (throwable instanceof BleGattCallbackTimeoutException) {
                messageKey = R.string.error_message_ble_characteristic_not_found;
            } else if (throwable instanceof BleGattCharacteristicException) {
                messageKey = R.string.error_message_ble_characteristic_not_found;
            } else if (throwable instanceof BleGattDescriptorException) {
                messageKey = R.string.error_message_ble_characteristic_not_found;
            } else if (throwable instanceof BleGattException) {
                messageKey = R.string.error_message_ble_characteristic_not_found;
            } else if (throwable instanceof BleCannotSetCharacteristicNotificationException) {
                messageKey = R.string.error_message_unknown_notification_exception;
            } else if (throwable instanceof BleConflictingNotificationAlreadySetException) {
                messageKey = R.string.error_message_unknown_notification_exception;
            } else if (throwable instanceof BleDisconnectedException) {
                cancelSubscriptions();
                messageKey = R.string.error_device_disconnected;
                type = BusMessageType.ERROR_DEVICE_DISCONNECTED;
            } else if (throwable instanceof BleException) {
                messageKey = R.string.error_message_ble_service_not_found;
            }

            BusMessage.post(type, RidePlusApplication.applicationContext.getString(messageKey));
        }
    }

    /**
     * Analyse la réponse reçu du boitier
     */
    private void parseResponse() {
        KLog.d("ble", "Full response received");
        if (response != null && response.length() > 0) {
            KLog.d("ble", "Response : " + response.toString());

            //on vérifie si il s'agit d'une réponse à une demande d'état
            final Pattern statePattern = Pattern.compile(Constants.REGEX_STATE);
            final Pattern dataPattern = Pattern.compile(Constants.REGEX_DATA);

            //on verifie qu'on ait une réponse correcte. Sinon, le timeout se chargera de relancer la commande
            if (statePattern.matcher(response.toString()).find() || dataPattern.matcher(response.toString()).find()) {

                KLog.d("ble", "Admissible pattern found");

                //on envoi un message pr indiquer que l'on est bien connecté
                BusMessage.post(BusMessageType.DEVICE_CONNECTION_CHANGED, RxBleConnection.RxBleConnectionState.CONNECTED);

                //on marque l'appareil à prêt à communiquer
                deviceReady = true;

                boolean validResponse = false;

                try {
                    //on vérifie si il s'agit d'une réponse à une demande d'état
                    final Matcher stateMatcher = statePattern.matcher(response.toString());
                    while (stateMatcher.find()) {
                        if (stateMatcher.groupCount() == 1) {
                            String state = stateMatcher.group(1);
                            validResponse = true;
                            device.setState(state);
                            //on vérifie que l'appareil est bien un authentique appareil BTW
                            if (device.isAuthenticated(state)) {
                                //si l'utilisateur a déjà l'appareil, on le modifie, sinon on l'ajoute
                                if (RidePlusApplication.currentUserManager.hasDevice(device)) {
                                    RidePlusApplication.currentUserManager.setDevice(device);

                                    //on change le sport courant pr refleter celui du boitier
                                    RidePlusApplication.configManager.setCurrentSport(device.sport);
                                } else {
                                    RidePlusApplication.currentUserManager.addDevice(device);
                                }
                            } else {
                                //si l'utilisateur a l'appareil, on le supprime
                                if (RidePlusApplication.currentUserManager.hasDevice(device)) {
                                    RidePlusApplication.currentUserManager.deleteDevice(device);
                                }
                                //on on relance le scan
                                startScan(null);
                            }
                        }
                    }

                } catch (JSONException exception) {
                    KLog.e("ble", exception);
                    BusMessage.post(BusMessageType.ERROR_DEVICE_STATE);
                }

                try {

                    //on vérifie si il s'agit d'une réponse à une demande de données
                    final Matcher dataMatcher = dataPattern.matcher(response.toString());
                    while (dataMatcher.find()) {
                        if (dataMatcher.groupCount() == 1) {
                            validResponse = true;
                            // on ne notifie pas l'utilisateur que les sessions sont vides lorsque l'on traite le résultat d'une commande clear
                            if (!Objects.equals(commandsQueue.peekFirst(), Constants.COMMAND_CLEAR_DATA)) {
                                List<Session> sessions = Session.createFromJson(dataMatcher.group(1), device.serial);
                                if (sessions != null && !sessions.isEmpty()) {
                                    RidePlusApplication.sessionManager.saveSessions(sessions);
                                    //on envoi une commande pr purger les session
                                    clearSessions();
                                    RidePlusApplication.configManager.setCurrentSport(sessions.get(sessions.size() - 1).sport);
                                } else {
                                    BusMessage.post(BusMessageType.SYNC_NO_WAVES);
                                }
                            }
                        }
                    }

                } catch (JSONException exception) {
                    KLog.e("ble", exception);
                    BusMessage.post(BusMessageType.ERROR_DEVICE_DATA);
                }

                //si la réponse n'est pas correcte, on déconnecte l'appareil
                if (!validResponse) {
                    BusMessage.post(BusMessageType.DEVICE_NOT_RESPONDING);
                }

                response = new StringBuffer();
                commandsQueue.remove(0);

                //on vérifie la file d'attente des commandes en cours et on envoi une commandes si nécessaire
                if (commandsQueue.size() > 0) {
                    sendCommand(null);
                }

            }

        }
    }

    /**
     * Surveille les changemnt d'état de la connexion avec l'appareil
     *
     * @param bleDevice l'appareil bLE à surveiller
     */
    private void setConnectionStateChangeListener(RxBleDevice bleDevice) {
        if ((connectionStateSubscription == null || connectionStateSubscription.isUnsubscribed()) && bleDevice != null) {
            KLog.d("ble", "Setup connection listner");
            connectionStateSubscription = bleDevice.observeConnectionStateChanges().subscribe(rxBleConnectionState -> {
                        KLog.w("ble", rxBleConnectionState);
                        BusMessage.post(BusMessageType.DEVICE_CONNECTION_CHANGED, rxBleConnectionState);
                    },
                    this::handleBleException);
        }
    }


}
