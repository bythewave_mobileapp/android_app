package com.bythewave.rideplus.manager;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.bythewave.rideplus.RidePlusApplication;

/**
 * com.by_the_wave.ride_plus_android.manager.ActivityLifecycleManager
 * <p/>
 * Classe permettant de compter les activités en cours d'exécutions
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 11/02/2018
 */
public class ActivityLifecycleManager implements Application.ActivityLifecycleCallbacks {

    private int resumed;
    private int paused;
    private int started;
    private int stopped;

    /**
     * Constructeur vide
     */
    public ActivityLifecycleManager() {
        resumed = paused = started = stopped = 0;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        ++resumed;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        ++paused;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        ++started;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        ++stopped;
        if (started <= stopped) {
            //on coupe les communications
            RidePlusApplication.bluetoothDeviceManager.cancelSubscriptions();
        }
    }

    /**
     * Indique si au moins une activité de l'application est actives
     *
     * @return vrai si au moins une activité de l'application est actives, faux sinon
     */
    public boolean isApplicationInForeground() {
        return resumed > paused;
    }
}
