package com.bythewave.rideplus.manager;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Config;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.models.enums.Sport;
import com.g00fy2.versioncompare.Version;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.socks.library.KLog;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

/**
 * com.bythewave.rideplus.manager.ConfigManager
 * <p/>
 * <p>
 * Gestionnaire des configurations distantes de l'application.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 10/11/2018
 */
public class ConfigManager {

    // Constantes pr le fichiers de configuration distant
    private static final String CONFIG_ANDROID = "android";
    private static final String CONFIG_API = "api";

    // TODO IMPORTANT : changer ce champ lorsque l'API évolue
    private static final String API_VERSION = "2.0.0";

    private static final String COACHING_FILE_NAME = "%s-coaching-%s.json";

    /**
     * Constantes utiliisées par le fichier JSON des conseils de coaching
     */
    private static final String MOMENT_COLORS = "momentColors";
    private static final String LEVELS = "levels";
    private static final String COLOR = "color";
    private static final String COLOR_FADED = "color_faded";
    private static final String ADVISES = "advices";
    private static final String FRONTSIDE = "frontside";
    private static final String BACKSIDE = "backside";
    private static final String IMPORTANT = "important";
    private static final String IMAGES = "images";
    private static final String PROS = "pros";
    private static final String CONS = "cons";
    private static final String HORIZONTAL_SPEED_DOWN = "horizontal_speed_down";
    private static final String HORIZONTAL_SPEED_UP = "horizontal_speed_up";
    private static final String VERTICAL_SPEED_DOWN = "vertical_speed_down";
    private static final String VERTICAL_SPEED_UP = "vertical_speed_up";
    private static final String SAMPLE_SIZE = "sample_size";

    private static ConfigManager instance;
    private Sport currentSport;

    //tableau de correspondance des conseils, points positifs, négatifs, etc pour illustrer les vagues
    private Map<Integer, List<String>> levelPros;
    private Map<Integer, List<String>> levelCons;
    private Map<Integer, List<String>> levelAdvicesImportant;
    private Map<Integer, List<String>> levelAdvicesFrontside;
    private Map<Integer, List<String>> levelAdvicesBackside;
    private Map<Integer, List<String>> levelImageFrontside;
    private Map<Integer, List<String>> levelImageBackside;
    private Map<Integer, Integer> levelColors;
    private Map<Integer, Integer> levelColorsFaded;
    private Map<String, Integer> momentColors;

    //paramètes de l'algorithme de tracking GPS
    public float verticalSpeedUp;
    public float verticalSpeedDown;
    public float horizontalSpeedUp;
    public float horizontalSpeedDown;
    public int sampleSize;

    @SuppressLint("UseSparseArrays")
    private ConfigManager() {
        levelPros = new HashMap<>();
        levelCons = new HashMap<>();
        levelAdvicesImportant = new HashMap<>();
        levelAdvicesFrontside = new HashMap<>();
        levelAdvicesBackside = new HashMap<>();
        levelImageFrontside = new HashMap<>();
        levelImageBackside = new HashMap<>();
        levelColors = new HashMap<>();
        levelColorsFaded = new HashMap<>();
        momentColors = new HashMap<>();
        currentSport = RidePlusApplication.preferencesManager.getDefaultSport();
        verticalSpeedUp = Constants.VERTICAL_SPEED_UP_THRESHOLD;
        verticalSpeedDown = Constants.VERTICAL_SPEED_DOWN_THRESHOLD;
        horizontalSpeedUp = Constants.HORIZONTAL_SPEED_UP_THRESHOLD;
        horizontalSpeedDown = Constants.HORIZONTAL_SPEED_DOWN_THRESHOLD;
        sampleSize = Constants.SPEED_SAMPLE_COUNT;
        reloadCurrentCoachingFile();
    }


    /**
     * Vérifie que la version courante de l'API est compatible avec la version actuelle de l'application.
     */
    public void checkApiVersion() {
        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_CONFIGS).document(CONFIG_API).get()
                .addOnCompleteListener(getDocumentTask -> {
                    if (getDocumentTask.isSuccessful()) {
                        DocumentSnapshot document = getDocumentTask.getResult();
                        if (document != null && document.exists()) {
                            Config config = document.toObject(Config.class);

                            if (config != null && new Version(API_VERSION).getMajor() == new Version(config.version).getMajor()) {
                                BusMessage.post(BusMessageType.APPLICATION_UP_TO_DATE);
                            } else {
                                BusMessage.post(BusMessageType.APPLICATION_OUTDATED);
                            }
                        }
                    }
                }).addOnFailureListener(throwable -> {
            KLog.e(throwable);
            BusMessage.post(BusMessageType.APPLICATION_UP_TO_DATE);
        });
    }

    /**
     * Récupère le fichier des conseils sur l'app
     */
    public void downloadCoachingFiles() {
        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_CONFIGS).document(CONFIG_ANDROID).get()
                .addOnCompleteListener(getDocumentTask -> {
                    if (getDocumentTask.isSuccessful()) {
                        DocumentSnapshot document = getDocumentTask.getResult();
                        if (document != null && document.exists()) {
                            Config config = document.toObject(Config.class);

                            if (config != null && config.advices != null) {

                                List<FileDownloadTask> tasks = new ArrayList<>();

                                for (String sport : config.advices.files.keySet()) {
                                    Map<String, String> sportConfig = config.advices.files.get(sport);
                                    if (sportConfig != null)
                                        for (String language : sportConfig.keySet()) {
                                            String fileUrl = sportConfig.get(language);
                                            File localFile = new File(RidePlusApplication.applicationContext.getFilesDir(), String.format(COACHING_FILE_NAME, sport, language));

                                            //on télécharge et sauvegarde tous le fichiers de coaching si la version a changé ou si le fichier n'existe pas
                                            if (RidePlusApplication.applicationContext != null && !StringUtils.isEmpty(fileUrl) && (!localFile.exists() || !RidePlusApplication.preferencesManager.getConfigVersion().equals(config.advices.version))) {
                                                StorageReference coachingFileRef = FirebaseStorage.getInstance().getReferenceFromUrl(fileUrl);
                                                tasks.add(coachingFileRef.getFile(localFile));
                                            }
                                        }
                                }

                                //on sauve la version actuelle du fichier de config
                                RidePlusApplication.preferencesManager.setConfigVersion(config.advices.version);

                                //on parse les paramètres du tracking GPS
                                if (config.gps_params != null) {
                                    this.horizontalSpeedDown = config.gps_params.horizontal_speed_down;
                                    this.horizontalSpeedUp = config.gps_params.horizontal_speed_up;
                                    this.verticalSpeedDown = config.gps_params.vertical_speed_down;
                                    this.verticalSpeedUp = config.gps_params.vertical_speed_up;
                                    this.sampleSize = config.gps_params.sample_size;
                                    KLog.d(String.format("hd : %f hu %f vd %f vu %f s %d", horizontalSpeedDown, horizontalSpeedUp, verticalSpeedDown, verticalSpeedUp, sampleSize));
                                }

                                //on attend la fin de tt les tâches si il y en a
                                boolean isPending = !tasks.isEmpty();
                                while (isPending) {
                                    isPending = false;
                                    for (FileDownloadTask task : tasks) {
                                        if (task.isInProgress())
                                            isPending = true;
                                    }
                                    if (isPending)
                                        synchronized (this) {
                                            try {
                                                this.wait(Constants.DEFAULT_UI_DELAY);
                                            } catch (InterruptedException e) {
                                                KLog.e(e);
                                            }
                                        }
                                }

                                //on charge le fichier de conseil
                                new Handler().post(this::reloadCurrentCoachingFile);

                                // on envoi un message pour indiquer que la configuration est à jour
                                BusMessage.post(BusMessageType.CONFIGURATION_UPDATED);
                            }
                        }
                    }
                }).addOnFailureListener(throwable -> {
            KLog.e(throwable);
            BusMessage.post(BusMessageType.CONFIGURATION_UPDATED);
        });
    }

    /**
     * Parse le fichier JSON des conseils à afficher pour une vague
     */
    private void reloadCurrentCoachingFile() {
        try {
            if (RidePlusApplication.applicationContext != null) {
                //on purge les maps de leurs info déjà chargées
                this.levelColorsFaded.clear();
                this.levelColors.clear();
                this.levelAdvicesBackside.clear();
                this.levelAdvicesFrontside.clear();
                this.levelImageBackside.clear();
                this.levelImageFrontside.clear();
                this.levelPros.clear();
                this.levelCons.clear();

                File localFile = new File(RidePlusApplication.applicationContext.getFilesDir(), String.format(COACHING_FILE_NAME, currentSport.value(), Locale.getDefault().getLanguage()));
                if (localFile.exists()) {
                    String content = AndroidUtils.readFile(localFile);
                    if (!TextUtils.isEmpty(content)) {
                        JSONObject rawJsonObject = new JSONObject(content);

                        // on récupère les couleurs associées aux moments caractéristiques
                        if (rawJsonObject.has(MOMENT_COLORS)) {
                            JSONObject colors = rawJsonObject.getJSONObject(MOMENT_COLORS);
                            Iterator<String> keys = colors.keys();
                            while (keys.hasNext()) {
                                String key = keys.next();
                                momentColors.put(key, Color.parseColor(colors.getString(key)));
                            }
                        }

                        // on récupère les informations de chaque niveau
                        if (rawJsonObject.has(LEVELS)) {
                            JSONObject levels = rawJsonObject.getJSONObject(LEVELS);
                            Iterator<String> keys = levels.keys();
                            while (keys.hasNext()) {
                                String levelKey = keys.next();
                                Integer levelInteger = Integer.parseInt(levelKey);
                                JSONObject level = levels.getJSONObject(levelKey);
                                if (level != null) {
                                    if (level.has(COLOR))
                                        levelColors.put(levelInteger, Color.parseColor(level.getString(COLOR)));
                                    if (level.has(COLOR_FADED))
                                        levelColorsFaded.put(levelInteger, Color.parseColor(level.getString(COLOR_FADED)));
                                    if (level.has(PROS)) {
                                        JSONArray pros = level.getJSONArray(PROS);
                                        ArrayList<String> texts = new ArrayList<>();
                                        for (int index = 0; index < pros.length(); index++) {
                                            texts.add(pros.getString(index));
                                        }
                                        levelPros.put(levelInteger, texts);
                                    }
                                    if (level.has(CONS)) {
                                        JSONArray cons = level.getJSONArray(CONS);
                                        ArrayList<String> texts = new ArrayList<>();
                                        for (int index = 0; index < cons.length(); index++) {
                                            texts.add(cons.getString(index));
                                        }
                                        levelCons.put(levelInteger, texts);
                                    }
                                    if (level.has(ADVISES)) {
                                        if (level.getJSONObject(ADVISES).has(IMPORTANT)) {
                                            JSONArray importantAdvises = level.getJSONObject(ADVISES).getJSONArray(IMPORTANT);
                                            ArrayList<String> advices = new ArrayList<>();
                                            for (int index = 0; index < importantAdvises.length(); index++) {
                                                advices.add(importantAdvises.getString(index));
                                            }
                                            levelAdvicesImportant.put(levelInteger, advices);
                                        }
                                        if (level.getJSONObject(ADVISES).has(FRONTSIDE)) {
                                            JSONArray frontsideAdvises = level.getJSONObject(ADVISES).getJSONArray(FRONTSIDE);
                                            ArrayList<String> advices = new ArrayList<>();
                                            for (int index = 0; index < frontsideAdvises.length(); index++) {
                                                advices.add(frontsideAdvises.getString(index));
                                            }
                                            levelAdvicesFrontside.put(levelInteger, advices);
                                        }
                                        if (level.getJSONObject(ADVISES).has(BACKSIDE)) {
                                            JSONArray backsideAdvises = level.getJSONObject(ADVISES).getJSONArray(BACKSIDE);
                                            ArrayList<String> advices = new ArrayList<>();
                                            for (int index = 0; index < backsideAdvises.length(); index++) {
                                                advices.add(backsideAdvises.getString(index));
                                            }
                                            levelAdvicesBackside.put(levelInteger, advices);
                                        }
                                    }
                                    if (level.has(IMAGES)) {
                                        if (level.getJSONObject(IMAGES).has(FRONTSIDE)) {
                                            JSONArray frontsideImages = level.getJSONObject(IMAGES).getJSONArray(FRONTSIDE);
                                            ArrayList<String> images = new ArrayList<>();
                                            for (int index = 0; index < frontsideImages.length(); index++) {
                                                images.add(frontsideImages.getString(index));
                                            }
                                            levelImageFrontside.put(levelInteger, images);
                                        }
                                        if (level.getJSONObject(IMAGES).has(BACKSIDE)) {
                                            JSONArray backsideImages = level.getJSONObject(IMAGES).getJSONArray(BACKSIDE);
                                            ArrayList<String> images = new ArrayList<>();
                                            for (int index = 0; index < backsideImages.length(); index++) {
                                                images.add(backsideImages.getString(index));
                                            }
                                            levelImageBackside.put(levelInteger, images);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //On informe les autres vues
                    BusMessage.post(BusMessageType.CURRENT_SPORT_UPDATED, currentSport);
                } else {
                    //on recharge le fichier
                    downloadCoachingFiles();
                }
            }
        } catch (JSONException e) {
            KLog.e(e);
        }

    }

    public static ConfigManager getInstance() {
        if (instance == null) {
            instance = new ConfigManager();
        }
        return instance;
    }

    /* ******************************************************************************************* *
     *                                          Coaching                                           *
     * ******************************************************************************************* */


    /**
     * Renvoi la couleur de la vague, en fonction de sa note
     *
     * @param level la niveau de la vague
     * @param faded vrai si pour récupérer les couleurs translucides, faux pour les couleurs vives.
     * @return la couleur de la vague, en fonction de sa note
     */
    public int getLevelColor(int level, boolean faded) {
        if (levelColors != null && levelColorsFaded != null && levelColors.containsKey(level) && levelColorsFaded.containsKey(level) && levelColors.get(level) != null && levelColorsFaded.get(level) != null)
            return faded ? levelColorsFaded.get(level) : levelColors.get(level);
        else
            return -1;
    }

    /**
     * Renvoi la couleur d'un moment caractéristique, en fonction de sa valeur
     *
     * @param momentValue la valeur du moment caractéristique
     * @return la couleur du moment caractéristique, en fonction de sa valeur
     */
    public int getMomentColor(String momentValue) {
        if (momentColors != null && momentColors.containsKey(momentValue))
            return momentColors.get(momentValue);
        else
            return R.color.lavender_gray;
    }

    /**
     * Renvoi les points positifs associés au niveau d'une vague
     *
     * @param level le niveau de la vague
     * @param max   le nombre maximal d'élément à renvoyer
     * @param seed  un nombre servant de base pour calculer un pseudo aléatoire
     * @return une liste de points positifs associés au niveau d'une vague
     */
    public List<String> getPros(int level, int max, long seed) {
        List result = new ArrayList<>();
        if (levelPros != null && levelPros.containsKey(level)) {
            result = pickElements(result, Objects.requireNonNull(levelPros.get(level)), max, seed);
        }
        return result;
    }

    /**
     * Renvoi les points négatifs associés au niveau d'une vague
     *
     * @param level le niveau de la vague
     * @param max   le nombre maximal d'élément à renvoyer
     * @param seed  un nombre servant de base pour calculer un pseudo aléatoire
     * @return une liste de points négatifs associés au niveau d'une vague
     */
    public List<String> getCons(int level, int max, long seed) {
        if (levelCons != null && levelCons.containsKey(level))
            return pickElements(new ArrayList(), levelCons.get(level), max, seed);
        else return new ArrayList<>();
    }

    /**
     * Renvoi les conseils associés au niveau d'une vague
     *
     * @param level     le niveau de la vague
     * @param frontside vrai pour récupérer les éléments pour un surfer frontside, faux sinon
     * @param max       le nombre maximal d'élément à renvoyer
     * @param seed      un nombre servant de base pour calculer un pseudo aléatoire
     * @return les conseils associés au niveau d'une vague
     */
    public List<String> getAdvices(int level, boolean frontside, int max, long seed) {
        //on initialise une liste de conseils avec les importants
        List result = new ArrayList<String>();
        //on ajoute les conseils généraux si disponibles
        if (levelAdvicesImportant != null && levelAdvicesImportant.containsKey(level) && levelAdvicesImportant.get(level) != null)
            result.addAll(levelAdvicesImportant.get(level));

        //puis on ajoute les conceils par stances
        if (frontside && levelAdvicesFrontside != null && levelAdvicesFrontside.containsKey(level) && levelAdvicesFrontside.get(level) != null)
            return pickElements(result, levelAdvicesFrontside.get(level), max, seed);
        else if (!frontside && levelAdvicesBackside != null && levelAdvicesBackside.containsKey(level) && levelAdvicesBackside.get(level) != null)
            return pickElements(result, levelAdvicesBackside.get(level), max, seed);
        return result;
    }

    /**
     * Renvoi l'id de l'image illustrant le niveau d'une vague
     *
     * @param level la niveau de la vague
     * @param seed  un nombre servant de base pour calculer un pseudo aléatoire
     * @return l'id de l'image illustrant le niveau d'une vague
     */
    public String getLevelImage(int level, boolean frontside, long seed) {
        List result = new ArrayList();
        if (frontside && levelImageFrontside != null && levelImageFrontside.containsKey(level) && levelImageFrontside.get(level) != null)
            result = pickElements(new ArrayList(), levelImageFrontside.get(level), 1, seed);
        else if (!frontside && levelImageBackside != null && levelImageBackside.containsKey(level) && levelAdvicesBackside.get(level) != null)
            result = pickElements(new ArrayList(), levelImageBackside.get(level), 1, seed);

        if (result.size() > 0 && result.get(0).getClass().equals(String.class))
            return (String) result.get(0);
        else return null;
    }

    /**
     * Renvoi une sous sélection aléatoire d'éléments d'une liste en paramètres
     *
     * @param result la liste des élémnts à renvoyer
     * @param source la liste d'où sélectionner des éléments
     * @param max    le nombre maximal d'éléments à renvoyer
     * @param seed   un nombre servant de base pour calculer un pseudo aléatoire
     * @return une sous sélection aléatoire d'éléments d'une liste en paramètres
     */
    private List pickElements(List result, List source, int max, long seed) {
        if (source != null && !source.isEmpty()) {
            int initialResultSize = result.size();

            int newSeed = Math.abs(Long.valueOf(seed / 1000).intValue());

            // on pioche pseudo aléatoirement 3 éléments.
            while (result.size() < Math.min(max, (source.size() + initialResultSize))) {
                if (newSeed > 0)
                    // On utilise un nombre source qu'on manipule pr générer un pseudo aléatoire entre
                    // les différentes vagues, mais qui soit tjrs calculé de la mme manière pr chaque vague.
                    newSeed = newSeed / 10;
                else
                    //si la graine d'aléa est nulle, on rebascule sur un pur aléatoire.
                    newSeed = new Random(source.size()).nextInt();

                Object randomElement = source.get(newSeed % source.size());
                if (!result.contains(randomElement)) {
                    result.add(randomElement);
                }
            }
        }
        return result;
    }

    /**
     * @return le sport courant
     */
    public Sport getCurrentSport() {
        return currentSport;
    }

    /**
     * Change le sport courant.
     * Configure le gestionnaire de configuration à l'aide du sport en paramètre
     *
     * @param currentSport le nouveau sport courant
     */
    public void setCurrentSport(Sport currentSport) {
        this.currentSport = currentSport;

        //on sauvegarde en préférence le sport par défaut
        RidePlusApplication.preferencesManager.setDefaultSport(currentSport);

        reloadCurrentCoachingFile();
    }
}
