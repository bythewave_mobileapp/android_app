package com.bythewave.rideplus.manager;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Session;
import com.bythewave.rideplus.models.Wave;
import com.bythewave.rideplus.models.deserializers.DirectionDeserializer;
import com.bythewave.rideplus.models.deserializers.GeoPointDeserializer;
import com.bythewave.rideplus.models.deserializers.SportDeserializer;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.models.enums.Direction;
import com.bythewave.rideplus.models.enums.Sport;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.WriteBatch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

/**
 * com.by_the_wave.ride_plus_android.manager.SessionManager
 * <p/>
 * Singleton gérant les sessions
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 16/02/2018
 */
public class SessionManager {

    private static SessionManager instance;
    private List<Session> sessionsToDelete;
    private boolean selectingSessions;

    private List<Wave> wavesToDelete;
    private boolean selectingWaves;

    /**
     * Constructeur vide
     */
    @SuppressLint("UseSparseArrays")
    private SessionManager() {
        sessionsToDelete = new ArrayList<>();
        selectingSessions = false;
        wavesToDelete = new ArrayList<>();
        selectingWaves = false;

    }


    public static SessionManager getInstance() {
        if (instance == null) {
            instance = new SessionManager();
        }
        return instance;
    }


    /* ******************************************************************************************* *
     *                                          Session                                            *
     * ******************************************************************************************* */

    /**
     * Purge la liste des sessions à supprimer
     */
    public void clearSessionToDelete() {
        this.sessionsToDelete.clear();
    }

    /**
     * Ajoute une session à la liste des sessions à supprimer
     *
     * @param session la session à ajouter de la liste des sessions à supprimer
     */
    public void addSessionToDelete(Session session) {
        if (session != null && !sessionsToDelete.contains(session)) {
            this.sessionsToDelete.add(session);
        }
        if (sessionsToDelete.size() == 1)
            BusMessage.post(BusMessageType.ITEM_MARKED);
    }

    /**
     * Retire une session de la liste des sessions à supprimer
     *
     * @param session la session à retirer de la liste des sessions à supprimer
     */
    public void removeSessionToDelete(Session session) {
        if (session != null && sessionsToDelete.contains(session)) {
            sessionsToDelete.remove(session);
        }
        if (sessionsToDelete.size() == 0)
            BusMessage.post(BusMessageType.NO_ITEM_MARKED);
    }

    /**
     * Marque la sélection de sessions à supprimer comme activé ou non
     *
     * @param selecting vrai pour marquer la sélection des session comme activé, faux sinon
     */
    public void setSelectingSessions(boolean selecting) {
        this.selectingSessions = selecting;
    }

    /**
     * Indique si la sélection de sessions à supprimer est activé ou non
     *
     * @return vrai si la sélection des session est activé, faux sinon
     */
    public boolean isSelectingSessions() {
        return this.selectingSessions;
    }

    /**
     * Indique si la session en paramètre est marqué à supprimer
     *
     * @param session la session à évaluer
     * @return vrai si la session est marquée à supprimer, faux sinon
     */
    public boolean isMarkedToDelete(Session session) {
        return session != null && sessionsToDelete.contains(session);
    }

    /**
     * Renvoi la liste des sessions à supprimer
     *
     * @return la liste des sessions à supprimer
     */
    public List<Session> getSessionsToDelete() {
        return sessionsToDelete;
    }

    /**
     * Sauvegarde la session sur la base FireStore
     */
    public void saveSessions(List<Session> sessions) {
        WriteBatch batch = FirebaseFirestore.getInstance().batch();
        for (Session session : sessions) {
            if (session != null && !TextUtils.isEmpty(session.userId) && session.startedAt != null) {
                DocumentReference sessionRef = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                        .document(session.getSessionId());
                batch.set(sessionRef, session);
            }
        }
        batch.commit().addOnFailureListener(exception -> {
            KLog.e(exception);
            BusMessage.post(BusMessageType.SYNC_FAILED);
        });
        BusMessage.post(BusMessageType.SESSION_ADDED);
    }

    /**
     * Sauvegarde la session en paramètre sur le serveur
     *
     * @param session la session à sauvegarder
     */
    public void saveSession(Session session) {
        //on sauvegarde la session sur le serveur
        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                .document(session.getSessionId())
                .set(session)
                .addOnFailureListener(exception -> {
                    KLog.e(exception);
                    BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                    //on essaye de mettre à jour la session depuis le serveur pr rester cohérent
                    reloadSession(session);
                });
        BusMessage.post(BusMessageType.SESSION_UPDATED, session);
    }

    /**
     * Supprime toutes les sessions marquées à supprimer
     */
    public void deleteMarkedSessions() {
        WriteBatch batch = FirebaseFirestore.getInstance().batch();
        for (Session session : sessionsToDelete) {
            if (session != null) {
                session.toDelete = true;
                DocumentReference sessionRef = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                        .document(session.getSessionId());
                batch.set(sessionRef, session);
            }
            // Firebase log
            RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_SESSION_DELETED, null);
        }

        batch.commit().addOnFailureListener(exception -> {
            KLog.e(exception);
            BusMessage.post(BusMessageType.SESSION_DELETE_FAILED);
        });
    }

    /**
     * Met à jour une session à partir des informations du serveur
     *
     * @param session la session à mettre à jour
     */
    private void reloadSession(Session session) {
        DocumentReference sessionReference = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS).document(session.getSessionId());
        sessionReference.get()
                .addOnCompleteListener(getDocumentTask -> {
                    if (getDocumentTask.isSuccessful()) {
                        DocumentSnapshot document = getDocumentTask.getResult();
                        if (document != null && document.exists()) {
                            Session sessionUpdated = document.toObject(Session.class);
                            BusMessage.post(BusMessageType.SESSION_UPDATED, sessionUpdated);
                        }
                    } else {
                        KLog.e(getDocumentTask.getException());
                        if (getDocumentTask.getException() instanceof FirebaseNetworkException) {
                            BusMessage.post(BusMessageType.ERROR_OFFLINE);
                        } else if (getDocumentTask.getException() instanceof FirebaseFirestoreException) {
                            BusMessage.post(BusMessageType.ERROR_GENERIC_AUTH);
                        } else {
                            BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                        }
                    }
                });
    }

    /**
     * Ajoute une session d'exemple à l'utilisateur courant
     */
    public void addExampleSession(Sport sport) {
        String session_example = AndroidUtils.loadFileFromAsset(String.format(RidePlusApplication.applicationContext.getResources().getString(R.string.session_file), sport.value().toLowerCase()));
        //on utilise un parseur spécifique pr l'énuméré de direction d'une vague
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Direction.class, new DirectionDeserializer());
        gsonBuilder.registerTypeAdapter(GeoPoint.class, new GeoPointDeserializer());
        gsonBuilder.registerTypeAdapter(Sport.class, new SportDeserializer());
        Session session = gsonBuilder.create().fromJson(session_example, Session.class);
        session.isViewed = false;
        session.deviceId = Constants.TEST_SESSION_DEVICE_NAME;
        session.userId = RidePlusApplication.currentUserManager.getCurrentUserId();
        RidePlusApplication.sessionManager.saveSession(session);
    }

    /**
     * Ajoute le champ sport aux anciennes session d'un utilisateur
     */
    public void migrateOldSession() {
        //on vérifie si on a déjà migré les anciennes sessions
        if (RidePlusApplication.preferencesManager.shouldMigrateOldSession()) {
            FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                    .whereEqualTo(Constants.FIELD_USER_ID, RidePlusApplication.currentUserManager.getCurrentUserId())
                    .get().addOnCompleteListener(task -> {
                List<Session> sessions = new ArrayList<>();
                if (task.getResult() != null) {
                    for (DocumentSnapshot document : task.getResult()) {
                        if (document != null && document.exists()) {
                            Session session = document.toObject(Session.class);
                            if (session != null) {
                                if (session.sport == null)
                                    session.sport = Sport.SURF;
                                sessions.add(session);
                            }
                        }
                    }
                }
                //on sauvegarde les sessions "à la main" pour ne pas envoyer de message sur le bus système
                WriteBatch batch = FirebaseFirestore.getInstance().batch();
                for (Session session : sessions) {
                    if (session != null && !TextUtils.isEmpty(session.userId) && session.startedAt != null) {
                        DocumentReference sessionRef = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                                .document(session.getSessionId());
                        batch.set(sessionRef, session);
                    }
                }
                batch.commit();

                RidePlusApplication.preferencesManager.setOldSessionsMigrated();
            });
        }
    }

    /**
     * Récupère une session sur le serveur et affiche son contenu en json
     *
     * @param sessionId l'ID de la session à mettre à jour
     */
    public void dumpSession(String sessionId) {
        DocumentReference sessionReference = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS).document(sessionId);
        sessionReference.get()
                .addOnCompleteListener(getDocumentTask -> {
                    if (getDocumentTask.isSuccessful()) {
                        DocumentSnapshot document = getDocumentTask.getResult();
                        if (document != null && document.exists()) {
                            Session session = document.toObject(Session.class);
                            if (session != null) {
                                KLog.d(new Gson().toJson(session));
                            }
                        }
                    } else {
                        KLog.e(getDocumentTask.getException());
                        if (getDocumentTask.getException() instanceof FirebaseNetworkException) {
                            BusMessage.post(BusMessageType.ERROR_OFFLINE);
                        } else if (getDocumentTask.getException() instanceof FirebaseFirestoreException) {
                            BusMessage.post(BusMessageType.ERROR_GENERIC_AUTH);
                        } else {
                            BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                        }
                    }
                });
    }


    /* ******************************************************************************************* *
     *                                          Vagues                                             *
     * ******************************************************************************************  */


    /**
     * Purge la liste des vagues à supprimer
     */
    public void clearWaveToDelete() {
        this.wavesToDelete.clear();
    }

    /**
     * Ajoute une vague à la liste des vagues à supprimer
     *
     * @param wave la vague à ajouter de la liste des vagues à supprimer
     */
    public void addWaveToDelete(Wave wave) {
        if (wave != null && !wavesToDelete.contains(wave)) {
            this.wavesToDelete.add(wave);
        }
        if (wavesToDelete.size() == 1)
            BusMessage.post(BusMessageType.ITEM_MARKED);
    }

    /**
     * Retire une vague de la liste des vagues à supprimer
     *
     * @param wave la vague à retirer de la liste des vagues à supprimer
     */
    public void removeWaveToDelete(Wave wave) {
        if (wave != null && wavesToDelete.contains(wave)) {
            wavesToDelete.remove(wave);
        }
        if (wavesToDelete.size() == 0)
            BusMessage.post(BusMessageType.NO_ITEM_MARKED);
    }

    /**
     * Marque la sélection de vagues à supprimer comme activé ou non
     *
     * @param selectingWaves vrai si la sélection de vagues à supprimer est activée, faux sinon
     */
    public void setSelectingWaves(boolean selectingWaves) {
        this.selectingWaves = selectingWaves;
    }

    /**
     * Indique si la sélection de vagues à supprimer est activé ou non
     *
     * @return vrai si la sélection des vague est activé, faux sinon
     */
    public boolean isSelectingWaves() {
        return selectingWaves;
    }

    /**
     * Indique si la vague en paramètre est marqué à supprimer
     *
     * @param wave la vague à évaluer
     * @return vrai si la vague est marquée à supprimer, faux sinon
     */
    public boolean isMarkedToDelete(Wave wave) {
        return wave != null && wavesToDelete.contains(wave);
    }

    /**
     * Renvoi la liste des vagues à supprimer
     *
     * @return la liste des vagues à supprimer
     */
    public List<Wave> getWavesToDelete() {
        return wavesToDelete;
    }

    /**
     * Supprime toutes les vagues marquées à supprimer pour une session
     *
     * @param parentSession la session parente à laquelle supprimer les vagues
     */
    public void deleteMarkedWaves(Session parentSession) {
        if (parentSession != null) {
            //on marque les vagues à supprimer
            for (Wave wave : wavesToDelete) {
                for (Wave waveParent : parentSession.waves) {
                    if (wave.equals(waveParent)) {
                        waveParent.toDelete = true;
                    }
                }
            }
            saveSession(parentSession);

            // Firebase log
            RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_WAVE_DELETED, null);
        }
    }


}
