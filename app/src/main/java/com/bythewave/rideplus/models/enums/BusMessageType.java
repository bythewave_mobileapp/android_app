package com.bythewave.rideplus.models.enums;

/**
 * BusMessageType
 * <p>
 * Enumération des différents type de messages circulant sur le bus système.
 * <p>
 *
 * @author L'Apptelier SARL
 * @date 19/01/18.
 */
public enum BusMessageType {
    // cas d'erreurs
    ERROR_USER_COLLISION,
    ERROR_WRONG_CREDENTIAL,
    ERROR_WEAK_PASSWORD,
    ERROR_WRONG_MAIL,
    ERROR_GENERIC_AUTH,
    ERROR_GENERIC_SERVER,
    ERROR_BLE_CORE,
    ERROR_OFFLINE,
    ERROR_DEVICE_DISCONNECTED,
    ERROR_DEVICE_DATA,
    ERROR_DEVICE_STATE,
    ERROR_BLE_STOP,
    ERROR_BLE_COMMAND_TIMEOUT,
    // messages sur le contenu des vues
    SCROLL_TO_TOP,
    RELOAD_CONTENT,
    REFRESH_CONTENT,
    DISMISS_POPUP,
    DELETE_ELEMENT,
    MARK_TO_DELETE,
    UNMARK_TO_DELETE,
    NO_ITEM_MARKED,
    ITEM_MARKED,
    SESSION_DELETE_FAILED,
    PERK_RANGE_CHANGE,
    OPEN_SESSION,
    OPEN_EDIT_SESSION,
    SESSION_LIST_SIZE_CHANGED,
    // messages sur l'utilisateur courant
    USER_LOGGED_OUT,
    USER_CURRENT_UPDATED,
    USER_CURRENT_DELETED,
    USER_CURRENT_LOCALLY_UPDATED, // l'utilisateur local a été modifié, mais pas poussé au serveur
    // messages sur les appareils de l'utilisateur courant
    DEVICE_ADDED,
    DEVICE_UPDATED,
    DEVICE_LOCALLY_UPDATED,
    DEVICE_DELETED,
    // message sur la connexion BLE
    DEVICE_CONNECTION_CHANGED,
    RESTART_SCAN,
    DEVICE_STATE_UPDATED,
    SESSION_ADDED,
    SESSION_UPDATED,
    SYNC_EMPTY,
    SYNC_NO_WAVES,
    USER_CURRENT_SAVED,
    SYNC_FAILED,
    // Message envoyé lorsque l'application peut fonctionner avec la dernière version de l'API
    APPLICATION_UP_TO_DATE,
    APPLICATION_OUTDATED,
    CONFIGURATION_UPDATED,
    // Message envoyé lors d'un changement de sport courant
    CURRENT_SPORT_UPDATED,
    CHANGE_SPORT_REQUEST,
    DEVICE_NOT_RESPONDING, // le boitier ne répond plus aux commandes states
    DELETE_FILE, OPEN_URL
}
