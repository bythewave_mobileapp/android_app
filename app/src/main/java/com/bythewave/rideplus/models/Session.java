package com.bythewave.rideplus.models;

import android.location.Address;
import android.location.Geocoder;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.enums.Sport;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;
import com.socks.library.KLog;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * com.by_the_wave.ride_plus_android.models.Session
 * <p/>
 * Modèle d'une session
 * <p>
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 24/01/2018
 */
@SuppressWarnings("WeakerAccess")
public class Session implements Parcelable {

    //constantes utilisée pour l'import JSON
    private static final String JSON_SESSION = "sessions";
    private static final String JSON_DATE = "date";
    private static final String JSON_TIME = "time";
    private static final String JSON_SPORT = "sport";
    private static final String JSON_DURATION = "duration";
    private static final String JSON_LATITUDE = "lat";
    private static final String JSON_LONGITUDE = "lon";
    private static final String JSON_WAVE = "waves";

    public String spotName; // Nom du spot où s'est déroulé la session
    public String cityName; // Nom de la ville où s'est déroulé la session
    public String countryName; // Nom du pays où s'est déroulé la session
    public GeoPoint position; // position GPS de la session
    public Date startedAt; // date de début de la session
    public String userId; // identifiant de l'utilisateur lié à cette session
    public String comment; // commentaire de l'utilisateur
    public Sport sport; // sport associé à la session
    public String deviceId; // identifiant de l'appareil ayant servi à enregistrer la session
    public List<Wave> waves; // liste des vagues ou rides de la session
    public double duration;  // durée de la session en secondes
    public Date syncAt; // Date de synchronisation
    public boolean isViewed; // Marqueur indiquant si la session a été consultée
    public boolean isFavorite; // Marqueur indiquant si la session a été marqué comme favorite
    public boolean toDelete; // Marqueur indiquant si la session doit être supprimée


    /**
     * Constructeur vide
     */
    public Session() {
        //par défaut, les sessions sont pr le surf. cela permet de traiter les sessions faites avec la V1, qui n'avait pas de sport défini.
        sport = Sport.SURF;
    }

    /**
     * Créé une nouvelle session avec le téléphone
     *
     * @return la nouvelle session
     */
    public static Session createWithPhone() {
        Session session = new Session();
        session.sport = Sport.SNOW;
        session.deviceId = Constants.PHONE_DEVICE_NAME;
        session.userId = RidePlusApplication.currentUserManager.getCurrentUserId();
        session.waves = new ArrayList<>();
        session.comment = "";

        session.isViewed = false;
        session.isFavorite = false;
        session.toDelete = false;
        session.spotName = "";
        session.countryName = "";
        session.cityName = "";

        return session;
    }

    /**
     * Créé une liste de sessions à partir du tableau JSON en paramètre
     *
     * @param rootJsonString la chaine de caractère contenant le JSON des sessions
     * @param deviceId       Id du device ayant généré la session
     * @return la liste des sessions
     * @throws JSONException lorsque l'exploitation du tableau JSON échoue
     */
    public static List<Session> createFromJson(String rootJsonString, String deviceId) throws JSONException {
        List<Session> sessions = new ArrayList<>();

        JSONObject rootJson = new JSONObject(rootJsonString);

        if (rootJson.has(JSON_SESSION)) {

            JSONArray sessionsJson = rootJson.getJSONArray(JSON_SESSION);

            if (sessionsJson.length() == 0) return null;

            for (int index = 0; index < sessionsJson.length(); index++) {
                JSONObject sessionJson = sessionsJson.getJSONObject(index);

                Session session = new Session();


                if (sessionJson.has(JSON_DATE) && sessionJson.has(JSON_TIME)) {
                    DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yy HH:mm:ss Z");
                    session.startedAt = fmt.parseDateTime(String.format("%s %s Z", sessionJson.getString(JSON_DATE), sessionJson.getString(JSON_TIME))).toDate();
                }
                if (sessionJson.has(JSON_LATITUDE) && sessionJson.has(JSON_LONGITUDE)) {
                    session.position = new GeoPoint(sessionJson.getDouble(JSON_LATITUDE), sessionJson.getDouble(JSON_LONGITUDE));
                }


                session.userId = RidePlusApplication.currentUserManager.getCurrentUserId();

                session.comment = "";

                session.syncAt = DateTime.now().toDate();
                session.isViewed = false;
                session.isFavorite = false;
                session.toDelete = false;
                session.deviceId = deviceId;

                if (sessionJson.has(JSON_DURATION))
                    session.duration = sessionJson.getDouble(JSON_DURATION);

                // par défaut, les sessions sont de surf
                if (sessionJson.has(JSON_SPORT))
                    session.sport = Sport.fromString(sessionJson.getString(JSON_SPORT));
                else
                    session.sport = Sport.SURF;

                session.spotName = "";
                session.countryName = "";
                session.cityName = "";
                //on appele l'API maps pour tenter de compléter les 3 champs au dessus
                try {
                    session.getGeoNames();
                } catch (IOException exception) {
                    // si la récupération du nom échoue, on se contente de logguer l'erreur
                    KLog.e(exception);
                }


                if (sessionJson.has(JSON_WAVE)) {
                    session.waves = Wave.createFromJson(sessionJson.getJSONArray(JSON_WAVE));
                }

                //si une session a des vagues, on l'ajoute à la liste des nouvelles
                if (!session.waves.isEmpty())
                    sessions.add(session);
            }
        }

        return sessions;
    }

    /**
     * Appelle l'API Google maps pour récupérer le nom de la ville, pays et spot pour une position GPS donnée
     *
     * @throws IOException si la connexion avec l'API Google Maps échoue
     */
    @Exclude
    public void getGeoNames() throws IOException {
        if (RidePlusApplication.applicationContext != null && this.position != null) {
            Geocoder geoCoder = new Geocoder(RidePlusApplication.applicationContext);
            List<Address> matches = geoCoder.getFromLocation(this.position.getLatitude(), this.position.getLongitude(), 1);
            Address address = (matches.isEmpty() ? null : matches.get(0));

            if (address != null) {
                this.cityName = TextUtils.isEmpty(address.getLocality()) ? TextUtils.isEmpty(address.getSubLocality()) ? "" : address.getSubLocality() : address.getLocality();
                this.countryName = TextUtils.isEmpty(address.getCountryName()) ? "" : address.getCountryName();
            }

        }
    }

    /**
     * Renvoi l'id de la session sur le serveur
     *
     * @return l'id de la session sur le serveur
     */
    @Exclude
    public String getSessionId() {
        return this.userId + "_" + this.startedAt.getTime();
    }

    /**
     * Renvoi le nom de la session. Le nom de la session est constitué du nom du spot ou de la ville
     *
     * @return le nom de la session
     */

    @Exclude
    public String getName() {
        String name = this.isFavorite ? "★ " : "";
        if (TextUtils.isEmpty(this.spotName)) {
            if (TextUtils.isEmpty(this.cityName)) {
                name += "";
            } else {
                name += this.cityName;
            }
        } else {
            name += this.spotName;
        }
        return name;
    }

    /**
     * Renvoi une chaine de caractères présentant le nom de la ville et du pays
     *
     * @return la chaine de caractères présentant le nom de la ville et du pays
     */
    @Exclude
    public String getCityCountry() {
        if (TextUtils.isEmpty(cityName) && TextUtils.isEmpty(countryName)) {
            return RidePlusApplication.applicationContext.getString(R.string.default_city_country);
        } else if (TextUtils.isEmpty(cityName)) {
            return countryName;
        } else if (TextUtils.isEmpty(countryName)) {
            return cityName;
        } else {
            return String.format(RidePlusApplication.applicationContext.getString(R.string.city_country), cityName, countryName);
        }
    }

    /**
     * Renvoi le niveau moyen de la session
     *
     * @return le niveau moyen de la session
     */
    @Exclude
    public int getAverageLevel() {
        int avgLevel = 0;
        for (Wave wave : this.waves) {
            if (wave != null)
                avgLevel += wave.level;
        }
        return avgLevel / (this.waves.size() > 0 ? this.waves.size() : 1);
    }

    /**
     * Renvoi la durée globale de la session
     *
     * @return la durée globale de la session
     */
    @Exclude
    public double getDuration() {
        if (this.duration > 0)
            return this.duration;
        else {
            double waveSumDuration = 0;
            for (Wave wave : waves) {
                if (wave != null)
                    waveSumDuration += wave.duration;
            }
            return waveSumDuration;
        }
    }

    /**
     * Renvoi la liste des vagues non supprimées
     *
     * @return la liste des vagues visibles
     */
    @Exclude
    public List<Wave> getVisibleWaves() {
        List<Wave> visibleWaves = new ArrayList<>();
        for (Wave wave : waves) {
            if (wave != null && !wave.toDelete)
                visibleWaves.add(wave);
        }
        return visibleWaves;
    }

    /**
     * Renvoi vrai si la session n'est pas à supprimer et si elle possède au moins une vague qui ne soit pas à supprimer
     *
     * @return vrai si la session est visible, faux sinon
     */
    @Exclude
    public boolean isVisible() {
        return !toDelete && getVisibleWaves().size() > 0;
    }


    /**
     * Calcule les statistiques du nouveau ride : durée
     */
    public void computeStatistics() {
        if (this.waves != null && !this.waves.isEmpty()) {
            if (this.waves.get(0) != null)
                this.startedAt = this.waves.get(0).startedAt;
            if (this.startedAt != null && this.waves.get(this.waves.size() - 1) != null && this.waves.get(this.waves.size() - 1).startedAt != null)
                this.duration = (this.waves.get(this.waves.size() - 1).startedAt.getTime() - this.startedAt.getTime() + this.waves.get(this.waves.size() - 1).duration) / 1000f;
        }
        this.syncAt = DateTime.now().toDate();
        //on appele l'API maps pour tenter de compléter les 3 champs au dessus
        try {
            this.getGeoNames();
        } catch (IOException exception) {
            // si la récupération du nom échoue, on se contente de logguer l'erreur
            KLog.e(exception);
        }
    }


    /**
     * Indique si la session est faite avec le téléphone
     *
     * @return vrai si la session est faite avec le téléphone
     */
    @Exclude
    public boolean isPhoneSession() {
        return this.deviceId != null && this.deviceId.equals(Constants.PHONE_DEVICE_NAME);
    }

    /****************************************************************************************
     *                        Getter & setter pour Firestore                                *
     ****************************************************************************************/

    public String getSport() {
        return sport.value();
    }

    public void setSport(String sport) {
        this.sport = Sport.fromString(sport);
    }

    /****************************************************************************************
     *                                  Interface Parcelable                                *
     ****************************************************************************************/


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.spotName);
        dest.writeString(this.cityName);
        dest.writeString(this.countryName);
        if (this.position != null)
            dest.writeDouble(this.position.getLatitude());
        else
            dest.writeDouble(0.0f);
        if (this.position != null)
            dest.writeDouble(this.position.getLongitude());
        else
            dest.writeDouble(0.0f);
        dest.writeLong(this.startedAt != null ? this.startedAt.getTime() : -1);
        dest.writeString(this.userId);
        dest.writeString(this.comment);
        dest.writeInt(this.sport == null ? -1 : this.sport.ordinal());
        dest.writeString(this.deviceId);
        dest.writeTypedList(this.waves);
        dest.writeDouble(this.duration);
        dest.writeLong(this.syncAt != null ? this.syncAt.getTime() : -1);
        dest.writeByte(this.isViewed ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isFavorite ? (byte) 1 : (byte) 0);
        dest.writeByte(this.toDelete ? (byte) 1 : (byte) 0);
    }

    protected Session(Parcel in) {
        this.spotName = in.readString();
        this.cityName = in.readString();
        this.countryName = in.readString();
        Double lat = in.readDouble();
        Double lng = in.readDouble();
        this.position = new GeoPoint(lat, lng);
        long tmpStartedAt = in.readLong();
        this.startedAt = tmpStartedAt == -1 ? null : new Date(tmpStartedAt);
        this.userId = in.readString();
        this.comment = in.readString();
        int tmpSport = in.readInt();
        this.sport = tmpSport == -1 ? null : Sport.values()[tmpSport];
        this.deviceId = in.readString();
        this.waves = in.createTypedArrayList(Wave.CREATOR);
        this.duration = in.readDouble();
        long tmpSyncAt = in.readLong();
        this.syncAt = tmpSyncAt == -1 ? null : new Date(tmpSyncAt);
        this.isViewed = in.readByte() != 0;
        this.isFavorite = in.readByte() != 0;
        this.toDelete = in.readByte() != 0;
    }

    public static final Creator<Session> CREATOR = new Creator<Session>() {
        @Override
        public Session createFromParcel(Parcel source) {
            return new Session(source);
        }

        @Override
        public Session[] newArray(int size) {
            return new Session[size];
        }
    };

}