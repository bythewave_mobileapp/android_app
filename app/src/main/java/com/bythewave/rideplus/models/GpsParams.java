package com.bythewave.rideplus.models;

import com.bythewave.rideplus.common.Constants;

/**
 * com.bythewave.rideplus.models.GpsParams
 * <p/>
 * <p>
 * Modèle des paramètres de l'algorithme GPS stocké sur Firebase
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 17/12/2018
 */
public class GpsParams {

    public float vertical_speed_up;
    public float vertical_speed_down;
    public float horizontal_speed_up;
    public float horizontal_speed_down;
    public int sample_size;

    public GpsParams(float vertical_speed_up, float vertical_speed_down, float horizontal_speed_up, float horizontal_speed_down, int sample_size) {
        this.vertical_speed_up = vertical_speed_up;
        this.vertical_speed_down = vertical_speed_down;
        this.horizontal_speed_up = horizontal_speed_up;
        this.horizontal_speed_down = horizontal_speed_down;
        this.sample_size = sample_size;
    }

    public GpsParams() {
        this.vertical_speed_up = Constants.VERTICAL_SPEED_UP_THRESHOLD;
        this.vertical_speed_down = Constants.VERTICAL_SPEED_DOWN_THRESHOLD;
        this.horizontal_speed_up = Constants.HORIZONTAL_SPEED_UP_THRESHOLD;
        this.horizontal_speed_down = Constants.HORIZONTAL_SPEED_DOWN_THRESHOLD;
        this.sample_size = Constants.SPEED_SAMPLE_COUNT;
    }
}
