package com.bythewave.rideplus.models;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.bythewave.rideplus.models.enums.Direction;
import com.bythewave.rideplus.models.enums.Stance;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * com.by_the_wave.ride_plus_android.models.Wave
 * <p/>
 * Modèle d'un ride / vague
 * <p>
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 07/02/2018
 */
@SuppressWarnings("WeakerAccess")
public class Wave implements Parcelable {

    //constantes utilisée pour l'import JSON
    private static final String JSON_DATE = "date";
    private static final String JSON_TIME = "time";
    private static final String JSON_LATITUDE = "lat";
    private static final String JSON_LONGITUDE = "lon";
    private static final String JSON_MOMENTS = "moments";
    private static final String JSON_LEVEL = "level";
    private static final String JSON_DISTANCE = "distance";
    private static final String JSON_DIRECTION = "direction";
    private static final String JSON_SPEED = "speed";
    private static final String JSON_DURATION = "duration";

    public GeoPoint position; // position GPS de la session
    public Date startedAt; // date de début de la session
    public double duration;  // durée de la session en milli-secondes
    public int level; // niveau atteint par l'utilisateur sur la session
    public double distance; // distance parcourue en m
    public double speed; // vitesse maximale sur le ride en km/h
    public boolean toDelete; // Indique si la ride doit être supprimé
    public Direction direction; // direction de la ride (gauche/droite)
    public List<Moment> moments; // liste des moments caractéristiques de la ride

    /**
     * Constructeur vide
     */
    public Wave() {
    }

    /**
     * Créé une liste de rides à partir du tableau JSON en paramètre
     *
     * @param wavesJson le tableau JSON contenant les rides
     * @return la liste des rides
     * @throws JSONException lorsque l'exploitation du tableau JSON échoue
     */
    public static List<Wave> createFromJson(JSONArray wavesJson) throws JSONException {
        List<Wave> waves = new ArrayList<>();

        for (int index = 0; index < wavesJson.length(); index++) {
            JSONObject waveJson = wavesJson.getJSONObject(index);

            Wave wave = new Wave();

            wave.toDelete = false;

            if (waveJson.has(JSON_DATE)) {
                DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yy HH:mm:ss");
                wave.startedAt = fmt.parseDateTime(String.format("%s %s", waveJson.getString(JSON_DATE), waveJson.getString(JSON_TIME))).toDate();
            }
            if (waveJson.has(JSON_LATITUDE) && waveJson.has(JSON_LONGITUDE)) {
                wave.position = new GeoPoint(waveJson.getDouble(JSON_LATITUDE), waveJson.getDouble(JSON_LONGITUDE));
            }

            if (waveJson.has(JSON_DIRECTION)) {
                wave.direction = Direction.fromString(waveJson.getString(JSON_DIRECTION));
            }

            if (waveJson.has(JSON_DURATION)) {
                wave.duration = waveJson.getDouble(JSON_DURATION);
            }

            if (waveJson.has(JSON_DISTANCE)) {
                wave.distance = waveJson.getDouble(JSON_DISTANCE);
            }

            if (waveJson.has(JSON_SPEED)) {
                wave.speed = waveJson.getDouble(JSON_SPEED);
            }

            if (waveJson.has(JSON_LEVEL)) {
                wave.level = waveJson.getInt(JSON_LEVEL);
            }

            if (waveJson.has(JSON_MOMENTS)) {
                wave.moments = Moment.createFromJson(waveJson.getJSONArray(JSON_MOMENTS));
            }

            // si le niveau de la ride est nul, c'est qu'un problème s'est produit à l'enregistrement.
            // on ne l'ajoute pas par conséquent
            // edit : idem si la vitesse moyenne de la vague est supérieure à la vitesse maximale
            if (wave.level > 0 && wave.getAverageSpeed() <= wave.getMaxSpeed())
                waves.add(wave);
        }

        return waves;
    }

    /**
     * Créé un ride avec le téléphone.
     *
     * @return un nouveau ride
     */
    public static Wave createWithPhone() {
        Wave wave = new Wave();
        wave.toDelete = false;
        wave.position = null;
        //le champ ne sera employé en Snow
        wave.direction = Direction.LEFT;
        wave.duration = 0;
        wave.distance = 0;
        wave.speed = 0;
        wave.level = 0;
        wave.moments = new ArrayList<>();
        return wave;
    }

    /**
     * Indique si la ride est surfé frontside ou backside
     *
     * @param stance la stance du surfer
     * @return vrai si la ride est surfé frontside, faux sinon
     */
    @Exclude
    public boolean isFrontside(Stance stance) {
        return (direction == Direction.LEFT && stance == Stance.GOOFY) || (direction == Direction.RIGHT && stance == Stance.REGULAR);
    }

    /**
     * Calcule les statistiques du nouveau ride : distance, durée, vitesse moyenne.
     */
    public void computeStatistics() {
        if (this.moments != null && !this.moments.isEmpty()) {
            if (this.moments.get(this.moments.size() - 1) != null && this.moments.get(0) != null) {
                this.startedAt = new Date(this.moments.get(0).loggedAt);
                this.duration = (this.moments.get(this.moments.size() - 1).loggedAt - this.moments.get(0).loggedAt) / 1000f;
            }
        } else {
            this.duration = 0;
        }

        this.speed = 0;

        //on calcule la distance entre chaque point
        Moment previousMoment = null;
        if (moments != null)
            for (Moment moment : moments) {
                if (previousMoment != null) {
                    float[] results = new float[1];
                    Location.distanceBetween(moment.position.getLatitude(), moment.position.getLongitude(), previousMoment.position.getLatitude(), previousMoment.position.getLongitude(), results);
                    float shortDistance = results[0];
                    distance += Float.valueOf(shortDistance).doubleValue();

                    //calcul de la vitesse max
                    float shortDuration = Long.valueOf(moment.loggedAt - previousMoment.loggedAt).floatValue() / 1000f;
                    if (shortDuration > 0 && shortDistance / shortDuration > this.speed) {
                        this.speed = Float.valueOf(shortDistance / shortDuration * 3.6f).doubleValue();
                    }

                }
                previousMoment = moment;
            }

    }

    /**
     * @return la vitesse moyenne du ride
     */
    @Exclude
    public int getAverageSpeed() {
        return this.duration > 0 ? Math.round((float) this.distance / (float) this.duration * 3.6f) : 0;
    }

    /**
     * @return la vitesse maximale du ride
     */
    @Exclude
    public int getMaxSpeed() {
        return Long.valueOf(Math.round(this.speed)).intValue();
    }

    /****************************************************************************************
     *                        Getter & setter pour Firestore                                *
     ****************************************************************************************/

    public String getDirection() {
        return direction.value();
    }

    public void setDirection(String direction) {
        this.direction = Direction.fromString(direction);
    }

    /****************************************************************************************
     *                                  Interface Parcelable                                *
     ****************************************************************************************/


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (this.position != null)
            dest.writeDouble(this.position.getLatitude());
        else
            dest.writeDouble(0.0f);
        if (this.position != null)
            dest.writeDouble(this.position.getLongitude());
        else
            dest.writeDouble(0.0f);
        dest.writeLong(this.startedAt != null ? this.startedAt.getTime() : -1);
        dest.writeDouble(this.duration);
        dest.writeString(this.direction.value());
        dest.writeInt(this.level);
        dest.writeDouble(this.distance);
        dest.writeDouble(this.speed);
        dest.writeList(this.moments);
        dest.writeByte(this.toDelete ? (byte) 1 : (byte) 0);
    }

    protected Wave(Parcel in) {
        Double lat = in.readDouble();
        Double lng = in.readDouble();
        this.position = new GeoPoint(lat, lng);
        long tmpStartedAt = in.readLong();
        this.startedAt = tmpStartedAt == -1 ? null : new Date(tmpStartedAt);
        this.duration = in.readDouble();
        this.direction = Direction.fromString(in.readString());
        this.level = in.readInt();
        this.distance = in.readDouble();
        this.speed = in.readDouble();
        this.moments = new ArrayList<>();
        in.readList(this.moments, Moment.class.getClassLoader());
        this.toDelete = in.readByte() != 0;
    }

    public static final Creator<Wave> CREATOR = new Creator<Wave>() {
        @Override
        public Wave createFromParcel(Parcel source) {
            return new Wave(source);
        }

        @Override
        public Wave[] newArray(int size) {
            return new Wave[size];
        }
    };

}
