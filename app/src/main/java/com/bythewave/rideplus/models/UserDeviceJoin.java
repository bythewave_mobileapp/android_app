package com.bythewave.rideplus.models;

/**
 * com.by_the_wave.ride_plus_android.models
 * <p/>
 * Modèle d'une jointure entre un utilisateur et un appareil
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 08/11/2018
 */
public class UserDeviceJoin {

    public String userId; // id de l'utilisateur
    public String deviceId; // id de l'appareil

    /**
     * Constructeur vide
     */
    public UserDeviceJoin() {
        userId = "";
        deviceId = "";
    }

    /**
     * Constructeur paramétré
     *
     * @param userId   id de l'utilisateur
     * @param deviceId id de l'appareil
     */
    public UserDeviceJoin(String userId, String deviceId) {
        this.userId = userId;
        this.deviceId = deviceId;
    }

}
