package com.bythewave.rideplus.models;

import java.util.HashMap;
import java.util.Map;

/**
 * com.bythewave.rideplus.models.Config
 * <p/>
 * <p>
 * Modèle d'une configuration de conseils.
 * <p>
 * La configuration contient les liens vers les fichiers de conseils, ainsi qu'un numéro de version pour le suivi.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 10/11/2018
 */
public class Advices {

    public String version; // plateform concerné par la config
    public Map<String, Map<String, String>> files; // liste des fichiers de conseils

    /**
     * Constructeur vide
     */
    public Advices() {
        version = "";
        files = new HashMap<>();
    }

}
