package com.bythewave.rideplus.models;


import com.bythewave.rideplus.models.enums.BusMessageType;

import org.greenrobot.eventbus.EventBus;

/**
 * BusMessage
 * <p>
 * Modèle d'une message de bus système
 * <p>
 *
 * @author L'Apptelier SARL
 * @date 08/09/17.
 */
public class BusMessage {

    public BusMessageType type; // le type de message
    public Object content; // un contenu additionnel en paramètre du message

    /**
     * Constrcteur vide
     */
    public BusMessage() {
    }

    /**
     * Constructeur paramétré
     *
     * @param type    le type de message
     * @param content le paramètre d'un message
     */
    public BusMessage(BusMessageType type, Object content) {
        this.type = type;
        this.content = content;
    }

    /**
     * Constructeur simple
     *
     * @param type le type de message
     */
    public BusMessage(BusMessageType type) {
        this.type = type;
        this.content = null;
    }

    /**
     * Poste un message sur le bus système
     *
     * @param type    le type de message
     * @param content le paramètre d'un message
     */
    public static void post(BusMessageType type, Object content) {
        if (content != null)
            EventBus.getDefault().post(new BusMessage(type, content));
        else
            EventBus.getDefault().post(new BusMessage(type));
    }

    /**
     * Poste un message sur le bus système
     *
     * @param type le type de message
     */
    public static void post(BusMessageType type) {
        EventBus.getDefault().post(new BusMessage(type));
    }

    /**
     * Poste un message sur le bus système avec un délai
     *
     * @param type    le type de message
     * @param content le paramètre d'un message
     * @param delay   le délai avant envoi
     */
    public static void postDelayed(BusMessageType type, Object content, int delay) {
        new android.os.Handler().postDelayed(() -> {
            if (content != null)
                EventBus.getDefault().post(new BusMessage(type, content));
            else
                EventBus.getDefault().post(new BusMessage(type));
        }, delay);
    }

    /**
     * Poste un message sur le bus système avec un délai
     *
     * @param type  le type de message
     * @param delay le délai avant envoi
     */
    public static void postDelayed(BusMessageType type, int delay) {
        new android.os.Handler().postDelayed(() -> EventBus.getDefault().post(new BusMessage(type)), delay);
    }


}
