package com.bythewave.rideplus.models;

/**
 * com.by_the_wave.ride_plus_android.models.Perk
 * <p/>
 * Modèle d'une mesure ou d'une statistique, telle la longueur d'une vague, la durée d'une session, etc.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 17/02/2018
 */
public class Perk {

    public String name;
    public String value;
    public boolean isLevel;

    /**
     * Constructeur paramétré
     *
     * @param name    le nom de la mesure
     * @param value   la valeur de la mesure
     * @param isLevel vrai la mesure représente un niveau de surf, faux sinon
     */
    public Perk(String name, String value, boolean isLevel) {
        this.name = name;
        this.value = value;
        this.isLevel = isLevel;
    }
}
