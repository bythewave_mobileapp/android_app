package com.bythewave.rideplus.models;

/**
 * com.by_the_wave.ride_plus_android.models.Perk
 * <p/>
 * Modèle d'une paire de mesures ou de statistiques, telle la longueur d'une vague, la durée d'une session, etc.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 17/02/2018
 */
public class PerkPair {

    public Perk leftPair;
    public Perk rightPair;

    /**
     * Constructeur paramétré
     *
     * @param leftPair  la statistique à gauche de la paire
     * @param rightPair la statistique à droite de la paire
     */
    public PerkPair(Perk leftPair, Perk rightPair) {
        this.leftPair = leftPair;
        this.rightPair = rightPair;
    }
}
