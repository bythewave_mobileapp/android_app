package com.bythewave.rideplus.models.deserializers;

import com.bythewave.rideplus.models.enums.Direction;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Classe permettant de sérialiser ou deserialiser un énuméré de direction en utilisant GSON
 *
 * @author L'ApptelierSARL
 * @date 08/11/2018
 */
public class DirectionDeserializer implements JsonDeserializer<Direction> {
    @Override
    public Direction deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        Direction[] directions = Direction.values();
        for (Direction direction : directions) {
            if (direction.value.toLowerCase().equals(json.getAsString().toLowerCase()))
                return direction;
            else
                return Direction.LEFT;
        }
        return null;
    }
}
