package com.bythewave.rideplus.models.deserializers;

/**
 * com.by_the_wave.ride_plus_android.models.GeoPointDeserializer
 * <p/>
 * <p>
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 12/06/2018
 */

import com.google.firebase.firestore.GeoPoint;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Classe permettant de sérialiser ou deserialiser un énuméré d'une position géographique en utilisant GSON
 */
public class GeoPointDeserializer implements JsonDeserializer<GeoPoint>, JsonSerializer<GeoPoint> {
    @Override
    public GeoPoint deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        if (((JsonObject) json).has("latitude") && ((JsonObject) json).has("longitude")) {
            return new GeoPoint(((JsonObject) json).get("latitude").getAsDouble(), ((JsonObject) json).get("longitude").getAsDouble());
        }
        return new GeoPoint(0, 0);
    }

    @Override
    public JsonElement serialize(GeoPoint src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.addProperty("latitude", Double.toString(src.getLatitude()));
        result.addProperty("longitude", Double.toString(src.getLongitude()));
        return result;
    }
}
