package com.bythewave.rideplus.models;

import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.enums.Sport;
import com.google.firebase.firestore.Exclude;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * com.by_the_wave.ride_plus_android.models
 * <p/>
 * Modèle d'un appareil By The Wave
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 26/01/2018
 */
public class Device {

    // Constante pour le transfert d'état avec le boitier
    private static final String SPORT = "sport";
    private static final String HARDWARE_REV = "hardwareRev";
    private static final String FIRMWARE_REV = "firmwareRev";
    private static final String MANUFACTURER = "manufacturer";
    private static final String MODEL = "model";
    private static final String AUTH = "auth";

    public String model; // Nom du modèle d'appareil
    public String serial; // Numéro de série de l'appareil
    public String hardwareRev; // Version de l'appareil
    public String firmwareRev; // Version du logiciel interne
    public String manufacturer; // Nom du fabriquant
    public String macAdress; //Adresse MAC de l'appareil
    public String customName; // Nom donné par l'utilisateur
    public Sport sport; // Sport actuel du boitier
    public int batteryLevel; // Niveau de batterie de l'appareil (0 - 100)
    public Date lastSyncAt; // Date de dernière synchronisation

    /**
     * Constructeur vide
     */
    public Device() {
        model = "";
        serial = "";
        hardwareRev = "";
        firmwareRev = "";
        manufacturer = "";
        macAdress = "";
        customName = "";
        sport = Sport.SURF;
        batteryLevel = -1;
        lastSyncAt = DateTime.now().toDate();
    }

    /**
     * Constructeur simple
     *
     * @param macAdress l'adresse mac de l'appareil
     */
    public Device(String macAdress) {
        this.macAdress = macAdress;
        this.customName = null;
        this.model = "";
        this.serial = macAdress.replace(":", "");
        this.hardwareRev = "";
        this.firmwareRev = "";
        this.manufacturer = "";
        this.sport = Sport.SURF;
        this.batteryLevel = -1;
        this.lastSyncAt = DateTime.now().toDate();
    }

    /**
     * Met à jour les informations de l'appareil à partir des caractéristiques lues en bluetooth
     *
     * @param model        le modèle d'appareil
     * @param serial       le numéro de série de l'appareil
     * @param firmwareRev  la version du logiciel interne
     * @param hardwareRev  la version matérielle
     * @param manufacturer le nom du fabriquant
     */
    public <R, T1, T2, T3, T4, T5> R updateFromCharateristics(T1 model, T2 serial, T3 firmwareRev, T4 hardwareRev, T5 manufacturer) {
        this.model = new String((byte[]) model);
        this.serial = new String((byte[]) serial);
        this.manufacturer = new String((byte[]) manufacturer);
        this.hardwareRev = new String((byte[]) hardwareRev);
        this.firmwareRev = new String((byte[]) firmwareRev);
        return (R) this;
    }

    /**
     * Renvoi l'état du boitier pour configurer le boitier physique
     *
     * @return l'état du boitier sous forme d'une chaine de caractères contenant un JSON
     */
    @Exclude
    public String getState() {
        Map<String, Object> state = new HashMap<>();

        state.put(SPORT, this.sport.value());

        return new JSONObject(state).toString();
    }

    /**
     * Modifie le modèle du boitier en fonction de l'état du boitier physique en paramètre
     *
     * @param state l'état du boitier physique sous forme d'une chaine de caractères contenant un JSON
     * @throws JSONException si l'exploitation du JSON en paramètre échoue
     */
    @Exclude
    public void setState(String state) throws JSONException {
        JSONObject stateJson = new JSONObject(state);
        if (stateJson.has(SPORT))
            this.sport = Sport.fromString(stateJson.getString(SPORT));
        if (stateJson.has(MODEL))
            this.model = stateJson.getString(MODEL);
        if (stateJson.has(HARDWARE_REV))
            this.hardwareRev = stateJson.getString(HARDWARE_REV);
        if (stateJson.has(FIRMWARE_REV))
            this.firmwareRev = stateJson.getString(FIRMWARE_REV);
        if (stateJson.has(MANUFACTURER))
            this.manufacturer = stateJson.getString(MANUFACTURER);
    }

    /**
     * Indique si le boitier est un boitier By the Wave
     *
     * @return vrai si le boitier est authentique, faux sinon
     */
    public boolean isAuthenticated(String state) {
        try {
            JSONObject stateJson = new JSONObject(state);
            return stateJson.has(AUTH) && stateJson.getString(AUTH).equals(Constants.UUID_AUTH);
        } catch (JSONException e) {
            return false;
        }
    }

    /****************************************************************************************
     *                        Getter & setter pour Firestore                                *
     ****************************************************************************************/

    public String getSport() {
        return sport.value();
    }

    public void setSport(String sport) {
        this.sport = Sport.fromString(sport);
    }
}
