package com.bythewave.rideplus.models.enums;

/**
 * com.bythewave.rideplus.models.enums.Direction
 * <p/>
 * <p>
 * Enuméré des directions d'un ride
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 07/02/2018
 */
// Énuméré des direction d'une ride
public enum Direction {
    LEFT("left"), // lorsque l'on observe la ride depuis la plage, le surfer part dans vers la gauche
    RIGHT("right"); // lorsque l'on observe la ride depuis la plage, le surfer part dans vers la droite

    public final String value;

    /**
     * Constructeur vide
     */
    Direction() {
        value = "left";
    }

    /**
     * Constructeur
     *
     * @param value la direction sous forme de chaine de caractères
     */
    Direction(String value) {
        this.value = value;
    }

    /**
     * Renvoi une direction en fonction d'une chaine de caractère
     *
     * @param value la chaine de caractère à évaluer
     * @return la direction si existante, null sinon
     */
    public static Direction fromString(String value) {
        if (value == null)
            return LEFT;
        for (Direction direction : Direction.values()) {
            if (direction.value.toLowerCase().equals(value.toLowerCase())) {
                return direction;
            }
        }
        return null;
    }

    /**
     * Renvoi la valeur en chaine de caractères
     *
     * @return la valeur en chaine de caractères
     */
    public String value() {
        return value;
    }

}
