package com.bythewave.rideplus.models.deserializers;

import com.bythewave.rideplus.models.enums.Sport;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Classe permettant de sérialiser ou deserialiser un énuméré de sport en utilisant GSON
 *
 * @author L'ApptelierSARL
 * @date 08/11/2018
 *
 */
public class SportDeserializer implements JsonDeserializer<Sport> {
    @Override
    public Sport deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        Sport[] sports = Sport.values();
        for (Sport sport : sports) {
            if (sport.value().toLowerCase().equals(json.getAsString().toLowerCase()))
                return sport;
        }
        return Sport.SURF;
    }
}
