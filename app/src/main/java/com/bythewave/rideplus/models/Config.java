package com.bythewave.rideplus.models;

import androidx.annotation.Nullable;

/**
 * com.bythewave.rideplus.models.Config
 * <p/>
 * <p>
 * Modèle d'une configuration d'application.
 * <p>
 * La configuration d'application contient les liens vers les fichiers de conseils.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 10/11/2018
 */
public class Config {

    public Advices advices; // configuration des fichiers de conseils

    @Nullable
    public String version; // version du fichier de configuration, utilisé par le fichier de config de l'API

    @Nullable
    public GpsParams gps_params; // paramètres du tracking GPS

    /**
     * Constructeur vide
     */
    private Config() {
        this.advices = new Advices();
        this.gps_params = new GpsParams();
    }

}
