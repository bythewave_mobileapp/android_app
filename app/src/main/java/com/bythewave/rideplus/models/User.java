package com.bythewave.rideplus.models;

import com.bythewave.rideplus.models.enums.Stance;

/**
 * com.by_the_wave.ride_plus_android.models
 * <p/>
 * Modèle d'un utilisateur
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 26/01/2018
 */
public class User {


    public String id; // id de l'utilisateur
    public String name; // Nom de l'utilisateur
    public String avatarUrl; // URL vers l'avatar de l'utilisateur
    public boolean toDelete; // Marqueur indiquant si l'utilisateur doit être supprimé
    public Stance stance; // Stance de l'utilisateur (goofy ou regular)

    /**
     * Constructeur vide
     */
    public User() {
        id = "";
        name = "";
        avatarUrl = "";
        stance = Stance.REGULAR;
        toDelete = false;
    }

    /****************************************************************************************
     *                        Getter & setter pour Firestore                                *
     ****************************************************************************************/

    public String getStance() {
        return stance.value();
    }

    public void setStance(String stance) {
        this.stance = Stance.fromString(stance);
    }
}
