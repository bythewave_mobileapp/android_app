package com.bythewave.rideplus.models.enums;

/**
 * com.bythewave.rideplus.models.enums.Stance
 * <p/>
 * <p>
 * Enuméré des Stances
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 26/01/2018
 */
public enum Stance {
    REGULAR("regular"), // Regular (pied gauche devant)
    GOOFY("goofy"); // Goofy (pied droit devant)

    private final String value;

    /**
     * Constructeur
     *
     * @param value la direction sous forme de chaine de caractères
     */
    Stance(String value) {
        this.value = value;
    }

    /**
     * Renvoi une stance en fonction d'une chaine de caractère
     *
     * @param value la chaine de caractère à évaluer
     * @return la stance si existante, null sinon
     */
    public static Stance fromString(String value) {
        if (value == null)
            return REGULAR;
        for (Stance stance : Stance.values()) {
            if (stance.value.toLowerCase().equals(value.toLowerCase())) {
                return stance;
            }
        }
        return null;
    }

    /**
     * Renvoi la valeur en chaine de caractères
     *
     * @return la valeur en chaine de caractères
     */
    public String value() {
        return value;
    }

}
