package com.bythewave.rideplus.models;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * com.by_the_wave.ride_plus_android.models.Moment
 * <p/>
 * Moment caractéristique d'une vague
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 07/02/2018
 */
public class Moment implements Parcelable {

    public static String MOMENT_DEFAULT = "Z";
    public static String MOMENT_PHONE = "P";

    //constantes utilisée pour l'import JSON
    private static final String JSON_LATITUDE = "lat";
    private static final String JSON_LONGITUDE = "lon";
    private static final String JSON_TYPE = "type";

    public GeoPoint position; // position GPS du moment caractéristique
    public String type; // définition du moment caractéristique

    @Exclude
    public long loggedAt; //date d'enregistrement du moment. Utilisé uniquement par l'algorithme d'enregistrement d'une session avec le téléphone.
    @Exclude
    public double altitude; //altitude du moment. Utilisé uniquement par l'algorithme d'enregistrement d'une session avec le téléphone.

    /**
     * Constructeur vide
     */
    private Moment() {
        type = MOMENT_DEFAULT;
    }

    /**
     * Créé une liste de moments caractéristiques à partir du tableau JSON en paramètre
     *
     * @param momentsJson le tableau JSON contenant les moments caractéristiques
     * @return la liste des moments caractéristiques
     * @throws JSONException lorsque l'exploitation du tableau JSON échoue
     */
    public static List<Moment> createFromJson(JSONArray momentsJson) throws JSONException {
        List<Moment> moments = new ArrayList<>();

        for (int index = 0; index < momentsJson.length(); index++) {
            JSONObject momentJson = momentsJson.getJSONObject(index);

            Moment moment = new Moment();


            if (momentJson.has(JSON_LATITUDE) && momentJson.has(JSON_LONGITUDE)) {
                moment.position = new GeoPoint(momentJson.getDouble(JSON_LATITUDE), momentJson.getDouble(JSON_LONGITUDE));
            }

            if (momentJson.has(JSON_TYPE)) {
                moment.type = momentJson.getString(JSON_TYPE);
            }

            moments.add(moment);
        }

        return moments;
    }

    /**
     * Créé un moment caractéristique à partir d'une position en paramètre
     *
     * @param location la position du moment
     * @return le moment caractéristique
     */
    public static Moment createFromLocation(Location location) {
        Moment moment = new Moment();
        try {
            if (location != null && !Double.isNaN(location.getLatitude()) && location.getLatitude() >= -90.0D && location.getLatitude() <= 90.0D &&
                    !Double.isNaN(location.getLongitude()) && location.getLongitude() >= -180.0D && location.getLongitude() <= 180.0D) {
                moment.position = new GeoPoint(location.getLatitude(), location.getLongitude());
                moment.loggedAt = location.getTime();
                moment.altitude = location.getAltitude();
            }
            moment.type = MOMENT_PHONE;
            return moment;
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }

    /****************************************************************************************
     *                                  Interface Parcelable                                *
     ****************************************************************************************/


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (this.position != null)
            dest.writeDouble(this.position.getLatitude());
        else
            dest.writeDouble(0.0f);
        if (this.position != null)
            dest.writeDouble(this.position.getLongitude());
        else
            dest.writeDouble(0.0f);
        dest.writeString(this.type);
    }

    protected Moment(Parcel in) {
        Double lat = in.readDouble();
        Double lng = in.readDouble();
        this.position = new GeoPoint(lat, lng);
        this.type = in.readString();
    }

    public static final Creator<Moment> CREATOR = new Creator<Moment>() {
        @Override
        public Moment createFromParcel(Parcel source) {
            return new Moment(source);
        }

        @Override
        public Moment[] newArray(int size) {
            return new Moment[size];
        }
    };
}
