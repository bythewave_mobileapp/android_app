package com.bythewave.rideplus.models.enums;

/**
 * com.bythewave.rideplus.models.enums.Sport
 * <p/>
 * Enuméré des différents sports utilisés dans l'application
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 08/11/2018
 */

public enum Sport {
    SURF("surf"), // Surf
    SNOW("snow"), // Snowboard
    SKATE("skate"); // Skateboard

    private final String value;

    /**
     * Constructeur
     *
     * @param value la sport sous forme de chaine de caractères
     */
    Sport(String value) {
        this.value = value;
    }

    /**
     * Renvoi une sport en fonction d'une chaine de caractère
     *
     * @param value la chaine de caractère à évaluer
     * @return la sport si existante, null sinon
     */
    public static Sport fromString(String value) {
        if (value == null)
            return SURF;
        for (Sport sport : Sport.values()) {
            if (sport.value.toLowerCase().equals(value.toLowerCase())) {
                return sport;
            }
        }
        return null;
    }

    /**
     * Renvoi la valeur en chaine de caractères
     *
     * @return la valeur en chaine de caractères
     */
    public String value() {
        return value.toLowerCase();
    }

}

