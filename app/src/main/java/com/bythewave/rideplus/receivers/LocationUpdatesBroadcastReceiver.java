/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bythewave.rideplus.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.Moment;
import com.bythewave.rideplus.models.deserializers.GeoPointDeserializer;
import com.google.android.gms.location.LocationResult;
import com.google.firebase.firestore.GeoPoint;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.socks.library.KLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Récepteur de positions GPS.
 * <p>
 * Ce récepteur récupère les positions GPS émises par le téléphone.
 * <p>
 * Passé Android 8.0 (O), l'exécution en arrière plan est sévèrement limitée. Il faut donc passer
 * par ce méchanisme pour récevoir des positions lorsque l'app n'est pas en arrière plan.
 * <p>
 * {@link android.app.PendingIntent#getBroadcast(Context, int, Intent, int)}
 * {@link com.google.android.gms.location.LocationRequest}
 */
public class LocationUpdatesBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (Constants.INTENT_PROCESS_LOCATION.equals(action)) {
                LocationResult locationResult = LocationResult.extractResult(intent);
                if (locationResult != null) {
                    try {
                        File file = new File(context.getFilesDir(), Constants.NEW_SESSION_FILE);
                        FileOutputStream outStream = new FileOutputStream(file, true);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outStream);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(GeoPoint.class, new GeoPointDeserializer());
                        Gson gson = gsonBuilder.create();
                        for (Location location : locationResult.getLocations()) {
                            if (location != null) {
                                Moment moment = Moment.createFromLocation(location);
                                if (moment != null) {
                                    String line = gson.toJson(moment);
                                    outputStreamWriter.write(line);
                                    outputStreamWriter.write("\n");
                                }
                            }
                        }
                        outputStreamWriter.close();
                        outStream.close();
                    } catch (IOException exception) {
                        KLog.e("GPS", exception);
                    }

                }
            }
        }
    }
}
