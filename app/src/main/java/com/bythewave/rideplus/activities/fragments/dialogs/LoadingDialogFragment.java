package com.bythewave.rideplus.activities.fragments.dialogs;

import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * com.by_the_wave.ride_plus_android.activities.fragments.dialogs.AddDeviceDialogFragment
 * <p/>
 * Affiche une fenêtre surgissante affichant un message d'attente
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 30/01/2018
 */
public class LoadingDialogFragment extends BaseDialogFragment {

    @BindView(R.id.loading_text)
    protected TextView loading;

    private String loadingText;

    /**
     * Renvoi une nvelle instance du framgent
     *
     * @param text le texte à afficher sur le dialogue
     * @return une nvelle instance du fragment
     */
    public static LoadingDialogFragment newInstance(String text) {
        LoadingDialogFragment fragment = new LoadingDialogFragment();
        Bundle args = new Bundle();
        args.putString(Constants.INTENT_LOADING_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Constructeur vide
     */
    public LoadingDialogFragment() {
        this.setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.loadingText = getArguments().getString(Constants.INTENT_LOADING_TEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_loading, container, true);
        ButterKnife.bind(this, view);

        loading.setText(loadingText);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getDialog() != null && getDialog().getWindow() != null && getContext() != null) {
            //on redimentionne le dialogue pr occuper tout l'écran horizontalement
            getDialog().getWindow().setLayout(AndroidUtils.getScreenWidth() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp48)), ViewGroup.LayoutParams.WRAP_CONTENT);

            getDialog().setCanceledOnTouchOutside(false);
            getDialog().setCancelable(false);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                getDialog().getWindow().setElevation(5f);
            }

           int transparentColor = ContextCompat.getColor(getContext(), android.R.color.transparent);
            getDialog().getWindow().setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{transparentColor, transparentColor}));
        }

    }

}
