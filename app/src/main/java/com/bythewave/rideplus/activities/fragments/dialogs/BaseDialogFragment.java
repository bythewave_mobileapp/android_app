package com.bythewave.rideplus.activities.fragments.dialogs;

import androidx.fragment.app.DialogFragment;
import android.text.TextUtils;
import android.widget.Toast;

import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.models.BusMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import es.dmoral.toasty.Toasty;


/**
 * com.by_the_wave.ride_plus_android.activities.fragments
 * <p/>
 * Fragment basique permettant de généraliser l'inscription des fragments au bus système
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 22/01/2018
 */
public class BaseDialogFragment extends DialogFragment {

    // nom de l'écran
    protected String screen = null;

    /**
     * Constructeur vide
     */
    public BaseDialogFragment() {
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        if (getActivity() != null) {
            if (TextUtils.isEmpty(screen))
                RidePlusApplication.firebaseAnalytics.setCurrentScreen(getActivity(), null, null);
            else
                RidePlusApplication.firebaseAnalytics.setCurrentScreen(getActivity(), screen, null);

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(BusMessage message) {
        if (getContext() != null) {
            switch (message.type) {
                case ERROR_BLE_CORE:
                case ERROR_BLE_STOP:
                    if (message.content instanceof String)
                        Toasty.error(getContext(), (CharSequence) message.content, Toast.LENGTH_LONG, true).show();

                    //on coupe les communication
                    RidePlusApplication.bluetoothDeviceManager.cancelSubscriptions();
                    break;
                case DEVICE_NOT_RESPONDING:
                    Toasty.error(getContext(), getString(R.string.error_message_device_not_responding), Toast.LENGTH_LONG, true).show();
                    //on coupe les communication
                    RidePlusApplication.bluetoothDeviceManager.cancelSubscriptions();
                    dismiss();
                    break;
                case ERROR_BLE_COMMAND_TIMEOUT:
                    Toasty.error(getContext(), getString(R.string.error_message_timeout_command_exception), Toast.LENGTH_LONG, true).show();
                    //on coupe les communication
                    RidePlusApplication.bluetoothDeviceManager.cancelSubscriptions();
                    dismiss();
                    break;
                case ERROR_DEVICE_DISCONNECTED:
                    Toasty.error(getContext(), getString(R.string.error_device_disconnected), Toast.LENGTH_LONG, true).show();
                    break;
                case ERROR_DEVICE_DATA:
                    Toasty.error(getContext(), getString(R.string.error_message_json_error), Toast.LENGTH_LONG, true).show();
                    break;
                case ERROR_DEVICE_STATE:
                    Toasty.error(getContext(), getString(R.string.error_message_state_error), Toast.LENGTH_LONG, true).show();
                    break;
                case ERROR_GENERIC_SERVER:
                    Toasty.error(getContext(), getString(R.string.error_unknown), Toast.LENGTH_LONG, true).show();
                    break;
                case ERROR_OFFLINE:
                    Toasty.error(getContext(), getString(R.string.error_offline), Toast.LENGTH_LONG, true).show();
                    break;
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStickyMessageEvent(BusMessage message) {
        //A surcharger
    }
}
