package com.bythewave.rideplus.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.BuildConfig;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.socks.library.KLog;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * fr.reseauscet.scet_app_android.activities.MailLoginActivity
 * <p/>
 * Activité permettant à l'utilisateur de s'autentifier par mail
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 19/01/2018
 */
public class MailLoginActivity extends BaseAppCompatActivity {

    @BindView(R.id.login_layout)
    protected ConstraintLayout layout;

    @BindView(R.id.login_email_field)
    EditText editTextEmail;

    @BindView(R.id.login_password_field)
    protected EditText editTextPassword;

    @BindView(R.id.login_logo_btw)
    protected ImageView imageViewLogo;

    @BindView(R.id.login_email_label)
    protected TextView labelEmail;

    @BindView(R.id.login_submit)
    protected Button buttonLogin;

    @BindView(R.id.login_forgotten_password)
    protected Button buttonForgottenPassword;

    public MailLoginActivity() {
        screen = Constants.SCREEN_LOGIN_MAIL;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_mail);
        ButterKnife.bind(this);

        //ajout d'un controle de saisie pour activer le bouton me connecter
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //on active le bouton Me connecter si le login et le mdp sont renseignés
                buttonLogin.setEnabled(!TextUtils.isEmpty(editTextEmail.getText()) && !TextUtils.isEmpty(editTextPassword.getText()) && editTextPassword.getText().length() > Constants.MIN_PASSWORD_LENGHT);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        editTextEmail.addTextChangedListener(textWatcher);
        editTextPassword.addTextChangedListener(textWatcher);

        //prise en compte du tap sur le bouton "go" du clavier lorsque l'on édite le mot de passe
        editTextPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_GO) {
                login(editTextPassword);
                return true;
            } else {
                return false;
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        //ajout de paramètre de debug
        if (BuildConfig.BUILD_TYPE.equals("debug")) {
            editTextEmail.setText("edouard.brethes@lapptelier.com");
            editTextPassword.setText("testtest");
        }
    }

    public void dismissKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @OnClick(R.id.login_submit)
    public void login(View view) {
        //on masque le clavier et on anime la vue
        dismissKeyboard(view);

        //on vérifie que les champs email et mot de passe soient correctement renseignés
        if (!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()) {
            BusMessage.post(BusMessageType.ERROR_WRONG_MAIL);
        } else if (editTextPassword.getText().toString().length() < Constants.MIN_PASSWORD_LENGHT) {
            BusMessage.post(BusMessageType.ERROR_WEAK_PASSWORD);
        } else {

            // on sauve en preferences le login/mdp
            FirebaseAuth.getInstance().signInWithEmailAndPassword(editTextEmail.getText().toString(), editTextPassword.getText().toString())
                    .addOnCompleteListener(this, loginUserTask -> {
                        if (loginUserTask.isSuccessful()) {
                            KLog.d("signInWithEmailAndPassword:success");
                            RidePlusApplication.currentUserManager.reload();
                        } else {
                            enableButtons(true);
                            KLog.w("signInWithEmail:failure", loginUserTask.getException());

                            if (loginUserTask.getException() instanceof FirebaseNetworkException) {
                                BusMessage.post(BusMessageType.ERROR_OFFLINE);
                            } else if (loginUserTask.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                BusMessage.post(BusMessageType.ERROR_WRONG_CREDENTIAL);
                            } else if (loginUserTask.getException() instanceof FirebaseAuthUserCollisionException) {
                                BusMessage.post(BusMessageType.ERROR_USER_COLLISION);
                            } else {
                                //on affiche une popup demandant à l'utilisateur de confirmer la création du compte
                                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.RidePlusSimpleAlertDialogTheme);
                                builder.setMessage(R.string.confirm_account_creation)
                                        .setPositiveButton(R.string.continue_label, (dialog, id) ->
                                        {
                                            // l'utilisateur a confirmé, on l'inscrit
                                            FirebaseAuth.getInstance().createUserWithEmailAndPassword(editTextEmail.getText().toString(), editTextPassword.getText().toString())
                                                    .addOnCompleteListener(MailLoginActivity.this, createUserTask -> {
                                                        if (createUserTask.isSuccessful()) {
                                                            RidePlusApplication.currentUserManager.createUser();
                                                            BusMessage.post(BusMessageType.USER_CURRENT_UPDATED);
                                                        } else {
                                                            if (createUserTask.getException() != null) {
                                                                KLog.e("signUpWithEmail:failure", createUserTask.getException().getCause());
                                                                if (createUserTask.getException() instanceof FirebaseNetworkException) {
                                                                    BusMessage.post(BusMessageType.ERROR_OFFLINE);
                                                                } else if (createUserTask.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                                                    BusMessage.post(BusMessageType.ERROR_WRONG_CREDENTIAL);
                                                                } else if (createUserTask.getException() instanceof FirebaseAuthUserCollisionException) {
                                                                    BusMessage.post(BusMessageType.ERROR_USER_COLLISION);
                                                                } else if (createUserTask.getException() instanceof FirebaseAuthWeakPasswordException) {
                                                                    BusMessage.post(BusMessageType.ERROR_WEAK_PASSWORD);
                                                                } else {
                                                                    BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                                                                }
                                                            }
                                                        }
                                                    });
                                        })
                                        .setNegativeButton(R.string.cancel, (dialog, id) -> KLog.i("User declined account creation"));
                                builder.create().show();
                            }
                        }
                    });

            //on désactive les boutons et active l'indicateur d'activité
            enableButtons(false);
        }
    }

    @OnClick({R.id.login_icon_previous, R.id.login_text_previous})
    public void backToLogin(View view) {
        //on anime la vue
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_pressed));

        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    /**
     * Méthode permettant d'activer ou désactiver les interactions de l'utilisateur avec l'ensemble des boutons de la vue.
     * En cas de désactivation, affiche l'indicateur d'activité
     *
     * @param enable vrai pour activer les boutons, faux pour les désactiver et afficher l'indicateur d'activité
     */
    private void enableButtons(boolean enable) {
        buttonLogin.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(enable ? 1f : 0f);
        editTextEmail.setEnabled(enable);
        editTextPassword.setEnabled(enable);
        buttonForgottenPassword.setEnabled(enable);
    }

    @OnClick(R.id.login_forgotten_password)
    public void displayForgottenPasswordActivity(View view) {
        //on affiche l'activité de saisie de l'email
        Intent intent = new Intent(RidePlusApplication.applicationContext, ForgottenPasswordActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        enableButtons(true);
        switch (message.type) {
            case USER_CURRENT_UPDATED:
                enableButtons(true);
                // Envoi de statistique
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.SIGN_UP_METHOD, "Mail");
                RidePlusApplication.firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                //on affiche l'activité principale
                Intent intent;
                if (RidePlusApplication.preferencesManager.shouldDisplayTutorial()) {
                    //TODO afficher le tuto
                    intent = new Intent(RidePlusApplication.applicationContext, MainActivity.class);
                } else {
                    intent = new Intent(RidePlusApplication.applicationContext, MainActivity.class);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                finish();
                break;
            case ERROR_USER_COLLISION:
                Toasty.error(this, getString(R.string.error_message_mail_user_collision), Toast.LENGTH_LONG, true).show();
                break;
            case ERROR_WRONG_CREDENTIAL:
                Toasty.error(this, getString(R.string.error_message_invalid_credential), Toast.LENGTH_LONG, true).show();
                break;
            case ERROR_WEAK_PASSWORD:
                Toasty.error(this, getString(R.string.error_message_weak_password), Toast.LENGTH_LONG, true).show();
                break;
            case ERROR_WRONG_MAIL:
                Toasty.error(this, getString(R.string.error_message_email_invalid), Toast.LENGTH_LONG, true).show();
                break;
        }
        super.onMessageEvent(message);
    }
}
