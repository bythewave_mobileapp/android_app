package com.bythewave.rideplus.activities.fragments.dialogs;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Device;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * com.by_the_wave.ride_plus_android.activities.fragments.dialogs.AddDeviceDialogFragment
 * <p/>
 * Affiche une fenêtre surgissante permettant d'ajouter un appareil
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 29/01/2018
 */
public class AddDeviceDialogFragment extends BaseDialogFragment {

    private static final int DEVICE_ON = 0;
    private static final int PERMISSION = 1;
    private static final int SCANNING = 2;
    private static final int COMPLETE = 3;

    @BindView(R.id.device_add_dot_0)
    protected ImageView dot0;
    @BindView(R.id.device_add_dot_1)
    protected ImageView dot1;
    @BindView(R.id.device_add_dot_2)
    protected ImageView dot2;

    @BindView(R.id.device_tutorial_button)
    protected Button button;

    @BindView(R.id.device_add_page_0)
    protected ConstraintLayout pageTutorial;
    @BindView(R.id.device_add_page_1)
    protected ConstraintLayout pageBluetooth;
    @BindView(R.id.device_add_page_2)
    protected ConstraintLayout pageSync;
    @BindView(R.id.device_add_page_3)
    protected ConstraintLayout pageComplete;

    @BindView(R.id.device_sync_image)
    protected ImageView syncImage;
    @BindView(R.id.device_sync_text)
    protected TextView syncText;
    @BindView(R.id.device_sync_pulse_0)
    protected ImageView pulse0;
    @BindView(R.id.device_sync_pulse_1)
    protected ImageView pulse1;
    @BindView(R.id.device_sync_pulse_2)
    protected ImageView pulse2;

    // la "page" de tutorial affichée
    int page;

    private PassCodeDialogFragment passCodeDialogFragment; // dialogue permettant à l'utilisateur de saisir le code d'accès du boitier

    /**
     * Renvoi une nvelle instance du framgent
     *
     * @return une nvelle instance du fragment
     */
    public static AddDeviceDialogFragment newInstance() {
        return new AddDeviceDialogFragment();
    }

    /**
     * Constructeur vide
     */
    public AddDeviceDialogFragment() {
        if (getActivity() != null)
            RidePlusApplication.firebaseAnalytics.setCurrentScreen(getActivity(), Constants.SCREEN_ADD_DEVICE, null);
        this.setStyle(STYLE_NO_TITLE, getTheme());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_device, container, true);
        ButterKnife.bind(this, view);

        syncImage.setImageResource(R.drawable.image_bluetooth);
        syncText.setText(getText(R.string.bluetooth_search));

        configureView();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (getDialog() != null && getDialog().getWindow() != null) {
            //on redimentionne le dialogue pr occuper tout l'écran
            getDialog().getWindow().setLayout(AndroidUtils.getScreenWidth() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp16)), AndroidUtils.getScreenHeight() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp32)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                getDialog().getWindow().setElevation(5f);
            }

           int transparentColor = ContextCompat.getColor(getContext(), android.R.color.transparent);
            getDialog().getWindow().setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{transparentColor, transparentColor}));
        }

        //par défaut, on affiche la première page
        page = DEVICE_ON;
    }

    @Override
    public void dismiss() {
        syncImage.clearAnimation();
        pulse0.clearAnimation();
        pulse1.clearAnimation();
        pulse2.clearAnimation();
        super.dismiss();
    }

    /**
     * Configure la vue selon la page courante
     */
    public void configureView() {

        dot0.setEnabled(false);
        dot1.setEnabled(false);
        dot2.setEnabled(false);
        dot0.setVisibility(View.VISIBLE);
        dot1.setVisibility(View.VISIBLE);
        dot2.setVisibility(View.VISIBLE);
        button.setText(R.string.next);
        button.setEnabled(true);

        switch (page) {
            case DEVICE_ON:
                dot0.setEnabled(true);
                pageTutorial.setAlpha(1f);
                pageBluetooth.setAlpha(0f);
                pageSync.setAlpha(0f);
                pageComplete.setAlpha(0f);
                break;
            case PERMISSION:
                dot1.setEnabled(true);
                pageTutorial.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(0f);
                pageBluetooth.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(1f);
                pageSync.setAlpha(0f);
                pageComplete.setAlpha(0f);
                break;
            case SCANNING:
                dot2.setEnabled(true);
                button.setText(R.string.finish);
                button.setEnabled(false);
                pageBluetooth.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(0f);
                pageSync.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(1f);
                Animation pulseAnim = AnimationUtils.loadAnimation(getContext(), R.anim.pulse);
                syncImage.startAnimation(pulseAnim);
                Animation expandFadeAnim0 = AnimationUtils.loadAnimation(getContext(), R.anim.expand_fade);
                pulse0.startAnimation(expandFadeAnim0);
                Animation expandFadeAnim1 = AnimationUtils.loadAnimation(getContext(), R.anim.expand_fade);
                expandFadeAnim1.setStartOffset(600);
                pulse1.startAnimation(expandFadeAnim1);
                Animation expandFadeAnim2 = AnimationUtils.loadAnimation(getContext(), R.anim.expand_fade);
                expandFadeAnim2.setStartOffset(1200);
                pulse2.startAnimation(expandFadeAnim2);

                //on vérifie si l'utilisateur a activé le Bluetooth
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    if (getActivity() != null && ContextCompat.checkSelfPermission(RidePlusApplication.applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(RidePlusApplication.applicationContext, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(RidePlusApplication.applicationContext, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED)
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN}, Constants.REQUEST_CODE_PERMISSIONS);
                    else startScan();
                else startScan();

                // Firebase log
                RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_SCAN_DEVICE, null);

                break;
            case COMPLETE:
                dot0.setVisibility(View.INVISIBLE);
                dot1.setVisibility(View.INVISIBLE);
                dot2.setVisibility(View.INVISIBLE);
                pageSync.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(0f);
                pageComplete.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(1f);
                button.setText(R.string.finish);

                // Firebase log
                RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_DEVICE_ADDED, null);
                break;
        }

    }

    @OnClick(R.id.device_tutorial_button)
    public void dialogButtonAction() {
        if (page < COMPLETE) {
            page += 1;
            configureView();
        } else {
            dismiss();
        }
    }

    @OnClick(R.id.device_tutorial_cancel)
    public void dialogButtonCancel() {
        //on annule la connexion
        RidePlusApplication.bluetoothDeviceManager.cancelSubscriptions();

        dismiss();
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case DEVICE_ADDED:
                //si le mot de passe est différent du mot de passe par défaut, on demande à l'utilisateur de le saisir pour continuer
                if (message.content instanceof Device) {
                    page = COMPLETE;
                    configureView();
                }
                break;
            case RESTART_SCAN:
                startScan();
                break;
            case ERROR_BLE_CORE:
            case ERROR_BLE_STOP:
                dismiss();
                break;
            case DISMISS_POPUP:
            case USER_CURRENT_LOCALLY_UPDATED:
                dismiss();
                break;
        }
        super.onMessageEvent(message);
    }

    /**
     * Démarre la recherche d'appareil si le bluetooth est activé
     */
    public void startScan() {
        if (BluetoothAdapter.getDefaultAdapter() != null) {
            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                RidePlusApplication.bluetoothDeviceManager.startScan(null);
            } else {
                if (getActivity() != null)
                    Toasty.error(getActivity(), getString(R.string.error_message_ble_disabled), Toast.LENGTH_LONG, true).show();
                dismiss();
            }
        } else {
            if (getActivity() != null)
                Toasty.error(getActivity(), getString(R.string.error_message_ble_not_avalaible), Toast.LENGTH_LONG, true).show();
            dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_PERMISSIONS:
                startScan();
                break;
        }
    }


}
