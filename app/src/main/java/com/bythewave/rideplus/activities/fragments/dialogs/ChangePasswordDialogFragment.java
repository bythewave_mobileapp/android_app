package com.bythewave.rideplus.activities.fragments.dialogs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * com.by_the_wave.ride_plus_android.activities.fragments.dialogs.AddDeviceDialogFragment
 * <p/>
 * Affiche une fenêtre surgissante permettant à l'utilisateur de modifier son mot de passe
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 04/02/2018
 */
public class ChangePasswordDialogFragment extends BaseDialogFragment {


    @BindView(R.id.change_password_field)
    protected EditText editText;

    @BindView(R.id.change_password_label)
    protected TextView title;

    @BindView(R.id.change_password_submit)
    protected Button submitButton;

    /**
     * Renvoi une nvelle instance du framgent
     *
     * @return une nvelle instance du fragment
     */
    public static ChangePasswordDialogFragment newInstance() {
        return new ChangePasswordDialogFragment();
    }

    /**
     * Constructeur vide
     */
    @SuppressLint("Range")
    public ChangePasswordDialogFragment() {
        if (getActivity() != null)
            RidePlusApplication.firebaseAnalytics.setCurrentScreen(getActivity(), Constants.SCREEN_CHANGE_PASSWORD, null);
        this.setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_change_password, container, true);
        ButterKnife.bind(this, view);

        //ajout d'un controle de saisie pour activer le bouton me connecter
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //on active le bouton Me connecter si le login et le mdp sont renseignés
                submitButton.setEnabled(!TextUtils.isEmpty(editText.getText()));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        editText.addTextChangedListener(textWatcher);

        //prise en compte du tap sur le bouton "go" du clavier lorsque l'on édite le mot de passe
        editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    changePassword(submitButton);
                    return true;
                } else {
                    return false;
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getDialog() != null && getDialog().getWindow() != null && getContext() != null) {

            //on redimentionne le dialogue pr occuper tout l'écran horizontalement
            getDialog().getWindow().setLayout(AndroidUtils.getScreenWidth() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp32)), ViewGroup.LayoutParams.WRAP_CONTENT);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                getDialog().getWindow().setElevation(5f);
            }

            int transparentColor = ContextCompat.getColor(getContext(), android.R.color.transparent);
            getDialog().getWindow().setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{transparentColor, transparentColor}));

            editText.requestFocus();
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    /**
     * Méthode permettant d'activer ou désactiver les interactions de l'utilisateur avec l'ensemble des boutons de la vue.
     * En cas de désactivation, affiche l'indicateur d'activité
     *
     * @param enable vrai pour activer les boutons, faux pour les désactiver et afficher l'indicateur d'activité
     */
    private void enableButtons(boolean enable) {
        submitButton.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(enable ? 1f : 0f);
        editText.setEnabled(enable);
    }

    /**
     * Masque le clavier
     *
     * @param view la vue demandant à masquer le clavier
     */
    public void dismissKeyboard(View view) {
        if (getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @OnClick(R.id.change_password_submit)
    public void changePassword(Button button) {
        enableButtons(false);

        dismissKeyboard(button);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.updatePassword(editText.getText().toString())
                    .addOnCompleteListener(task -> {
                        enableButtons(true);
                        if (task.isSuccessful()) {
                            // Firebase log
                            RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_PROFILE_PASSWORD_CHANGED, null);

                            //auth ok, on ferme le dialogue
                            dismiss();
                        } else {
                            //on affiche un message d'erreur si la tache échoue
                            if (task.getException() instanceof FirebaseAuthWeakPasswordException) {
                                if (getContext() != null)
                                    Toasty.error(getContext(), getString(R.string.error_message_weak_password), Toast.LENGTH_LONG, true).show();
                            } else {
                                BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                            }
                        }
                    });
        }
    }

    @OnClick(R.id.change_password_cancel)
    public void cancel(Button button) {
        dismissKeyboard(button);

        dismiss();
    }
}
