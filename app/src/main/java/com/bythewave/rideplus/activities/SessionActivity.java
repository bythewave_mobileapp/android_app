package com.bythewave.rideplus.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.adapters.viewholders.LockedWaveViewHolder;
import com.bythewave.rideplus.adapters.viewholders.SessionViewHolder;
import com.bythewave.rideplus.adapters.viewholders.SpaceViewHolder;
import com.bythewave.rideplus.adapters.viewholders.WaveViewHolder;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Moment;
import com.bythewave.rideplus.models.Session;
import com.bythewave.rideplus.models.Wave;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lapptelier.smartrecyclerview.MultiGenericAdapter;
import com.lapptelier.smartrecyclerview.SmartRecyclerView;
import com.lapptelier.smartrecyclerview.ViewHolderInteraction;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;
import com.socks.library.KLog;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * fr.reseauscet.scet_app_android.activities.fragments.DeviceFragment
 * <p/>
 * Fragment affichant la liste de session de l'utilisateur
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 24/01/2018
 */
public class SessionActivity extends BaseAppCompatActivity implements OnMapReadyCallback, ViewHolderInteractionListener {

    /**
     * Constantes de la classe
     */
    private static final float POLYLINE_INNER_WIDTH = AndroidUtils.dp(4f);
    private static final double CIRCLE_RADIUS = 1;
    private static final float CIRCLE_STROKE_WIDTH = AndroidUtils.dp(2f);


    public enum ViewState {LIST_COLLAPSED, LIST_EXPANDED, LIST_ANIMATING}

    @BindView(R.id.session_toolbar)
    protected Toolbar actionBar;

    @BindView(R.id.toolbar_title)
    TextView title;

    @BindView(R.id.session_waves_list)
    protected SmartRecyclerView recyclerView;
    protected MultiGenericAdapter adapter;
    protected LinearLayoutManager layoutManager;

    @InjectExtra
    public Session session; // la session à afficher

    private Menu menu;
    private GoogleMap map;
    private ViewState listExpansionState;

    private int currentRecyclerViewPosition = 0; // conserve la position de la dernière vague affichée

    /**
     * Constructeur vide
     */
    public SessionActivity() {
        screen = Constants.SCREEN_SESSION;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_session);
        ButterKnife.bind(this);
        Dart.inject(this);

        //configuration de l'action bar
        setSupportActionBar(actionBar);
        ActionBar supportActionBar = getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        //on change le titre affiché
        title.setText(R.string.session_summary);

        // Configuration de l'adapter
        adapter = new MultiGenericAdapter(Session.class, SessionViewHolder.class, R.layout.cell_session_detail, this);
        if (session != null && session.isPhoneSession())
            adapter.addViewHolderType(Wave.class, LockedWaveViewHolder.class, R.layout.cell_wave_detail);
        else
            adapter.addViewHolderType(Wave.class, WaveViewHolder.class, R.layout.cell_wave_detail);

        adapter.addViewHolderType(Boolean.class, SpaceViewHolder.class, R.layout.cell_wave_space);
        recyclerView.setAdapter(adapter);

        //configuration de la liste
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        //On ajoute un snapHelper pour simuler le comportement d'un view pager
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView.getRecyclerView());

        recyclerView.addExternalOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int position = layoutManager.findFirstCompletelyVisibleItemPosition();
                    if (currentRecyclerViewPosition != position) {
                        currentRecyclerViewPosition = position;
                        //on évalue si l'objet est à la position courante est une vague ou une session
                        Object item = adapter.getItemAt(position);
                        //on rafraichit la carte
                        configureMap(item instanceof Wave ? (Wave) item : null, true);
                        //on modifie le titre
                        changeTitle();
                    }
                }
            }
        });

        //on sette le texte de la vue vide
        recyclerView.setEmptyLayout(R.layout.layout_empty);
        recyclerView.enableSwipeToRefresh(false);
        recyclerView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));

        //on change le texte de la vue vide
        View emptyView = recyclerView.getEmptyView();
        TextView emptyText = emptyView.findViewById(R.id.empty_text);
        if (emptyText != null) {
            emptyText.setText(getString(R.string.surf_empty_session));
        }

        //configuration de la carte
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.session_map);
        if (mapFragment != null)
            mapFragment.getMapAsync(this);

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable(Constants.INTENT_SESSION, session);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        session = savedInstanceState.getParcelable(Constants.INTENT_SESSION);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        map.setPadding((int) getResources().getDimension(R.dimen.dp16), (int) getResources().getDimension(R.dimen.dp16), (int) getResources().getDimension(R.dimen.dp16), (int) (AndroidUtils.getScreenHeight() / 3 - getResources().getDimension(R.dimen.cell_height) + 10));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(session.position.getLatitude(), session.position.getLongitude()), 19));
        //on sélectionne la vague correspondant à un cercle ou une ligne cliquée
        map.setOnPolylineClickListener(polyline -> selectWave((Wave) polyline.getTag()));
        map.setOnCircleClickListener(circle -> selectWave((Wave) circle.getTag()));
        configureMap(null, true);
    }

    /**
     * Ajoute les éléments à la cartographie
     *
     * @param selectedWave        la vague sélectionnée
     * @param animateCameraUpdate vrai pour animer les changements de camera, faux sinon
     */
    private void configureMap(@Nullable Wave selectedWave, boolean animateCameraUpdate) {
        map.clear();

        LatLngBounds.Builder globalBounds = new LatLngBounds.Builder();

        //on ajoute chaque vague de la session
        for (Wave wave : session.getVisibleWaves()) {
            //on évalue si la vague est présentée
            boolean waveVisible = wave.equals(selectedWave);

            int color = session.isPhoneSession() ? ContextCompat.getColor(this, waveVisible ? R.color.corn : R.color.faded_corn) : RidePlusApplication.configManager.getLevelColor(wave.level, !waveVisible);
            PolylineOptions innerPolyLine = new PolylineOptions().width(POLYLINE_INNER_WIDTH).color(color).clickable(true).jointType(JointType.ROUND).zIndex(0f);

            for (Moment moment : wave.moments) {
                LatLng latLng = new LatLng(moment.position.getLatitude(), moment.position.getLongitude());
                innerPolyLine.add(latLng);

                //si la vague est celle affichée, on affiche les moments caractéristiques
                //on affiche uniquement les moments importants
                if (waveVisible && !moment.type.equals(Moment.MOMENT_DEFAULT)) {
                    Circle circle = map.addCircle(new CircleOptions()
                            .center(latLng)
                            .clickable(true)
                            .fillColor(RidePlusApplication.configManager.getMomentColor(moment.type))
                            .strokeColor(ContextCompat.getColor(this, R.color.white))
                            .strokeWidth(CIRCLE_STROKE_WIDTH)
                            .radius(CIRCLE_RADIUS)
                            .zIndex(1f));
                    circle.setTag(wave);
                }

                // on ajoute les points aux points à prendre en compte pr le niveau de zoom
                // si on a une vague en paramètre et que celle ci est sélectionnée ou si on a aucune vague en paramètre
                //noinspection ConstantConditions
                if ((waveVisible && selectedWave != null) || (selectedWave == null)) {
                    globalBounds.include(latLng);
                }
            }

            Polyline line = map.addPolyline(innerPolyLine);
            line.setTag(wave);

            //on zoome soit sur les éléments selectionnés, ou tous les éléments si aucun ne l'est
            map.setOnMapLoadedCallback(() ->
            {
                if (animateCameraUpdate)
                    map.animateCamera(CameraUpdateFactory.newLatLngBounds(globalBounds.build(), (int) getResources().getDimension(R.dimen.dp96)));
                else
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(globalBounds.build(), (int) getResources().getDimension(R.dimen.dp96)));
            });
        }


    }

    /**
     * Sélectionne une vague dans la liste des vagues et met à jour la cartographie
     *
     * @param wave la vague à sélectionner
     */
    private void selectWave(Wave wave) {
        currentRecyclerViewPosition = adapter.getObjectIndex(wave);
        recyclerView.smoothScrollTo(currentRecyclerViewPosition);
        configureMap(wave, true);
        //on modifie le titre
        changeTitle();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_wave:
                startActivityForResult(Henson.with(this)
                        .gotoWaveListActivity()
                        .session(session)
                        .build(), Constants.REQUEST_CODE_UPDATED_SESSION);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu != null) {
            getMenuInflater().inflate(R.menu.wave, menu);
            //par défaut on masque le bouton "Terminer" et le bouton supprimer désactivé
            if (menu.findItem(R.id.menu_wave) != null)
                menu.findItem(R.id.menu_wave_disabled).setVisible(false);
            this.menu = menu;
            changeMenuVisibility();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //on envoi un message avec un peu de délai sur le bus système pour recharger les vues concernées
        try {
            if (requestCode == Constants.REQUEST_CODE_UPDATED_SESSION && resultCode == RESULT_OK && data != null && data.getParcelableExtra(Constants.INTENT_SESSION) != null) {
                session = data.getParcelableExtra(Constants.INTENT_SESSION);
                configureView();
                //si une vague est renvoyée, on la selectionne
                int selectedWaveIndex = data.getIntExtra(Constants.INTENT_SELECTED_WAVE_INDEX, -1);
                if (session != null && !session.getVisibleWaves().isEmpty() && selectedWaveIndex > -1 && session.getVisibleWaves().get(selectedWaveIndex) != null)
                    selectWave(session.getVisibleWaves().get(selectedWaveIndex));
            }
        } catch (IllegalStateException exp) {
            KLog.e(exp.getLocalizedMessage());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Firebase log
        RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_SESSION_SELECTED, null);
    }

    @Override
    public void onResume() {
        super.onResume();

        configureView();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void finish() {
        setResult(Activity.RESULT_OK, new Intent());
        super.finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    /**
     * Configure la vue en fonction de la session en paramètre
     */
    private void configureView() {
        //si la session n'a plus de vagues, on supprime la session et on ferme l'activité
        if (session != null && session.getVisibleWaves().isEmpty()) {
            //on supprime la session
            FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS).document(session.getSessionId()).delete();
            //on ferme l'activité
            finish();
        }

        recyclerView.setTranslationY(AndroidUtils.getScreenHeight() * 2 / 3);
        listExpansionState = ViewState.LIST_COLLAPSED;

        if (session != null) {
            adapter.add(session);
            adapter.appendAll(session.getVisibleWaves());
            //on ajoute un boolean en fin pour ajouter une cellule "d'espace",
            // permettant de centrer la cellule de la dernière vague
            adapter.append(true);

            //on modifie le titre
            changeTitle();

            //on marque la session comme consultée si besoin
            if (!session.isViewed) {
                session.isViewed = true;
                RidePlusApplication.sessionManager.saveSession(session);
                adapter.notifyDataSetChanged();
            }

            //on relance le reverse geocoding si besoin
            if (TextUtils.isEmpty(session.getName())) {
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            session.getGeoNames();
                            if (!TextUtils.isEmpty(session.getName())) {
                                RidePlusApplication.sessionManager.saveSession(session);
                            }
                        } catch (IOException e) {
                            KLog.e(e);
                        }
                    }
                }.start();
            }
        }
        changeMenuVisibility();

        //on indique aux cellules l'état initial de la recycler view
        BusMessage.post(BusMessageType.SESSION_LIST_SIZE_CHANGED, listExpansionState);
    }

    /**
     * Modifie le titre de l'activité en fonction de la vague ou de la session affichée
     */
    private void changeTitle() {
        if (currentRecyclerViewPosition == 0) {
            title.setText(getString(R.string.session_summary));
        } else {
            title.setText(String.format(getString(R.string.session_title), currentRecyclerViewPosition, session.getVisibleWaves().size()));
        }
    }

    /**
     * Modifie l'apparence du bouton dans la barre d'état
     */
    private void changeMenuVisibility() {
        if (menu != null && menu.findItem(R.id.menu_finish) != null && menu.findItem(R.id.menu_wave) != null && menu.findItem(R.id.menu_wave_disabled) != null) {
            menu.findItem(R.id.menu_wave).setVisible(!adapter.isEmpty());
            menu.findItem(R.id.menu_wave_disabled).setVisible(adapter.isEmpty());
        }
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case OPEN_EDIT_SESSION:
                startActivityForResult(Henson.with(this)
                        .gotoEditSessionActivity()
                        .session(session)
                        .build(), Constants.REQUEST_CODE_UPDATED_SESSION);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                break;
            case SESSION_UPDATED:
                //on recharge la session
                if (message.content instanceof Session) {
                    session = (Session) message.content;
                    configureView();
                }
                break;
        }
        super.onMessageEvent(message);
    }

    /**
     * Agrandit ou réduit la liste des vagues/session
     *
     * @param expand vrai pour agrandir la liste, faux sinon
     */
    public void displayWaveList(boolean expand) {
        listExpansionState = ViewState.LIST_ANIMATING;

        ValueAnimator translateAnimation = ObjectAnimator.ofFloat(recyclerView, "translationY", expand ? AndroidUtils.getScreenHeight() * 2 / 3 : 0f, expand ? 0f : AndroidUtils.getScreenHeight() * 2 / 3);
        translateAnimation.setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));
        translateAnimation.setInterpolator(new OvershootInterpolator());
        translateAnimation.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                listExpansionState = expand ? ViewState.LIST_EXPANDED : ViewState.LIST_COLLAPSED;
                BusMessage.post(BusMessageType.SESSION_LIST_SIZE_CHANGED, listExpansionState);

            }
        });
        translateAnimation.start();
    }

    /**
     * Renvoi l'état de la liste des vagues : agrandie, réduite ou en transition d'un état à un autre
     *
     * @return l'état de la liste des vagues : agrandie, réduite ou en transition d'un état à un autre
     */
    public ViewState getListExpansionState() {
        return listExpansionState;
    }


    @Override
    public void onItemAction(@Nullable Object object, int index, @Nullable ViewHolderInteraction viewHolderInteraction) {

    }
}
