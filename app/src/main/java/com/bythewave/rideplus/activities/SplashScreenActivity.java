package com.bythewave.rideplus.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.models.BusMessage;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import io.fabric.sdk.android.Fabric;

/**
 * fr.reseauscet.scet_app_android.activities.MainActivity
 * <p/>
 * Activité principale, présentant le menu de l'app.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 19/01/2018
 */
public class SplashScreenActivity extends BaseAppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);

        //on lance le téléchargement des fichiers de conseils
        RidePlusApplication.configManager.checkApiVersion();

    }

    @Override
    public void onMessageEvent(BusMessage message) {
        super.onMessageEvent(message);
        switch (message.type) {
            case CONFIGURATION_UPDATED:
                final Intent intent;
                //si utilisateur courant, on lance son refresh dès le splashscreen
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                if (currentUser != null && RidePlusApplication.currentUserManager.getCurrentUser() != null) {
                    intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                } else {
                    intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                }

                //on vérifie si on a un current user
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case APPLICATION_OUTDATED:
                //on affiche un toast d'erreur et on ferme l'app
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.RidePlusSimpleAlertDialogTheme);
                builder.setMessage(R.string.error_application_outdated)
                        .setPositiveButton(R.string.go_to_store, (dialog, id) -> startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getApplication().getPackageName()))))
                        .setNegativeButton(R.string.quit, (dialog, id) -> finish());
                builder.create().show();
                break;
            case APPLICATION_UP_TO_DATE:
                //application à jour, on télécharge si besoin les fichiers de coaching
                RidePlusApplication.configManager.downloadCoachingFiles();
                break;
        }
    }
}