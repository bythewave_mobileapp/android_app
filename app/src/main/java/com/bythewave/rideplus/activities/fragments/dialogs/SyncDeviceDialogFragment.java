package com.bythewave.rideplus.activities.fragments.dialogs;

import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * com.by_the_wave.ride_plus_android.activities.fragments.dialogs.AddDeviceDialogFragment
 * <p/>
 * Affiche une fenêtre surgissante permettant de synchroniser un appareil
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 30/01/2018
 */
public class SyncDeviceDialogFragment extends BaseDialogFragment {

    @BindView(R.id.device_sync_image)
    protected ImageView syncImage;
    @BindView(R.id.device_sync_text)
    protected TextView syncText;
    @BindView(R.id.device_sync_pulse_0)
    protected ImageView pulse0;
    @BindView(R.id.device_sync_pulse_1)
    protected ImageView pulse1;
    @BindView(R.id.device_sync_pulse_2)
    protected ImageView pulse2;


    /**
     * Renvoi une nvelle instance du framgent
     *
     * @return une nvelle instance du fragment
     */
    public static SyncDeviceDialogFragment newInstance() {
        return new SyncDeviceDialogFragment();
    }

    /**
     * Constructeur vide
     */
    public SyncDeviceDialogFragment() {
        if (getActivity() != null)
            RidePlusApplication.firebaseAnalytics.setCurrentScreen(getActivity(), Constants.SCREEN_SYNC_DEVICE, null);
        this.setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_sync_device, container, true);
        ButterKnife.bind(this, view);

        syncImage.setImageResource(R.drawable.image_sync);
        syncText.setText(getText(R.string.synchronizing));

        Animation pulseAnim = AnimationUtils.loadAnimation(getContext(), R.anim.pulse_rotate);
        syncImage.startAnimation(pulseAnim);
        Animation expandFadeAnim0 = AnimationUtils.loadAnimation(getContext(), R.anim.expand_fade);
        pulse0.startAnimation(expandFadeAnim0);
        Animation expandFadeAnim1 = AnimationUtils.loadAnimation(getContext(), R.anim.expand_fade);
        expandFadeAnim1.setStartOffset(600);
        pulse1.startAnimation(expandFadeAnim1);
        Animation expandFadeAnim2 = AnimationUtils.loadAnimation(getContext(), R.anim.expand_fade);
        expandFadeAnim2.setStartOffset(1200);
        pulse2.startAnimation(expandFadeAnim2);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getDialog() != null && getDialog().getWindow() != null) {
            //on redimentionne le dialogue pr occuper tout l'écran
            getDialog().getWindow().setLayout(AndroidUtils.getScreenWidth() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp16)), AndroidUtils.getScreenHeight() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp32)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                getDialog().getWindow().setElevation(5f);
            }

            int transparentColor = ContextCompat.getColor(getContext(), android.R.color.transparent);
            getDialog().getWindow().setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{transparentColor, transparentColor}));
        }

    }

    @Override
    public void dismiss() {
        syncImage.clearAnimation();
        pulse0.clearAnimation();
        pulse1.clearAnimation();
        pulse2.clearAnimation();
        super.dismiss();
    }

    @OnClick(R.id.device_tutorial_cancel)
    public void dialogButtonCancel() {
        //on annule la connexion
        RidePlusApplication.bluetoothDeviceManager.cancelSubscriptions();
        dismiss();
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case DEVICE_STATE_UPDATED:
            case SESSION_ADDED:
                // Firebase log
                RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_DEVICE_SYNC, null);
                dismiss();
                break;
            case SYNC_EMPTY:
            case SYNC_NO_WAVES:
                // Firebase log
                RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_DEVICE_SYNC, null);
                if (getContext() != null)
                    //on affiche un message d'avertissement comme quoi il n'y a pas de session à importer
                    Toasty.warning(getContext(), AndroidUtils.getSportString("_error_no_waves"), Toast.LENGTH_LONG, true).show();
                dismiss();
                break;
            case ERROR_BLE_CORE:
            case ERROR_BLE_STOP:
            case ERROR_DEVICE_DATA:
            case ERROR_DEVICE_STATE:
            case SYNC_FAILED:
                dismiss();
                break;
        }
        super.onMessageEvent(message);
    }


}
