package com.bythewave.rideplus.activities.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.activities.EditProfileActivity;
import com.bythewave.rideplus.activities.fragments.dialogs.ChangeSportDialogFragment;
import com.bythewave.rideplus.adapters.viewholders.PerkPairViewHolder;
import com.bythewave.rideplus.adapters.viewholders.ProfileHeaderViewHolder;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Perk;
import com.bythewave.rideplus.models.PerkPair;
import com.bythewave.rideplus.models.Session;
import com.bythewave.rideplus.models.User;
import com.bythewave.rideplus.models.Wave;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lapptelier.smartrecyclerview.MultiGenericAdapter;
import com.lapptelier.smartrecyclerview.SmartRecyclerView;
import com.lapptelier.smartrecyclerview.ViewHolderInteraction;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;
import com.socks.library.KLog;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * fr.reseauscet.scet_app_android.activities.fragments.headerFragment
 * <p/>
 * Fragment affichant le profil de l'utilisateur
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 24/01/2018
 */
public class ProfileFragment extends BaseFragment implements ViewHolderInteractionListener {

    private static final int RANGE_1 = 0;
    private static final int RANGE_30 = 1;
    private static final int RANGE_365 = 2;

    @BindView(R.id.profile_perk_list)
    protected SmartRecyclerView recyclerView;

    @BindView(R.id.profile_fake_toolbar)
    protected View fakeActionBar;

    @BindView(R.id.profile_fake_title)
    protected TextView fakeTitle;

    @BindView(R.id.profile_settings_blue)
    protected ImageButton settingsBlue;

    // id de l'onglet correspondant au fragment
    private int tabId;

    // horizon d'affichage des statistiques
    private int perkRange;

    // adapter de la recyclerView
    private MultiGenericAdapter adapter;


    /**
     * Renvoi une instance de ce fragment
     *
     * @param tabId l'id de l'onglet correspondant
     * @return l'instance du fragment
     */
    public static Fragment newInstance(int tabId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.INTENT_TAB_ID, tabId);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Constructeur vide
     */
    public ProfileFragment() {
        screen = Constants.SCREEN_PROFILE;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.tabId = getArguments().getInt(Constants.INTENT_TAB_ID);
        }

        //on désactive le menu de l'action Bar sur ce fragment
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        // Configuration de l'adapter
        adapter = new MultiGenericAdapter(PerkPair.class, PerkPairViewHolder.class, R.layout.cell_perk_pair, this);
        adapter.addViewHolderType(User.class, ProfileHeaderViewHolder.class, R.layout.cell_profile_header);
        recyclerView.setAdapter(adapter);
        recyclerView.setRefreshListener(this::configureView);
        recyclerView.setLoadMoreLayout(R.layout.layout_perk_shimmer_loading);

        //configuration de la liste
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity(), RecyclerView.VERTICAL, false));

        //on sette le texte de la vue vide
        recyclerView.setLoadingLayout(R.layout.layout_loading);
        recyclerView.setEmptyLayout(R.layout.layout_empty);
        if (getContext() != null)
            recyclerView.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
        Objects.requireNonNull(recyclerView.getSwipeLayout()).setColorSchemeResources(R.color.maya_blue, R.color.maya_blue, R.color.maya_blue, R.color.maya_blue);

        //on fait apparaitre ou disparaitre la fausse actionBar au scroll
        recyclerView.addExternalOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                fakeActionBar.setAlpha(recyclerView.computeVerticalScrollOffset() / 100f);
                fakeTitle.setAlpha(recyclerView.computeVerticalScrollOffset() / 100f);
                settingsBlue.setAlpha(recyclerView.computeVerticalScrollOffset() / 100f);
            }
        });

        //on change le texte de la vue vide
        View emptyView = recyclerView.getEmptyView();
        TextView emptyText = emptyView.findViewById(R.id.empty_text);
        if (emptyText != null && getContext() != null) {
            emptyText.setText(getString(R.string.error_loading_profile));
        }

        configureView();

        return view;
    }

    @OnClick({R.id.profile_settings, R.id.profile_settings_blue})
    protected void editProfile() {
        if (getActivity() != null) {
            getActivity().startActivityForResult(new Intent(getActivity(), EditProfileActivity.class), Constants.REQUEST_CODE_REFRESH_NEEDED);
            getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        }
    }


    /**
     * Récupère une de statistiques sur le serveur
     */
    public void getPerks() {
        int range = 1;
        switch (perkRange) {
            case RANGE_30:
                range = 30;
                break;
            case RANGE_365:
                range = 365;
                break;
        }

        //on ajoute les statistiques à la liste
        adapter.clear();
        adapter.setHasMore(true);
        //ajout du header
        if (RidePlusApplication.currentUserManager.hasCurrentUser()) {
            adapter.append(RidePlusApplication.currentUserManager.getCurrentUser());
        }


        FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                .whereEqualTo(Constants.FIELD_USER_ID, RidePlusApplication.currentUserManager.getCurrentUserId())
                .whereEqualTo(Constants.FIELD_SPORT, RidePlusApplication.configManager.getCurrentSport().value())
                .whereLessThanOrEqualTo(Constants.FIELD_CREATED_AT, DateTime.now().toDate())
                .whereGreaterThanOrEqualTo(Constants.FIELD_CREATED_AT, DateTime.now().minusDays(range).toDate())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        // statistiques à calculer
                        double waveCount = 0;
                        double maxWaveLength = 0;
                        double avgLevel = 0;
                        double maxLevel = 0;
                        double surfTime = 0;

                        //on récupère toutes les sessions
                        List<Session> sessions = new ArrayList<>();
                        for (DocumentSnapshot document : task.getResult()) {
                            if (document != null && document.exists()) {
                                Session session = document.toObject(Session.class);
                                sessions.add(session);
                                //on calcule les statistiques
                                if (session.waves != null) {
                                    waveCount += session.waves.size();
                                    for (Wave wave : session.waves) {
                                        if (wave != null) {
                                            if (wave.distance > maxWaveLength || maxWaveLength < 0)
                                                maxWaveLength = wave.distance;
                                            if (wave.level > maxLevel || maxLevel < 0)
                                                maxLevel = wave.level;
                                            avgLevel += wave.level;
                                            surfTime += wave.duration;
                                        }

                                    }
                                }
                            }
                        }

                        List<PerkPair> perks = new ArrayList<>();
                        if (getContext() != null) {
                            perks.add(new PerkPair(
                                    new Perk(
                                            AndroidUtils.getSportString("_perk_ride_count"),
                                            String.valueOf(Double.valueOf(waveCount).intValue()), false),
                                    new Perk(
                                            AndroidUtils.getSportString("_perk_session_count"),
                                            String.valueOf(sessions.size()), false)));

                            perks.add(new PerkPair(
                                    new Perk(
                                            AndroidUtils.getSportString("_perk_longest_ride"),
                                            String.format(getString(R.string.distance), maxWaveLength), false),
                                    new Perk(
                                            AndroidUtils.getSportString("_perk_ride_time"),
                                            Html.fromHtml(AndroidUtils.getDurationText(surfTime, false)).toString(), false)));

                            perks.add(new PerkPair(
                                    new Perk(
                                            AndroidUtils.getSportString("_perk_best_ride"),
                                            String.valueOf(Double.valueOf(maxLevel).intValue()), true),
                                    new Perk(
                                            AndroidUtils.getSportString("_perk_avg_ride"),
                                            String.valueOf(waveCount > 0.0 ? avgLevel / waveCount : 0.0), true)));

                        }

                        //on ajoute les statistiques à la liste
                        adapter.clear();
                        adapter.setHasMore(false);
                        //ajout du header
                        if (RidePlusApplication.currentUserManager.hasCurrentUser()) {
                            adapter.append(RidePlusApplication.currentUserManager.getCurrentUser());
                        }
                        adapter.appendAll(perks);
                    } else {
                        KLog.e("Error getting sessions: ", task.getException());
                        BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    /**
     * Configure la vue à l'aide des informations de l'utilisateur courant
     */
    private void configureView() {
        //ajout du header
        adapter.setHasMore(true);
        if (RidePlusApplication.currentUserManager.hasCurrentUser()) {
            adapter.add(RidePlusApplication.currentUserManager.getCurrentUser());
        }

        getPerks();
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case USER_CURRENT_UPDATED:
            case USER_CURRENT_SAVED:
            case CURRENT_SPORT_UPDATED:
            case REFRESH_CONTENT:
                configureView();
                break;
            case CHANGE_SPORT_REQUEST:
                changeSport((int[]) message.content);
            case PERK_RANGE_CHANGE:
                if (message.content instanceof Integer) {
                    perkRange = (int) message.content;
                    getPerks();
                }
                break;
            case SCROLL_TO_TOP:
                if ((this.tabId == (int) message.content) && recyclerView != null)
                    recyclerView.scrollToTop();
                break;
        }
        super.onMessageEvent(message);
    }

    /**
     * Affiche la popup de sélection d'un sport
     *
     * @param location la position à laquelle afficher la popup.
     */
    protected void changeSport(int[] location) {
        if (getFragmentManager() != null)
            ChangeSportDialogFragment.newInstance(location[1]).show(getFragmentManager(), "");
    }

    @Override
    public void onItemAction(@Nullable Object object, int index, @Nullable ViewHolderInteraction viewHolderInteraction) {
    }
}
