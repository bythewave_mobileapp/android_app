package com.bythewave.rideplus.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.activities.fragments.dialogs.LoadingDialogFragment;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Device;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.socks.library.KLog;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.DateTime;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * com.by_the_wave.ride_plus_android.activities.EditDeviceActivity
 * <p/>
 * Activité permettant d'éditer les informations d'un appareil
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 31/01/2018
 */
public class EditDeviceActivity extends BaseAppCompatActivity {

    @BindView(R.id.edit_device_toolbar)
    protected Toolbar actionBar;

    @BindView(R.id.edit_device_name_label)
    protected TextView customNameLabel;

    @BindView(R.id.edit_device_name)
    protected EditText customName;

    @BindView(R.id.edit_device_model)
    protected TextView model;

    @BindView(R.id.edit_device_serial)
    protected TextView serial;

    @BindView(R.id.edit_device_hardware_rev)
    protected TextView hardwareRev;

    @BindView(R.id.edit_device_firmware_rev)
    protected TextView firmwareRev;

    private Device device;


    private LoadingDialogFragment loadingDialogFragment; // dialogue affichant un message d'attente
    private Menu menu;

    /**
     * Constructeur vide
     */
    public EditDeviceActivity() {
        screen = Constants.SCREEN_DEVICE_EDIT;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_device);
        ButterKnife.bind(this);

        //configuration de l'action bar
        setSupportActionBar(actionBar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        //on configure les éléments de la vue
        configureView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED, new Intent());
                finish();
                return true;
            case R.id.menu_save:
                setResult(Activity.RESULT_OK, new Intent());
                saveDevice();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu != null) {
            getMenuInflater().inflate(R.menu.save, menu);
            this.menu = menu;
            configureView();
        }
        return true;
    }

    @Override
    public void finish() {
        setResult(Activity.RESULT_OK, new Intent());
        super.finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    /**
     * Configure la vue avec l'appareil courant de l'utilisateur
     */
    private void configureView() {
        if (RidePlusApplication.currentUserManager.hasDevices()) {
            device = RidePlusApplication.currentUserManager.getCurrentUserDevices().get(0);

            if (device != null) {
                customName.setText(device.customName);
                model.setText(Html.fromHtml(String.format(getString(R.string.model), device.model)), TextView.BufferType.SPANNABLE);
                serial.setText(Html.fromHtml(String.format(getString(R.string.serial_number), device.serial)), TextView.BufferType.SPANNABLE);
                hardwareRev.setText(Html.fromHtml(String.format(getString(R.string.hardware_revision), device.hardwareRev)), TextView.BufferType.SPANNABLE);
                firmwareRev.setText(Html.fromHtml(String.format(getString(R.string.firmware_revision), device.firmwareRev)), TextView.BufferType.SPANNABLE);


                // Si le nom de l'appareil n'a pas déjà été donné, on permet l'édition du nom.
                customName.setVisibility(TextUtils.isEmpty(device.customName) ? View.VISIBLE : View.GONE);
                customNameLabel.setVisibility(TextUtils.isEmpty(device.customName) ? View.VISIBLE : View.GONE);
                serial.setVisibility(TextUtils.isEmpty(device.serial) ? View.INVISIBLE : View.VISIBLE);
                model.setVisibility(TextUtils.isEmpty(device.model) ? View.INVISIBLE : View.VISIBLE);
                hardwareRev.setVisibility(TextUtils.isEmpty(device.hardwareRev) ? View.INVISIBLE : View.VISIBLE);
                firmwareRev.setVisibility(TextUtils.isEmpty(device.firmwareRev) ? View.INVISIBLE : View.VISIBLE);

                if (menu != null && menu.findItem(R.id.menu_save) != null) {
                    menu.findItem(R.id.menu_save).setVisible(TextUtils.isEmpty(device.customName));
                }
            }
        }
    }

    /**
     * Affiche le fragment d'attente
     */
    private void displayLoadingFragment(String string) {
        if (loadingDialogFragment == null)
            loadingDialogFragment = LoadingDialogFragment.newInstance(string);
        if (loadingDialogFragment != null && !loadingDialogFragment.isVisible())
            loadingDialogFragment.show(getSupportFragmentManager(), "");
    }

    /**
     * Masque le fragment d'attente
     */
    private void dismissLoadingFragment() {
        if (loadingDialogFragment != null && loadingDialogFragment.isVisible())
            loadingDialogFragment.dismiss();
    }


    /**
     * Enregistre les changements sur l'appareil et retourne à l'écran précédent
     */
    private void saveDevice() {
        if (device != null) {
            // on affiche la vue de chargement
            displayLoadingFragment(getString(R.string.saving));

            //on enregistre les changement sur l'appareil
            device.customName = TextUtils.isEmpty(customName.getText().toString()) ? getString(R.string.default_device_name) : customName.getText().toString();
            device.lastSyncAt = DateTime.now().toDate();

            RidePlusApplication.currentUserManager.setDevice(device);

            // Firebase log
            RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_DEVICE_MODIFIED, null);
        }
    }

    @OnClick(R.id.edit_device_delete_button)
    protected void deleteDevice() {
        //on affiche une boite de dialogue pour faire confirmer par l'utilisateur la suppression du device
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    //si l'appareil est connecté, on le déconnecte
                    RidePlusApplication.bluetoothDeviceManager.cancelSubscriptions();

                    //on le supprime du compte de l'utilisateur
                    RidePlusApplication.currentUserManager.deleteDevice(device);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //doing nothing
                    break;
            }
        };

        //showing an alert
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.RidePlusAlertDialogTheme);
        builder.setTitle(getString(R.string.remove_device_dialog_title)).setMessage(getString(R.string.remove_device_dialog_subtitle)).setPositiveButton(getString(R.string.delete), dialogClickListener)
                .setNegativeButton(getString(R.string.cancel), dialogClickListener).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == Constants.REQUEST_CODE_RELOAD_NEEDED && resultCode == RESULT_OK) {
                BusMessage.post(BusMessageType.RELOAD_CONTENT);
            } else if (requestCode == Constants.REQUEST_CODE_REFRESH_NEEDED && resultCode == RESULT_OK) {
                BusMessage.post(BusMessageType.REFRESH_CONTENT);
            } else {
                EventBus.getDefault().post(BusMessageType.DISMISS_POPUP);
                BusMessage.post(BusMessageType.REFRESH_CONTENT);
            }
        } catch (IllegalStateException exp) {
            KLog.e(exp.getLocalizedMessage());
        }
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case USER_CURRENT_LOCALLY_UPDATED:
                Toasty.warning(this, getString(R.string.warning_user_modification), Constants.TOAST_LENGTH_LONG, true).show();
                dismissLoadingFragment();
                finish();
                break;
            case DEVICE_LOCALLY_UPDATED:
                Toasty.warning(this, getString(R.string.warning_user_modification), Constants.TOAST_LENGTH_LONG, true).show();
                dismissLoadingFragment();
                finish();
                break;
            case DEVICE_DELETED:
                dismissLoadingFragment();
                finish();
                break;
            case DEVICE_ADDED:
            case DEVICE_UPDATED:
                //si la popup de chargement est affichée, on la masque et on ferme la fenêtre.
                // Sinon il peut s'agir d'un message envoyé lors de la connexion du boitier
                if (loadingDialogFragment != null && loadingDialogFragment.isVisible()) {
                    dismissLoadingFragment();
                    finish();
                }
                break;
        }
        super.onMessageEvent(message);
    }
}
