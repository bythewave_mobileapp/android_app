package com.bythewave.rideplus.activities;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.BuildConfig;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.socks.library.KLog;

import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * fr.reseauscet.scet_app_android.activities.ForgottenPasswordActivity
 * <p/>
 * Activité présentant un formulaire de récupération du mot de passe.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 09/09/2017
 */
public class ForgottenPasswordActivity extends BaseAppCompatActivity {

    @BindView(R.id.password_forgotten_layout)
    protected ConstraintLayout layout;

    @BindView(R.id.password_forgotten_email)
    protected EditText editTextMail;

    @BindView(R.id.password_forgotten_icon_previous)
    protected ImageView iconPrevious;

    @BindView(R.id.password_forgotten_text_previous)
    protected TextView textPrevious;

    @BindView(R.id.password_forgotten_toast)
    protected TextView textToast;

    @BindView(R.id.password_forgotten_submit)
    protected Button buttonLogin;

    public ForgottenPasswordActivity() {
        screen = Constants.SCREEN_FORGOTTEN_PASSWORD;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotten_password);
        ButterKnife.bind(this);


        //ajout d'un controle de saisie pour activer le bouton me connecter
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //on active le bouton Me connecter si le password et le mdp sont renseignés
                buttonLogin.setEnabled(!TextUtils.isEmpty(editTextMail.getText()));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        editTextMail.addTextChangedListener(textWatcher);

        //prise en compte du tap sur le bouton "go" du clavier lorsque l'on édite le mot de passe
        editTextMail.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    password(editTextMail);
                    return true;
                } else {
                    return false;
                }
            }
        });

        //on masque le message d'envoi par défaut
        textToast.setVisibility(View.GONE);

    }


    @Override
    protected void onStart() {
        super.onStart();

        //ajout de paramètre de debug
        if (BuildConfig.BUILD_TYPE.equals("debug")) {
            editTextMail.setText("edouard.brethes@gmail.com");
        }
    }

    @OnClick(R.id.password_forgotten_submit)
    public void password(View view) {

        //on anime la vue
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_pressed));

        //on désactive les boutons et active l'indicateur d'activité
        enableButtons(false);

        FirebaseAuth.getInstance().sendPasswordResetEmail(editTextMail.getText().toString())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        //on affiche un message d'information
                        textToast.setVisibility(View.VISIBLE);

                        // on utilise un handler pr laisser à l'écran la vue login un certain temps
                        new Handler().postDelayed(() -> backToLogin(null), Constants.SPLASH_TIME_OUT);
                    } else {
                        //on réactive les boutons
                        enableButtons(true);
                        if (task.getException() != null) {
                            KLog.e("sendPasswordResetEmail:failure", task.getException());
                            if (task.getException() instanceof FirebaseNetworkException) {
                                BusMessage.post(BusMessageType.ERROR_OFFLINE);
                            } else if (task.getException() instanceof FirebaseAuthInvalidUserException) {
                                Toasty.error(ForgottenPasswordActivity.this, getString(R.string.error_message_unknown_mail_adress), Toast.LENGTH_LONG, true).show();
                            } else {
                                BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);

                            }
                        }
                    }
                });
    }

    /**
     * Méthode permettant d'activer ou désactiver les interactions de l'utilisateur avec l'ensemble des boutons de la vue.
     * En cas de désactivation, affiche l'indicateur d'activité
     *
     * @param enable vrai pour activer les boutons, faux pour les désactiver et afficher l'indicateur d'activité
     */
    private void enableButtons(boolean enable) {
        buttonLogin.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(enable ? 1f : 0f);
        editTextMail.setEnabled(enable);
    }

    @OnClick({R.id.password_forgotten_icon_previous, R.id.password_forgotten_text_previous})
    public void backToLogin(View view) {
        //on anime la vue
        if (view != null)
            view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_pressed));

        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

}
