package com.bythewave.rideplus.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.activities.fragments.dialogs.AuthDialogFragment;
import com.bythewave.rideplus.activities.fragments.dialogs.ChangeEmailDialogFragment;
import com.bythewave.rideplus.activities.fragments.dialogs.ChangePasswordDialogFragment;
import com.bythewave.rideplus.activities.fragments.dialogs.LoadingDialogFragment;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.common.CustomImageLoader;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.User;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.models.enums.Stance;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socks.library.KLog;

import java.io.File;
import java.io.IOException;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import es.dmoral.toasty.Toasty;
import id.zelory.compressor.Compressor;

/**
 * com.by_the_wave.ride_plus_android.activities.EditDeviceActivity
 * <p/>
 * Activité permettant d'éditer les informations de l'utilisateur courant
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 31/01/2018
 */
public class EditProfileActivity extends BaseAppCompatActivity {

    @BindView(R.id.edit_profile_toolbar)
    protected Toolbar actionBar;

    @BindView(R.id.edit_profile_stance)
    protected SegmentedButtonGroup stanceButton;

    @BindView(R.id.edit_profile_name)
    protected EditText name;

    @BindView(R.id.edit_profile_avatar)
    protected RoundedImageView avatar;

    @BindView(R.id.edit_profile_email_button)
    protected Button emailButton;

    @BindView(R.id.edit_profile_password_button)
    protected Button passwordButton;

    // Url locale vers le nouvel avatar à envoyer
    private Image newAvatar;

    private LoadingDialogFragment loadingDialogFragment; // dialogue affichant un message d'attente
    private ChangeEmailDialogFragment changeEmailDialogFragment; // dialogue permettant de modifier son email
    private ChangePasswordDialogFragment changePasswordDialogFragment; // dialogue permettant de modifier son mot de passe
    private AuthDialogFragment authDialogFragment; // dialogue permettant à l'utilisateur de se réauthentifier

    /**
     * Constructeur vide
     */
    public EditProfileActivity() {
        screen = Constants.SCREEN_PROFILE_EDIT;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        //configuration de l'action bar
        setSupportActionBar(actionBar);
        ActionBar supportActionBar = getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);

            supportActionBar.setDisplayHomeAsUpEnabled(true);
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(ContextCompat.getColor(this, R.color.maya_blue), PorterDuff.Mode.SRC_ATOP);
            supportActionBar.setHomeAsUpIndicator(upArrow);
        }

        //on configure les éléments de la vue
        configureView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED, new Intent());
                finish();
                return true;
            case R.id.menu_save:
                // on affiche la vue de chargement
                displayLoadingFragment(getString(R.string.saving));

                if (newAvatar != null) {
                    //si on a une image de profil, on l'uploade également
                    uploadPicture(Constants.BUCKET_AVATARS, newAvatar);
                } else {
                    saveProfile(null);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu != null)
            getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }


    @Override
    public void finish() {
        setResult(Activity.RESULT_OK, new Intent());
        super.finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_AVATAR_PICKER && resultCode == RESULT_OK && data != null) {
            newAvatar = ImagePicker.getFirstImageOrNull(data);
            if (newAvatar != null && !TextUtils.isEmpty(newAvatar.getPath())) {
                AndroidUtils.displayRoundedImage(newAvatar.getPath(), avatar, R.drawable.ic_logo_btw);
            }
        }
    }

    /**
     * Configure la vue avec l'appareil courant de l'utilisateur
     */
    private void configureView() {
        User currentUser = RidePlusApplication.currentUserManager.getCurrentUser();
        if (currentUser != null) {
            name.setText(currentUser.name);
            stanceButton.setPosition(currentUser.stance == Stance.REGULAR ? 0 : 1, false);


            //si on a une nouvelle image d'avatar, on l'affiche
            if (newAvatar != null && !TextUtils.isEmpty(newAvatar.getPath())) {
                AndroidUtils.displayRoundedImage(newAvatar.getPath(), avatar, R.drawable.ic_logo_btw);
            } else {
                AndroidUtils.displayRoundedImage(currentUser.avatarUrl, avatar, R.drawable.ic_logo_btw);
            }

            FirebaseUser authUser = FirebaseAuth.getInstance().getCurrentUser();
            if (authUser != null && authUser.getProviders() != null && !authUser.getProviders().isEmpty()) {
                if (authUser.getProviders().get(0).equals(Constants.AUTH_EMAIL)) {
                    emailButton.setEnabled(true);
                    passwordButton.setEnabled(true);
                } else {
                    emailButton.setEnabled(false);
                    passwordButton.setEnabled(false);
                }

            }
        }
    }


    /**
     * Envoi une image sur FireCloud storage
     *
     * @param bucket le dossier sur lequel sauvegarder l'image
     * @param image  l'image à envoyer
     */
    private void uploadPicture(final String bucket, Image image) {
        //on vérifie que la connexion internet soit valide
        if (AndroidUtils.isNetworkAvailable()) {
            final StorageReference avatarRef = FirebaseStorage.getInstance().getReference().child(bucket + RidePlusApplication.currentUserManager.getCurrentUserId());
            //on compresse l'image avant l'envoi
            try {
                File compressedImageFile = new Compressor(this)
                        .setMaxWidth(bucket.contains(Constants.BUCKET_AVATARS) ? Constants.DEFAULT_AVATAR_IMAGE_SIZE : Constants.DEFAULT_COVER_IMAGE_WIDTH)
                        .setMaxHeight(bucket.contains(Constants.BUCKET_AVATARS) ? Constants.DEFAULT_AVATAR_IMAGE_SIZE : Constants.DEFAULT_COVER_IMAGE_HEIGHT)
                        .setQuality(75)
                        .setCompressFormat(Bitmap.CompressFormat.JPEG).
                                compressToFile(new File(image.getPath()));

                //ajout de metadonnées
                StorageMetadata metadata = new StorageMetadata.Builder()
                        .setContentType("image/jpg")
                        .build();

                Uri file = Uri.fromFile(compressedImageFile);
                UploadTask uploadTask = avatarRef.putFile(file, metadata);
                uploadTask.continueWithTask(task -> {
                    if (!task.isSuccessful()) {
                        KLog.e(task.getException());
                        BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                    }

                    // On génère l'url de téléchargement
                    return avatarRef.getDownloadUrl();
                }).addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        saveProfile(task.getResult().toString());
                    } else {
                        KLog.e(task.getException());
                        BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                    }
                });
            } catch (IOException exception) {
                KLog.e(exception);
                Toasty.error(this, getString(R.string.error_file), Toast.LENGTH_LONG, true).show();
            }
        } else {
            Toasty.error(this, getString(R.string.error_offline), Toast.LENGTH_LONG, true).show();
        }

    }

    /**
     * Enregistre les changements sur l'appareil et retourne à l'écran précédent
     *
     * @param newAvatarUrl la nvelle adresse de l'avatar, si modifiée
     */
    private void saveProfile(String newAvatarUrl) {
        //on enregistre les changement sur l'appareil
        User currentUser = RidePlusApplication.currentUserManager.getCurrentUser();
        if (currentUser != null) {

            currentUser.name = name.getText().toString();
            currentUser.stance = stanceButton.getPosition() == 0 ? Stance.REGULAR : Stance.GOOFY;

            if (newAvatarUrl != null)
                currentUser.avatarUrl = newAvatarUrl;

            // Firebase log
            RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_PROFILE_MODIFIED, null);

            RidePlusApplication.currentUserManager.save(BusMessageType.USER_CURRENT_SAVED, currentUser);
        }
    }

    @OnClick(R.id.edit_profile_disconnect)
    protected void disconnectUser() {
        // on purge les données de l'utilisateur courant
        RidePlusApplication.currentUserManager.logout();

    }

    @OnClick(R.id.edit_profile_email_button)
    protected void changeEmail() {
        if (authDialogFragment == null)
            authDialogFragment = AuthDialogFragment.newInstance(getString(R.string.title_auth));
        authDialogFragment.setOnAuthResultListener(authResult -> {
            if (authResult) {
                if (changeEmailDialogFragment == null)
                    changeEmailDialogFragment = ChangeEmailDialogFragment.newInstance();
                changeEmailDialogFragment.show(getSupportFragmentManager(), "");
            }
        });
        authDialogFragment.show(getSupportFragmentManager(), "");
    }

    @OnClick(R.id.edit_profile_password_button)
    protected void changePassword() {
        if (authDialogFragment == null)
            authDialogFragment = AuthDialogFragment.newInstance(getString(R.string.title_auth));
        authDialogFragment.setOnAuthResultListener(authResult -> {
            if (authResult) {
                if (changePasswordDialogFragment == null)
                    changePasswordDialogFragment = ChangePasswordDialogFragment.newInstance();
                changePasswordDialogFragment.show(getSupportFragmentManager(), "");
            }
        });
        authDialogFragment.show(getSupportFragmentManager(), "");
    }

    @OnClick(R.id.edit_profile_delete_button)
    protected void deleteProfile() {
        //on affiche une boite de dialogue pour faire confirmer par l'utilisateur la suppression du profile
        DialogInterface.OnClickListener dialogClickListener = (dialog, action) -> {
            switch (action) {
                case DialogInterface.BUTTON_POSITIVE:
                    //on demande d'abord à l'utilisateur de se réauthentifier si on passe par le mail
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    if (user != null && !user.getProviders().isEmpty()) {
                        if (user.getProviders().get(0).equals(Constants.AUTH_EMAIL)) {

                            // si l'utilisateur est connecté par un email, on le force à s'authentifier
                            if (authDialogFragment == null)
                                authDialogFragment = AuthDialogFragment.newInstance(getString(R.string.title_auth));
                            authDialogFragment.setOnAuthResultListener(authResult -> {
                                if (authResult) {
                                    // on affiche la vue de chargement
                                    displayLoadingFragment(getString(R.string.deleting_profile));

                                    RidePlusApplication.currentUserManager.deleteUser();
                                }
                            });
                            authDialogFragment.show(getSupportFragmentManager(), "");
                        }
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this, R.style.RidePlusAlertDialogTheme);
        builder.setTitle(
                getString(R.string.remove_profile_dialog_title)).
                setMessage(getString(R.string.remove_profile_dialog_subtitle)).
                setPositiveButton(getString(R.string.delete), dialogClickListener).
                setNegativeButton(getString(R.string.cancel), dialogClickListener).
                show();


    }

    /**
     * Affiche le fragment d'attente
     */
    private void displayLoadingFragment(String string) {
        if (loadingDialogFragment == null)
            loadingDialogFragment = LoadingDialogFragment.newInstance(string);
        if (loadingDialogFragment != null && !loadingDialogFragment.isVisible())
            loadingDialogFragment.show(getSupportFragmentManager(), "");
    }

    /**
     * Masque le fragment d'attente
     */
    private void dismissLoadingFragment() {
        if (loadingDialogFragment != null && loadingDialogFragment.isVisible())
            loadingDialogFragment.dismiss();
    }


    @OnClick({R.id.edit_profile_avatar_button})
    protected void selectImage(Button button) {
        //on affiche l'image picker
        ImagePicker.create(this)
                .returnMode(ReturnMode.ALL)
                .folderMode(false)
                .single()
                .toolbarFolderTitle(getString(R.string.photo))
                .showCamera(true)
                .imageLoader(new CustomImageLoader())
                .enableLog(true)
                .start(Constants.REQUEST_CODE_AVATAR_PICKER);
    }

    @Override
    public void onMessageEvent(final BusMessage message) {
        switch (message.type) {
            case USER_CURRENT_LOCALLY_UPDATED:
                if (loadingDialogFragment.isVisible()) {
                    dismissLoadingFragment();
                    finish();
                }
                break;
            case USER_CURRENT_UPDATED:
                if (loadingDialogFragment.isVisible()) {
                    dismissLoadingFragment();
                    finish();
                }
                configureView();
                break;
            case USER_CURRENT_SAVED:
                if (loadingDialogFragment.isVisible()) {
                    dismissLoadingFragment();
                    finish();
                }
                break;
        }
        super.onMessageEvent(message);
    }


}
