/*
 * L'Apptelier SARL (c) 2017.
 */

package com.bythewave.rideplus.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;

import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * fr.reseauscet.scet_app_android.activities.BaseAppCompatActivity
 * <p/>
 * Activité personnalisée servant de base à toutes les activités de l'app.
 * Permet de surcharger les méthodes de l'activité AppCompat (@see AppCompatActivity) pour toutes
 * les activités de l'app.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 06/09/2017
 */
public class BaseAppCompatActivity extends AppCompatActivity {

    //on conserve la date du denrier échec de connexion ou de communication avec le serveur pour ne pas réafficher 10 fois le message d'erreur
    DateTime lastFailDate = null;

    // nom de l'écran
    protected String screen = null;

    /**
     * Constructeur vide
     */
    public BaseAppCompatActivity() {
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        if (TextUtils.isEmpty(screen))
            RidePlusApplication.firebaseAnalytics.setCurrentScreen(this, null, null);
        else
            RidePlusApplication.firebaseAnalytics.setCurrentScreen(this, screen, null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case ERROR_GENERIC_SERVER:
                if (lastFailDate == null || DateTime.now().isAfter(lastFailDate.plusSeconds(Constants.SAME_ERROR_TIMEOUT))) {
                    Toasty.error(this, getString(R.string.error_unknown), Toast.LENGTH_LONG, true).show();
                }
                lastFailDate = DateTime.now();
                break;
            case ERROR_OFFLINE:
                if (lastFailDate == null || DateTime.now().isAfter(lastFailDate.plusSeconds(Constants.SAME_ERROR_TIMEOUT))) {
                    Toasty.error(this, getString(R.string.error_offline), Toast.LENGTH_LONG, true).show();
                }
                lastFailDate = DateTime.now();
                break;
            case ERROR_GENERIC_AUTH:
                if (lastFailDate == null || DateTime.now().isAfter(lastFailDate.plusSeconds(Constants.SAME_ERROR_TIMEOUT))) {
                    Toasty.error(this, getString(R.string.error_unknown), Toast.LENGTH_LONG, true).show();
                    lastFailDate = DateTime.now();
                }
            case USER_LOGGED_OUT:
            case USER_CURRENT_DELETED:
                Intent intent = new Intent(BaseAppCompatActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
                finish();
                break;
            case OPEN_URL:
                //on ouvre l'url en paramètre du message dans le navigateur externe à l'app
                startActivity(new Intent(Intent.ACTION_VIEW, (Uri) message.content));
                break;
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStickyMessageEvent(BusMessage message) {
        //A surcharger
    }


}
