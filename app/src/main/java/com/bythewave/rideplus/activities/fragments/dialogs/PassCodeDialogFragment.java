package com.bythewave.rideplus.activities.fragments.dialogs;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * com.by_the_wave.ride_plus_android.activities.fragments.dialogs.AddDeviceDialogFragment
 * <p/>
 * Affiche une fenêtre surgissante permettant à l'utilisateur de confirmer le code d'accès à un appareil
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 02/02/2018
 */
public class PassCodeDialogFragment extends BaseDialogFragment {

    /**
     * Interface utilisée par l'activité ou le fragment parent pour obtenir le résultat de l'authentification
     */
    public interface OnAuthResultListener {
        /**
         * Méthode appelée pour vérifier le code d'accès d'un appareil a été saisi.
         *
         * @param authResult le résultat de l'authentification : vrai si le code est bon, faux sinon
         */
        void setOnAuthResult(boolean authResult);

    }

    @BindView(R.id.passcode_field)
    protected EditText passcode;

    private OnAuthResultListener onAuthResultListener; // listener pour obtenir les résultats de l'authentification

    private String code; // le vrai code d'accès de l'appareil

    boolean passCodeMatch = false; // conserve le résultat de la dernière évaluation du code par l'utilisateur

    /**
     * Renvoi une nouvelle instance du framgent
     *
     * @param code le vrai code d'accès de l'appareil
     * @return une nouvelle instance du fragment
     */
    public static PassCodeDialogFragment newInstance(String code) {
        PassCodeDialogFragment fragment = new PassCodeDialogFragment();
        Bundle args = new Bundle();
        args.putString(Constants.INTENT_LOADING_TEXT, code);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Constructeur vide
     */
    public PassCodeDialogFragment() {
        if (getActivity() != null)
            RidePlusApplication.firebaseAnalytics.setCurrentScreen(getActivity(), Constants.SCREEN_PASSCODE, null);
        this.setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.code = getArguments().getString(Constants.INTENT_LOADING_TEXT);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_passcode, container, true);
        ButterKnife.bind(this, view);

        //ajout d'un controle de saisie pour activer le bouton me connecter
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //on vérifie le code une fois les 4 chiffres saisis
                if (passcode.getText().toString().length() == 4)
                    checkPassCode();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        passcode.addTextChangedListener(textWatcher);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getDialog() != null && getDialog().getWindow() != null && getContext() != null) {
            //on redimentionne le dialogue pr occuper tout l'écran horizontalement
            getDialog().getWindow().setLayout(AndroidUtils.getScreenWidth() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp24)), ViewGroup.LayoutParams.WRAP_CONTENT);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                getDialog().getWindow().setElevation(5f);
            }

           int transparentColor = ContextCompat.getColor(getContext(), android.R.color.transparent);
            getDialog().getWindow().setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{transparentColor, transparentColor}));

            passcode.requestFocus();
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    /**
     * Masque le clavier
     *
     * @param view la vue demandant à masquer le clavier
     */
    public void dismissKeyboard(View view) {
        if (getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Vérifie le code d'accès saisi par l'utilisateur
     */
    public void checkPassCode() {

        if (passcode.getText().toString().equals(code)) {
            dismissKeyboard(passcode);
            passCodeMatch = true;
            dismiss();
        } else {
            if (getActivity() != null)
                Toasty.error(getActivity(), getString(R.string.error_message_invalid_passcode), Toast.LENGTH_LONG, true).show();
            passcode.setText("");
            passcode.requestFocus();
        }


    }

    @Override
    public void dismiss() {
        //on efface le champ code d'accès
        onAuthResultListener.setOnAuthResult(passCodeMatch);
        passcode.setText(null);
        passCodeMatch = false;
        super.dismiss();
    }

    @OnClick(R.id.passcode_cancel)
    public void cancel(Button button) {
        dismissKeyboard(button);
        dismiss();
    }

    /**
     * Configure le listener sur les résultat d'authentification
     *
     * @param listener le listener sur les résultat d'authentification
     */
    public void setOnAuthResultListener(OnAuthResultListener listener) {
        this.onAuthResultListener = listener;
    }


}
