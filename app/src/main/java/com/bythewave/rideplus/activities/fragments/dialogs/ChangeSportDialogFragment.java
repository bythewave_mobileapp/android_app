package com.bythewave.rideplus.activities.fragments.dialogs;

import android.annotation.SuppressLint;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.adapters.viewholders.SportViewHolder;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.models.enums.Sport;
import com.lapptelier.smartrecyclerview.DrawableDividerItemDecoration;
import com.lapptelier.smartrecyclerview.MultiGenericAdapter;
import com.lapptelier.smartrecyclerview.SmartRecyclerView;
import com.lapptelier.smartrecyclerview.ViewHolderInteraction;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;

import java.util.Arrays;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * com.by_the_wave.ride_plus_android.activities.fragments.dialogs.AddDeviceDialogFragment
 * <p/>
 * Affiche une fenêtre surgissante permettant de changer le sport couramment affiché
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 13/11/2018
 */
public class ChangeSportDialogFragment extends BaseDialogFragment implements ViewHolderInteractionListener {

    private static final String INTENT_ANCHOR_Y = "intent_anchor_y";

    @BindView(R.id.change_sport_list)
    protected SmartRecyclerView recyclerView;

    // point d'ancrage de la popup à l'écran
    private int anchorY;

    // adapter pr la recyclerView
    private MultiGenericAdapter adapter;

    /**
     * Renvoi une nvelle instance du framgent
     *
     * @param anchorY la valeur de l'ordonnée de la position à laquelle ancrer la popup
     * @return une nvelle instance du fragment
     */
    public static ChangeSportDialogFragment newInstance(int anchorY) {
        ChangeSportDialogFragment fragment = new ChangeSportDialogFragment();
        Bundle args = new Bundle();
        args.putInt(INTENT_ANCHOR_Y, anchorY);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Constructeur vide
     */
    public ChangeSportDialogFragment() {
        if (getActivity() != null)
            RidePlusApplication.firebaseAnalytics.setCurrentScreen(getActivity(), Constants.SCREEN_SPORT, null);

        this.setStyle(STYLE_NO_TITLE, getTheme());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.anchorY = getArguments().getInt(INTENT_ANCHOR_Y);
        }
    }

    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = Objects.requireNonNull(inflater).inflate(R.layout.dialog_change_sport, container, true);
        ButterKnife.bind(this, view);

        //configuration de la liste, ss loading, ni vue vide
        adapter = new MultiGenericAdapter(Sport.class, SportViewHolder.class, R.layout.cell_sport, this);
        recyclerView.setAdapter(adapter);

        //configuration de la liste
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity(), RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DrawableDividerItemDecoration(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.divider), null, true));
        adapter.setHasMore(false);
        adapter.addAll(Arrays.asList(Sport.values()));

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @SuppressLint("RtlHardcoded")
    @Override
    public void onResume() {
        super.onResume();


        if (getDialog() != null && getDialog().getWindow() != null && getContext() != null) {
            //on redimentionne le dialogue pr occupe moins de place
            getDialog().getWindow().setLayout(Float.valueOf(getResources().getDimension(R.dimen.sport_cell_width)).intValue(), Float.valueOf(getResources().getDimension(R.dimen.sport_cell_height) * adapter.getItemCount()).intValue());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                getDialog().getWindow().setElevation(getResources().getDimension(R.dimen.default_elevation));
            }

            int transparentColor = ContextCompat.getColor(getContext(), android.R.color.transparent);
            getDialog().getWindow().setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{transparentColor, transparentColor}));

            getDialog().getWindow().setGravity(Gravity.TOP | Gravity.CENTER);
            WindowManager.LayoutParams windowAttributes = this.getDialog().getWindow().getAttributes();
            windowAttributes.dimAmount = 0.2f;
            windowAttributes.y = anchorY - Float.valueOf(getResources().getDimension(R.dimen.sport_cell_height) / 2f).intValue();
            getDialog().getWindow().setAttributes(windowAttributes);
        }

    }

    @Override
    public void onMessageEvent(BusMessage message) {
        if (message.type.equals(BusMessageType.CURRENT_SPORT_UPDATED)) {
            dismiss();
        }
        super.onMessageEvent(message);
    }

    @Override
    public void onItemAction(@Nullable Object object, int index, @Nullable ViewHolderInteraction viewHolderInteraction) {

    }
}
