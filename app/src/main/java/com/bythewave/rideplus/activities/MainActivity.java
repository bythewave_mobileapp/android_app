package com.bythewave.rideplus.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.activities.fragments.DeviceFragment;
import com.bythewave.rideplus.activities.fragments.ProfileFragment;
import com.bythewave.rideplus.activities.fragments.SessionListFragment;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.socks.library.KLog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * fr.reseauscet.scet_app_android.activities.MainActivity
 * <p/>
 * Activité principale, présentant le menu de l'app.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 19/01/2018
 */
public class MainActivity extends BaseAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.main_bottom_bar)
    protected BottomNavigationView bottomBar;

    List<Integer> previousTabSelectedIds; // Historique des onglets ouverts


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        previousTabSelectedIds = new ArrayList<>();

        bottomBar.setOnNavigationItemSelectedListener(menuItem -> {
            selectTab(menuItem.getItemId());
            return true;
        });
        bottomBar.setOnNavigationItemReselectedListener(menuItem -> {
            //on envoi un message sur le bus pour informer les fragment du scroll to top
            BusMessage.post(BusMessageType.SCROLL_TO_TOP, menuItem.getItemId());
        });

        selectTab(R.id.tab_devices);
    }


    /**
     * Affiche l'onglet correspondant, ou ouvre la webview pour afficher le contenu d'un tiroir du menu latéral
     *
     * @param tabId l'id de l'onglet à présenter
     */
    private void selectTab(@IdRes int tabId) {
        Fragment fragment = null;

        switch (tabId) {
            case R.id.tab_devices:
                fragment = getSupportFragmentManager().findFragmentByTag(DeviceFragment.class.getSimpleName());
                if (fragment == null) {
                    fragment = DeviceFragment.newInstance();
                }
                break;
            case R.id.tab_sessions:
                fragment = getSupportFragmentManager().findFragmentByTag(SessionListFragment.class.getSimpleName());
                if (fragment == null) {
                    fragment = SessionListFragment.newInstance(tabId);
                }
                break;
            case R.id.tab_profile:
                fragment = getSupportFragmentManager().findFragmentByTag(ProfileFragment.class.getSimpleName());
                if (fragment == null) {
                    fragment = ProfileFragment.newInstance(tabId);
                }
                break;
            default:
                break;
        }

        final Fragment finalFragment = fragment;
        if (finalFragment != null) {

            //on ajoute l'onglet à l'historique des onglets ouverts
            previousTabSelectedIds.add(0, tabId);

            new Handler().post(() -> {
                try {
                    // on remplace le fragment du contenu principal par le fragment correspondant à l'onglet sélectionné
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_content, finalFragment, finalFragment.getClass().getName())
                            .addToBackStack(null)
                            .commit();
                } catch (IllegalStateException exp) {
                    KLog.e(exp.getLocalizedMessage());
                }
            });
        }
    }


    @Override
    public void onBackPressed() {
        if (!previousTabSelectedIds.isEmpty()) {
            super.onBackPressed();
            //poping the previousTabSelectedIds list
            previousTabSelectedIds.remove(0);

            //updating the selected tab
            if (!previousTabSelectedIds.isEmpty()) {
                bottomBar.setSelectedItemId(previousTabSelectedIds.get(0));
            } else {
                finish();
            }
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case SESSION_ADDED:
                //on affiche l'onglet des sessions
                selectTab(R.id.tab_sessions);
                //on sélectionne l'onglet
                if (bottomBar != null)
                    bottomBar.setSelectedItemId(R.id.tab_sessions);
                break;
        }
        super.onMessageEvent(message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //on envoi un message avec un peu de délai sur le bus système pour recharger les vues concernées
        try {
            if (requestCode == Constants.REQUEST_CODE_RELOAD_NEEDED && resultCode == RESULT_OK) {
                BusMessage.postDelayed(BusMessageType.RELOAD_CONTENT, Constants.DEFAULT_UI_DELAY);
            } else if (requestCode == Constants.REQUEST_CODE_REFRESH_NEEDED && resultCode == RESULT_OK) {
                BusMessage.postDelayed(BusMessageType.REFRESH_CONTENT, Constants.DEFAULT_UI_DELAY);
            } else if (requestCode == Constants.REQUEST_ENABLE_BLE) {
                BusMessage.postDelayed(BusMessageType.RESTART_SCAN, Constants.DEFAULT_UI_DELAY);
            } else if (requestCode == Constants.REQUEST_SESSION_ADDED && resultCode == RESULT_OK) {
                BusMessage.postDelayed(BusMessageType.SESSION_ADDED, Constants.DEFAULT_UI_DELAY);
            } else if (requestCode == Constants.REQUEST_SESSION_ADDED && resultCode == RESULT_CANCELED) {
                BusMessage.postDelayed(BusMessageType.SYNC_NO_WAVES, Constants.DEFAULT_UI_DELAY);
            } else {
                BusMessage.postDelayed(BusMessageType.DISMISS_POPUP, Constants.DEFAULT_UI_DELAY);
                BusMessage.postDelayed(BusMessageType.REFRESH_CONTENT, Constants.DEFAULT_UI_DELAY);
            }
        } catch (IllegalStateException exp) {
            KLog.e(exp.getLocalizedMessage());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        checkPendingNewSession();
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        selectTab(item.getItemId());
        return true;
    }

    /**
     * Vérifie si une nouvelle session a été entamée mais pas terminée.
     */
    public void checkPendingNewSession() {
        //on vérifie si on a des fichiers de logs à envoyer
        File[] files = new File(getFilesDir(), Constants.DIR_GEO_LOG).listFiles();
        if (files != null && files.length > 0) {
            // on uploade tous les fichiers de logs restants
            for (File file : files)
                uploadGeoLog(file);
        }

        //on vérifie si on a un fichier de session temporaire
        File file = new File(getFilesDir(), Constants.NEW_SESSION_FILE);
        if (file.exists()) {
            //si oui, on demande à l'utilisateur si il souhaite reprendre sa nvelle session.
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //on relance la nvelle session
                        startActivityForResult(new Intent(this, NewSessionActivity.class), Constants.REQUEST_SESSION_ADDED);
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //on supprime le fichier temporaire
                        deleteFile(Constants.NEW_SESSION_FILE);
                        break;
                }
            };

            //showing an alert
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.RidePlusSimpleAlertDialogTheme);
            builder.setTitle(getString(R.string.existing_new_session_title)).setMessage(getString(R.string.existing_new_session_subtitle)).setPositiveButton(getString(R.string.continue_label), dialogClickListener)
                    .setNegativeButton(getString(R.string.cancel), dialogClickListener).show();

        }
    }

    /**
     * Envoi le geolog de la session sur firestore
     *
     * @param logFile le geolog à envoyer
     */
    private void uploadGeoLog(File logFile) {
        //on vérifie que la connexion internet soit valide
        if (AndroidUtils.isNetworkAvailable()) {
            final StorageReference geologRef = FirebaseStorage.getInstance().getReference().child(String.format(Locale.FRANCE, Constants.BUCKET_GEO_LOG, RidePlusApplication.currentUserManager.getCurrentUserId(), logFile.getName()));
            //ajout de metadonnées
            StorageMetadata metadata = new StorageMetadata.Builder()
                    .setContentType("text/plain")
                    .build();

            UploadTask uploadTask = geologRef.putFile(Uri.fromFile(logFile), metadata);
            uploadTask.continueWithTask(task -> {
                runOnUiThread(() -> {
                    if (task.isSuccessful()) {
                        //on supprime le fichier local
                        deleteFile(logFile.getName());
                    } else {
                        KLog.e(task.getException());
                        BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                    }
                });

                // On génère l'url de téléchargement
                return geologRef.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (!task.isSuccessful()) {
                    KLog.e(task.getException());
                }
            });
        }
    }


}
