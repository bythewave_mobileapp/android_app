package com.bythewave.rideplus.activities.fragments;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.activities.BaseAppCompatActivity;
import com.bythewave.rideplus.activities.Henson;
import com.bythewave.rideplus.activities.fragments.dialogs.ChangeSportDialogFragment;
import com.bythewave.rideplus.adapters.viewholders.SessionViewHolder;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Session;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.ui.SportButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.lapptelier.smartrecyclerview.DrawableDividerItemDecoration;
import com.lapptelier.smartrecyclerview.MultiGenericAdapter;
import com.lapptelier.smartrecyclerview.SmartRecyclerView;
import com.lapptelier.smartrecyclerview.ViewHolderInteraction;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * fr.reseauscet.scet_app_android.activities.fragments.DeviceFragment
 * <p/>
 * Fragment affichant la liste de session de l'utilisateur
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 24/01/2018
 */
public class SessionListFragment extends BaseFragment implements ViewHolderInteractionListener {


    @BindView(R.id.sessions_toolbar)
    protected Toolbar actionBar;

    // id de l'onglet correspondant au fragment
    private int tabId;

    @BindView(R.id.sessions_list)
    protected SmartRecyclerView recyclerView;

    @BindView(R.id.sessions_delete)
    protected Button deleteButton;

    @BindView(R.id.sessions_sport)
    protected SportButton sportButton;

    private DocumentSnapshot lastVisible; // index vers le dernier élément visible
    private Menu menu;

    // Adapter de la recyclerView
    private MultiGenericAdapter adapter;

    /**
     * Renvoi une instance de ce fragment
     *
     * @param tabId l'id de l'onglet correspondant
     * @return l'instance du fragment
     */
    public static Fragment newInstance(int tabId) {
        SessionListFragment fragment = new SessionListFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.INTENT_TAB_ID, tabId);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Constructeur vide
     */
    public SessionListFragment() {
        screen = Constants.SCREEN_SESSIONS;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.tabId = getArguments().getInt(Constants.INTENT_TAB_ID);
        }

        //on désactive le menu de l'action Bar sur ce fragment
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = Objects.requireNonNull(inflater).inflate(R.layout.fragment_sessions, container, false);
        ButterKnife.bind(this, view);

        //configuration de l'action bar
        ((BaseAppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(actionBar);
        ActionBar supportActionBar = ((BaseAppCompatActivity) getActivity()).getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
        }

        // Configuration de l'adapter
        adapter = new MultiGenericAdapter(Session.class, SessionViewHolder.class, R.layout.cell_session, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setRefreshListener(() -> getSessions(true));

        //configuration de la liste
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity(), RecyclerView.VERTICAL, false));
        recyclerView.setOnMoreListener((i, i1, i2) -> getSessions(false), 5);

        //on sette le texte de la vue vide
        recyclerView.setLoadingLayout(R.layout.layout_session_shimmer_loading);
        recyclerView.setLoadMoreLayout(R.layout.layout_loading_small);
        recyclerView.setEmptyLayout(R.layout.layout_empty);
        recyclerView.addItemDecoration(new DrawableDividerItemDecoration(getActivity().getDrawable(R.drawable.divider), null, true));
        Objects.requireNonNull(recyclerView.getSwipeLayout()).setColorSchemeResources(R.color.maya_blue, R.color.maya_blue, R.color.maya_blue, R.color.maya_blue);

        //on change le texte de la vue vide
        View emptyView = recyclerView.getEmptyView();
        TextView emptyText = Objects.requireNonNull(emptyView).findViewById(R.id.empty_text);
        if (emptyText != null) {
            emptyText.setText(getString(R.string.empty_sessions));
        }

        //par défaut, le bouton supprimer est masqué
        deleteButton.setAlpha(0f);
        deleteButton.setTranslationY(getActivity().getResources().getDimension(R.dimen.button_big_height) * 2);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                if (!adapter.isEmpty())
                    displayDeleteMode(true);
                return true;
            case R.id.menu_finish:
                displayDeleteMode(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.delete, menu);
        //par défaut on masque le bouton "Terminer" et le bouton supprimer désactivé
        if (menu != null && menu.findItem(R.id.menu_finish) != null) {
            menu.findItem(R.id.menu_finish).setVisible(false);
        }
        if (menu != null && menu.findItem(R.id.menu_delete) != null) {
            menu.findItem(R.id.menu_delete).setVisible(false);
        }
        this.menu = menu;
    }

    @Override
    public void onStart() {
        super.onStart();
        getSessions(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (recyclerView != null && !adapter.isEmpty())
            adapter.notifyDataSetChanged();
    }


    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onMessageEvent(BusMessage message) {
        super.onMessageEvent(message);
        switch (message.type) {
            case SCROLL_TO_TOP:
                if ((this.tabId == (int) message.content) && recyclerView != null)
                    recyclerView.scrollToTop();
                break;
            case OPEN_SESSION:
                if (message.content instanceof Session && getActivity() != null) {
                    getActivity().startActivityForResult(Henson.with(this.getActivity())
                            .gotoSessionActivity()
                            .session((Session) message.content)
                            .build(), Constants.REQUEST_CODE_REFRESH_NEEDED);
                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                }
                break;
            case ITEM_MARKED:
                displayDeleteButton(true);
                break;
            case CURRENT_SPORT_UPDATED:
                getSessions(true);
                break;
            case SESSION_UPDATED:
                getSessions(true);
                break;
            case NO_ITEM_MARKED:
                displayDeleteButton(false);
                break;
            case SESSION_DELETE_FAILED:
                if (getContext() != null)
                    Toasty.error(getContext(), getString(R.string.error_unknown), Toast.LENGTH_LONG, true).show();
                //si erreur à la suppression de sessions par lot, on recharge tt les sessions
                getSessions(true);
                break;
            case DELETE_ELEMENT:
                //on affiche une boite de dialogue pour faire confirmer par l'utilisateur la suppression du profile
                DialogInterface.OnClickListener dialogClickListener = (dialog, action) -> {
                    switch (action) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //on récupère la session associée
                            Session session = (Session) message.content;
                            if (session != null) {

                                // Firebase log
                                RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_SESSION_DELETED, null);

                                final Integer position = adapter.getObjectIndex(session);

                                session.toDelete = true;
                                FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS).document(session.getSessionId())
                                        .set(session).addOnFailureListener(exception -> {
                                    KLog.e(exception);
                                    BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                                    session.toDelete = false;
                                    //on replace la session
                                    adapter.insertAt(position, session);
                                });

                                //on supprime la cellule
                                adapter.removeAt(position);
                            }
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                };

                if (getContext() != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.RidePlusAlertDialogTheme);
                    builder.setTitle(
                            getString(R.string.remove_session_dialog_title)).
                            setMessage(getString(R.string.remove_session_dialog_subtitle)).
                            setPositiveButton(getString(R.string.delete), dialogClickListener).
                            setNegativeButton(getString(R.string.cancel), dialogClickListener).
                            show();
                }
                break;
        }
    }

    /**
     * Récupère une liste de sessions sur le serveur
     *
     * @param clear vrai si il faut purger la liste de sessions et récupérer les premières, faux sinon
     */
    public void getSessions(boolean clear) {
        if (clear) {
            recyclerView.displayLoadingView();

            lastVisible = null;
            //on désactive la sélection de sessions
            RidePlusApplication.sessionManager.setSelectingSessions(false);
            //on masque le bouton dans la barre d'état
            changeMenuVisibility(false, true);
        }

        Query query;
        if (lastVisible != null)
            query = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                    .whereEqualTo(Constants.FIELD_USER_ID, RidePlusApplication.currentUserManager.getCurrentUserId())
                    .whereEqualTo(Constants.FIELD_TO_DELETE, false)
                    .whereEqualTo(Constants.FIELD_SPORT, RidePlusApplication.configManager.getCurrentSport().value())
                    .orderBy(Constants.FIELD_CREATED_AT, Query.Direction.DESCENDING).limit(Constants.DEFAULT_API_ELEMENT_COUNT)
                    .startAfter(lastVisible);
        else
            query = FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                    .whereEqualTo(Constants.FIELD_USER_ID, RidePlusApplication.currentUserManager.getCurrentUserId())
                    .whereEqualTo(Constants.FIELD_TO_DELETE, false)
                    .whereEqualTo(Constants.FIELD_SPORT, RidePlusApplication.configManager.getCurrentSport().value())
                    .orderBy(Constants.FIELD_CREATED_AT, Query.Direction.DESCENDING).limit(Constants.DEFAULT_API_ELEMENT_COUNT);


        query.get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        List<Session> sessions = new ArrayList<>();
                        for (DocumentSnapshot document : task.getResult()) {
                            if (document != null && document.exists()) {
                                Session session = document.toObject(Session.class);
                                //on ajoute la session si elle est à afficher
                                if (session.isVisible())
                                    sessions.add(session);
                            }

                            //on conserve le dernier élément récupéré
                            lastVisible = task.getResult().getDocuments().get(task.getResult().size() - 1);
                        }

                        if (clear)
                            adapter.clear();

                        //on évalue si il reste des éléments supplémentaires
                        adapter.setHasMore(sessions.size() == Constants.DEFAULT_API_ELEMENT_COUNT);
                        adapter.appendAll(sessions);

                        //on modifie le bouton dans la barre d'état
                        changeMenuVisibility(false, false);

                    } else {
                        KLog.e("Error getting sessions: ", task.getException());
                        BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    /**
     * Change le sport de la vue : normal ou sélection de sessions à supprimer
     *
     * @param displayDelete vrai si il faut afficher le bouton supprimer, faux sinon
     */
    private void displayDeleteMode(boolean displayDelete) {
        //on modifie le bouton dans la barre d'état
        changeMenuVisibility(displayDelete, false);

        //on bloque le changement de sport si la liste des sessions est vide
        // on purge la liste des session marquées à chaque changement de sport
        RidePlusApplication.sessionManager.clearSessionToDelete();

        // si on sort du sport suppression, on masque quoi qu'il arrive le bouton
        if (!displayDelete)
            displayDeleteButton(false);

        RidePlusApplication.sessionManager.setSelectingSessions(displayDelete);
        adapter.notifyDataSetChanged();
    }

    /**
     * Modifie l'apparence du bouton dans la barre d'état
     *
     * @param displayFinish vrai pour afficher le bouton Terminer, faux pour afficher le bouton normal
     * @param disabled      vrai pour forcer le masquage du bouton, même si il y a des éléments dans la liste
     */
    private void changeMenuVisibility(boolean displayFinish, boolean disabled) {
        if (menu != null && menu.findItem(R.id.menu_finish) != null && menu.findItem(R.id.menu_delete) != null && menu.findItem(R.id.menu_delete_disabled) != null) {
            // on ne peut pas changer de sport si la liste des session est vide
            menu.findItem(R.id.menu_finish).setVisible(!disabled && !adapter.isEmpty() && displayFinish);
            menu.findItem(R.id.menu_delete).setVisible(!disabled && !adapter.isEmpty() && !displayFinish);
            //on force manuellement la couleur de l'icone supprimer
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (getContext() != null)
                    menu.findItem(R.id.menu_delete).setIconTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.maya_blue)));
            }
            menu.findItem(R.id.menu_delete_disabled).setVisible(disabled || adapter.isEmpty());
        }
        //on gère l'affichage du bouton de changement des sports
        sportButton.setEnabled(!disabled);
    }

    /**
     * Affiche ou masque le bouton de suppression de sessions
     *
     * @param displayDelete vrai pour afficher le bouton, faux pr le masquer
     */
    private void displayDeleteButton(boolean displayDelete) {
        ValueAnimator translateAnimation = ObjectAnimator.ofFloat(deleteButton, "translationY", displayDelete ? deleteButton.getHeight() * 2 : 0,
                displayDelete ? 0 : deleteButton.getHeight() * 2);
        translateAnimation.setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));
        translateAnimation.setInterpolator(new OvershootInterpolator());
        translateAnimation.start();

        deleteButton.animate().setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime)).setInterpolator(new LinearInterpolator()).alpha(displayDelete ? 1f : 0f);

    }

    @OnClick(R.id.sessions_delete)
    protected void bulkDeleteSession() {
        //on récupère les sessions marquées
        List<Session> sessions = RidePlusApplication.sessionManager.getSessionsToDelete();

        if (sessions.size() > 0) {

            //on affiche une boite de dialogue pour faire confirmer par l'utilisateur la suppression du sessions
            DialogInterface.OnClickListener dialogClickListener = (dialog, action) -> {
                switch (action) {
                    case DialogInterface.BUTTON_POSITIVE:
                        for (Session session : sessions) {
                            adapter.removeAt(adapter.getObjectIndex(session));
                        }
                        //on supprime les sessions
                        RidePlusApplication.sessionManager.deleteMarkedSessions();
                        RidePlusApplication.sessionManager.setSelectingSessions(false);
                        //on repasse la vue en sport normal
                        displayDeleteMode(false);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            };

            if (getContext() != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.RidePlusAlertDialogTheme);
                builder.setTitle(String.format(getString(R.string.remove_sessions_dialog_title), sessions.size(), sessions.size() > 1 ? getString(R.string.plural) : "")).
                        setMessage(getString(R.string.remove_sessions_dialog_subtitle)).
                        setPositiveButton(getString(R.string.delete), dialogClickListener).
                        setNegativeButton(getString(R.string.cancel), dialogClickListener).
                        show();
            }
        }
    }

    @OnClick(R.id.sessions_sport)
    protected void changeSport() {
        if (getActivity() != null && sportButton.isEnabled()) {
            int[] location = new int[2];
            sportButton.getLocationOnScreen(location);

            ChangeSportDialogFragment.newInstance(location[1]).show(getActivity().getSupportFragmentManager(), "");
        }
    }

    @Override
    public void onItemAction(@Nullable Object object, int index, @Nullable ViewHolderInteraction viewHolderInteraction) {

    }
}
