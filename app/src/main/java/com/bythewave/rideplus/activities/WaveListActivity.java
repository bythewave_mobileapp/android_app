package com.bythewave.rideplus.activities;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.adapters.viewholders.WaveViewHolder;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Session;
import com.bythewave.rideplus.models.Wave;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lapptelier.smartrecyclerview.DrawableDividerItemDecoration;
import com.lapptelier.smartrecyclerview.MultiGenericAdapter;
import com.lapptelier.smartrecyclerview.SmartRecyclerView;
import com.lapptelier.smartrecyclerview.ViewHolderInteraction;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;
import com.socks.library.KLog;

import java.util.List;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * fr.reseauscet.scet_app_android.activities.fragments.DeviceFragment
 * <p/>
 * Fragment affichant la liste de vague de l'utilisateur
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 24/01/2018
 */
public class WaveListActivity extends BaseAppCompatActivity implements ViewHolderInteractionListener {


    @BindView(R.id.waves_toolbar)
    protected Toolbar actionBar;

    @BindView(R.id.waves_list)
    protected SmartRecyclerView recyclerView;

    @BindView(R.id.waves_delete)
    protected Button deleteButton;

    @BindView(R.id.toolbar_title)
    protected TextView title;

    @InjectExtra
    public Session session; // la session à afficher

    public int selectedWaveIndex; // la vague sélectionnée

    private Menu menu;

    // Adapter pr la recyclerView
    private MultiGenericAdapter adapter;

    /**
     * Constructeur vide
     */
    public WaveListActivity() {
        screen = Constants.SCREEN_SESSION_WAVES;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wave_list);
        ButterKnife.bind(this);
        Dart.inject(this);

        //configuration de l'action bar
        this.setSupportActionBar(actionBar);
        ActionBar supportActionBar = this.getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        //on sette le titre
        title.setText(AndroidUtils.getSportString("_tab_rides"));

        // Configuration de l'adapter
        adapter = new MultiGenericAdapter(Wave.class, WaveViewHolder.class, R.layout.cell_wave, this);
        recyclerView.setAdapter(adapter);

        //configuration de la liste
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        //on sette le texte de la vue vide
        recyclerView.setEmptyLayout(R.layout.layout_empty);
        recyclerView.enableSwipeToRefresh(false);
        recyclerView.addItemDecoration(new DrawableDividerItemDecoration(this.getDrawable(R.drawable.divider), null, true));
        Objects.requireNonNull(recyclerView.getSwipeLayout()).setColorSchemeResources(R.color.maya_blue, R.color.maya_blue, R.color.maya_blue, R.color.maya_blue);

        //on change le texte de la vue vide
        View emptyView = recyclerView.getEmptyView();
        TextView emptyText = Objects.requireNonNull(emptyView).findViewById(R.id.empty_text);
        if (emptyText != null) {
            emptyText.setText(AndroidUtils.getSportString("_empty_session"));
        }

        //par défaut, le bouton supprimer est masqué
        deleteButton.setAlpha(0f);
        deleteButton.setTranslationY(getResources().getDimension(R.dimen.button_big_height) * 2);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_delete:
                if (!adapter.isEmpty())
                    displayDeleteMode(true);
                return true;
            case R.id.menu_finish:
                displayDeleteMode(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu != null) {
            getMenuInflater().inflate(R.menu.delete, menu);
            //par défaut on masque le bouton "Terminer" et le bouton supprimer désactivé
            if (menu.findItem(R.id.menu_finish) != null)
                menu.findItem(R.id.menu_finish).setVisible(false);
            if (menu.findItem(R.id.menu_delete) != null)
                menu.findItem(R.id.menu_delete).setVisible(false);
            this.menu = menu;
            changeMenuVisibility(false);
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (session != null) {
            //on ajoute les vagues
            adapter.addAll(session.getVisibleWaves());
        }

        //on repasse la vue en sport normal
        //on utilise un postDelayed pour que le changment de sport soit fait une fois que l'UI est affichée.
        //sinon, le mouvement du bouton Supprimer n'est pas effectué
        recyclerView.postDelayed(() -> displayDeleteMode(false), Constants.DEFAULT_UI_DELAY);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_SESSION, session);
        if (selectedWaveIndex > -1)
            intent.putExtra(Constants.INTENT_SELECTED_WAVE_INDEX, selectedWaveIndex);
        setResult(RESULT_OK, intent);
        super.finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }


    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case ITEM_MARKED:
                // si l'utilisateur est en train de supprimer des vagues,
                // on affiche le bouton supprimer dès qu'au moins une est sélectionnée
                if (RidePlusApplication.sessionManager.isSelectingWaves()) {
                    displayDeleteButton(true);
                } else {
                    // sinon, on ferme la vue des vagues et on sélectionne la vague cliquée
                    // dans la liste des vagues de la vue session
                    if (message.content instanceof Wave) {
                        // on renvoi l'index de la vague parmi les vauges de la session,
                        // sinon cela pose un soucis pour la retrouve ensuite dans la liste des vagues de la vue session
                        selectedWaveIndex = adapter.getObjectIndex(message.content);
                        finish();
                    }
                }
                break;
            case NO_ITEM_MARKED:
                displayDeleteButton(false);
                break;
            case SESSION_DELETE_FAILED:
                Toasty.error(this, getString(R.string.error_unknown), Toast.LENGTH_LONG, true).show();
                //si erreur à la suppression de vagues par lot, on recharge la session
                FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                        .document(session.getSessionId())
                        .get().addOnCompleteListener(getDocumentTask -> {
                    if (getDocumentTask.isSuccessful()) {
                        DocumentSnapshot document = getDocumentTask.getResult();
                        if (document != null && document.exists()) {
                            session = document.toObject(Session.class);
                            //on recharge la liste
                            adapter.addAll(session.getVisibleWaves());
                        }
                    }
                });
                break;
            case DELETE_ELEMENT:
                //on affiche une boite de dialogue pour faire confirmer par l'utilisateur la suppression du profile
                DialogInterface.OnClickListener dialogClickListener = (dialog, action) -> {
                    switch (action) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //on récupère la vague associée
                            Wave wave = (Wave) message.content;
                            if (wave != null) {

                                final Integer position = adapter.getObjectIndex(wave);


                                // Firebase log
                                RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_WAVE_DELETED, null);

                                for (Wave waveToDelete : session.waves) {
                                    if (waveToDelete.equals(wave)) {
                                        waveToDelete.toDelete = true;
                                    }
                                }

                                //on sauvegarde la session sur le serveur
                                FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                                        .document(session.getSessionId())
                                        .set(session)
                                        .addOnFailureListener(exception -> {
                                            KLog.e(exception);
                                            BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                                            adapter.insertAt(position, wave);
                                        });

                                //on supprime la cellule
                                adapter.removeAt(position);
                            }
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.RidePlusAlertDialogTheme);
                builder.setTitle(
                        AndroidUtils.getSportString("_remove_ride_dialog_title")).
                        setMessage(AndroidUtils.getSportString("_remove_ride_dialog_subtitle")).
                        setPositiveButton(getString(R.string.delete), dialogClickListener).
                        setNegativeButton(getString(R.string.cancel), dialogClickListener).
                        show();
                break;
        }
        super.onMessageEvent(message);
    }


    /**
     * Change le sport de la vue : normal ou sélection de vagues à supprimer
     *
     * @param displayDelete vrai si il faut afficher le bouton supprimer, faux sinon
     */
    private void displayDeleteMode(boolean displayDelete) {
        //on modifie le bouton dans la barre d'état
        changeMenuVisibility(displayDelete);

        //on bloque le changement de sport si la liste des vagues est vide
        // on purge la liste des vague marquées à chaque changement de sport
        RidePlusApplication.sessionManager.clearWaveToDelete();

        // si on sort du sport suppression, on masque quoi qu'il arrive le bouton
        if (!displayDelete)
            displayDeleteButton(false);

        RidePlusApplication.sessionManager.setSelectingWaves(displayDelete);
        adapter.notifyDataSetChanged();
    }

    /**
     * Modifie l'apparence du bouton dans la barre d'état
     *
     * @param displayFinish vrai pour afficher le bouton Terminer, faux pour afficher le bouton normal
     */
    private void changeMenuVisibility(boolean displayFinish) {
        if (menu != null && menu.findItem(R.id.menu_finish) != null && menu.findItem(R.id.menu_delete) != null && menu.findItem(R.id.menu_delete_disabled) != null) {
            // on ne peut pas changer de sport si la liste des vague est vide
            menu.findItem(R.id.menu_finish).setVisible(!adapter.isEmpty() && displayFinish);
            menu.findItem(R.id.menu_delete).setVisible(!adapter.isEmpty() && !displayFinish);
            //on force manuellement la couleur de l'icone supprimer
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                menu.findItem(R.id.menu_delete).setIconTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.maya_blue)));
            }
            menu.findItem(R.id.menu_delete_disabled).setVisible(adapter.isEmpty());
        }
    }

    /**
     * Affiche ou masque le bouton de suppression de sessions
     *
     * @param displayDelete vrai pour afficher le bouton, faux pr le masquer
     */
    private void displayDeleteButton(boolean displayDelete) {
        ValueAnimator translateAnimation = ObjectAnimator.ofFloat(deleteButton, "translationY", displayDelete ? deleteButton.getHeight() * 2 : 0,
                displayDelete ? 0 : deleteButton.getHeight() * 2);
        translateAnimation.setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));
        translateAnimation.setInterpolator(new OvershootInterpolator());
        translateAnimation.start();

        deleteButton.animate().setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime)).setInterpolator(new LinearInterpolator()).alpha(displayDelete ? 1f : 0f);

    }

    @OnClick(R.id.waves_delete)
    protected void bulkDeleteWave() {
        //on récupère les vagues marquées
        List<Wave> waves = RidePlusApplication.sessionManager.getWavesToDelete();

        if (waves.size() > 0) {

            //on affiche une boite de dialogue pour faire confirmer par l'utilisateur la suppression du vagues
            DialogInterface.OnClickListener dialogClickListener = (dialog, action) -> {
                switch (action) {
                    case DialogInterface.BUTTON_POSITIVE:
                        for (Wave wave : waves) {
                            adapter.removeAt(adapter.getObjectIndex(wave));
                        }
                        //on supprime les vagues
                        RidePlusApplication.sessionManager.deleteMarkedWaves(session);
                        RidePlusApplication.sessionManager.setSelectingWaves(false);
                        //on repasse la vue en sport normal
                        displayDeleteMode(false);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.RidePlusAlertDialogTheme);
            builder.setTitle(
                    String.format(
                            AndroidUtils.getSportString("_remove_rides_dialog_title"), waves.size(), waves.size() > 1 ? getString(R.string.plural) : "")).
                    setMessage(AndroidUtils.getSportString("_remove_rides_dialog_subtitle")).
                    setPositiveButton(getString(R.string.delete), dialogClickListener).
                    setNegativeButton(getString(R.string.cancel), dialogClickListener).
                    show();
        }
    }

    @Override
    public void onItemAction(Object o, int i, ViewHolderInteraction viewHolderInteraction) {

    }
}
