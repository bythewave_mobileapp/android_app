package com.bythewave.rideplus.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.util.Pair;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.BuildConfig;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.activities.fragments.dialogs.LoadingDialogFragment;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.receivers.LocationUpdatesBroadcastReceiver;
import com.bythewave.rideplus.tasks.ParseLocationsTask;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;
import com.socks.library.KLog;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import me.tankery.permission.PermissionRequestActivity;

/**
 * fr.reseauscet.scet_app_android.activities.MainActivity
 * <p/>
 * Activité présentant une nouvelle session au GPS
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 19/01/2018
 */
public class NewSessionActivity extends BaseAppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    // Intervalle de réception des coordonnées GPS préférable et plus rapides
    private static final long PREFERED_POLLING_INTERVAL = 4000;
    private static final long FASTEST_POLLING_INTERVAL = 2000;
    private static final long MAX_WAIT_TIME = PREFERED_POLLING_INTERVAL * 5;

    @BindView(R.id.new_session_image)
    protected ImageView syncImage;
    @BindView(R.id.new_session_text)
    protected TextView newSessionText;
    @BindView(R.id.new_session_pulse_0)
    protected ImageView pulse0;
    @BindView(R.id.new_session_pulse_1)
    protected ImageView pulse1;
    @BindView(R.id.new_session_pulse_2)
    protected ImageView pulse2;

    private GoogleApiClient mGoogleApiClient; // client Google API
    private LocationRequest locationRequest; // paramètres de geolocalisation

    // flag indiquant si une nvelle session a bien été créé
    private boolean sessionAdded;

    // popup d'attente lors de l'enregistrement de la session
    private LoadingDialogFragment loadingDialogFragment;


    /**
     * Constructeur vide
     */
    public NewSessionActivity() {
        screen = Constants.SCREEN_NEW_SESSION;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_session);

        ButterKnife.bind(this);

        // changement du texte avec couleurs

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            newSessionText.setText(Html.fromHtml(getString(R.string.new_session_text), Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            newSessionText.setText(Html.fromHtml(getString(R.string.new_session_text)), TextView.BufferType.SPANNABLE);
        }

        Animation pulseAnim = AnimationUtils.loadAnimation(this, R.anim.pulse);
        syncImage.startAnimation(pulseAnim);
        Animation expandFadeAnim0 = AnimationUtils.loadAnimation(this, R.anim.expand_fade);
        pulse0.startAnimation(expandFadeAnim0);
        Animation expandFadeAnim1 = AnimationUtils.loadAnimation(this, R.anim.expand_fade);
        expandFadeAnim1.setStartOffset(600);
        pulse1.startAnimation(expandFadeAnim1);
        Animation expandFadeAnim2 = AnimationUtils.loadAnimation(this, R.anim.expand_fade);
        expandFadeAnim2.setStartOffset(1200);
        pulse2.startAnimation(expandFadeAnim2);

        // Check if the user revoked runtime permissions.
        if (!checkPermissions()) {
            requestPermissions();
        }

        configureLocationTracking();

        //controle des permissions
        PermissionRequestActivity.start(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                getString(R.string.error_message_ble_location_permission_missing), getString(R.string.error_message_ble_location_permission_missing));
    }

    @Override
    public void onResume() {
        super.onResume();

        requestLocationUpdates();
    }

    @Override
    public void finish() {

        syncImage.clearAnimation();
        pulse0.clearAnimation();
        pulse1.clearAnimation();
        pulse2.clearAnimation();

        if (sessionAdded)
            setResult(Activity.RESULT_OK, new Intent());
        else
            setResult(Activity.RESULT_CANCELED, new Intent());
        super.finish();

        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void configureLocationTracking() {
        if (mGoogleApiClient != null) {
            return;
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .build();

        //configuration du suivi GPS
        locationRequest = new LocationRequest();
        locationRequest.setInterval(PREFERED_POLLING_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_POLLING_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setMaxWaitTime(MAX_WAIT_TIME);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, locationSettingsResponse -> builder.addLocationRequest(locationRequest));

        //si erreur d'autorisation, on demande à l'utilisateur de la résoudre
        task.addOnFailureListener(this, e -> {
            if (e instanceof ResolvableApiException) {
                try {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(this,
                            Constants.REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException sendEx) {
                    KLog.e(sendEx);
                }
            }
        });

    }

    @OnClick(R.id.new_session_button)
    public void dialogButtonFinish() {
        //on arrête l'enregistrement de la nouvelle session
        removeLocationUpdates();

        displayLoadingFragment();

        // on analyse les positions enregistrées
        parseLocations();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        KLog.i("GoogleApiClient connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        final String text = "Connection suspended";
        KLog.w(text + ": Error code: " + i);
        Toasty.error(this, getString(R.string.error_google_play), Toast.LENGTH_LONG, true).show();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        final String text = "Exception while connecting to Google Play services";
        KLog.w(text + ": " + connectionResult.getErrorMessage());
        Toasty.error(this, getString(R.string.error_google_play), Toast.LENGTH_LONG, true).show();
    }

    // Gestion des permissions

    /**
     * Contrôle l'état des persmissions
     *
     * @return vrai si la persmission d'utiliser la position est donnée
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Demande la permission d'exploiter la position courante à l'utilisateur
     */
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale) {
            KLog.i("Displaying permission rationale to provide additional context.");

            Snackbar.make(
                    findViewById(R.id.new_session_layout),
                    R.string.error_message_ble_location_permission_missing,
                    Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(ContextCompat.getColor(this, R.color.maya_blue))
                    .setAction(R.string.continue_label, view -> {
                        // Request permission
                        ActivityCompat.requestPermissions(NewSessionActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                Constants.REQUEST_CODE_PERMISSIONS);
                    })
                    .show();
        } else {
            KLog.i("Requesting permission");
            ActivityCompat.requestPermissions(NewSessionActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.REQUEST_CODE_PERMISSIONS);
        }
    }

    /**
     * Callback appelé lorsque la permission est donnée ou refusée
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        KLog.i("onRequestPermissionResult");
        if (requestCode == Constants.REQUEST_CODE_PERMISSIONS) {
            if (grantResults.length <= 0) {
                KLog.i("User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                configureLocationTracking();
            } else {
                Snackbar.make(
                        findViewById(R.id.new_session_layout),
                        R.string.error_message_ble_location_permission_missing_short,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, view -> {
                            //on renvoi aux paramètres de l'app pr forcer l'utilisateur à changer
                            Intent intent = new Intent();
                            intent.setAction(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        })
                        .show();
            }
        }
    }

    /**
     * Renvoi l'intent vers le service en cours pour récupérer les positions
     *
     * @return l'intent vers le service de position
     */
    private PendingIntent getPendingIntent() {
        Intent intent = new Intent(this, LocationUpdatesBroadcastReceiver.class);
        intent.setAction(Constants.INTENT_PROCESS_LOCATION);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * Lance le service de récupération des positions
     */
    public void requestLocationUpdates() {
        try {
            KLog.i("Starting location updates");
            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(
                    locationRequest, getPendingIntent());
        } catch (SecurityException e) {
            KLog.e(e);
        }
    }

    /**
     * Arrête le service de récupération des positions
     */
    public void removeLocationUpdates() {
        KLog.i("Removing location updates");
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(
                getPendingIntent());
    }

    @SuppressLint("CheckResult")
    private void parseLocations() {
        new ParseLocationsTask(new WeakReference<>(getApplicationContext())) {
            @Override
            protected void onPostExecute(Boolean success) {
                super.onPostExecute(success);
                BusMessage.post(BusMessageType.SESSION_ADDED, new Pair(success, sessionAdded));
            }
        }.execute((Void) null);

    }

    /**
     * Affiche le fragment d'attente
     */
    private void displayLoadingFragment() {
        if (loadingDialogFragment == null)
            loadingDialogFragment = LoadingDialogFragment.newInstance(getString(R.string.computing_new_session));
        if (loadingDialogFragment != null && !loadingDialogFragment.isVisible() && getSupportFragmentManager() != null)
            loadingDialogFragment.show(getSupportFragmentManager(), "");
    }

    /**
     * Masque le fragment d'attente
     */
    private void dismissLoadingFragment() {
        if (loadingDialogFragment != null && loadingDialogFragment.isVisible())
            loadingDialogFragment.dismiss();
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        super.onMessageEvent(message);
        if (message.type.equals(BusMessageType.SESSION_ADDED) && message.content instanceof Pair) {
            runOnUiThread(() -> {
                sessionAdded = (boolean) ((Pair) message.content).second;
                if (!(boolean) ((Pair) message.content).first) {
                    Toasty.error(getApplicationContext(), getString(R.string.error_new_session)).show();
                }
                dismissLoadingFragment();
                finish();
            });
        }
    }
}