package com.bythewave.rideplus.activities.fragments.dialogs;

import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * com.by_the_wave.ride_plus_android.activities.fragments.dialogs.AddDeviceDialogFragment
 * <p/>
 * Affiche une fenêtre surgissante permettant d'afficher une aide à la connexion
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 10/02/2018
 */
public class HelpDialogFragment extends BaseDialogFragment {

    private static final int DEVICE_ON = 0;
    private static final int PERMISSION = 1;

    @BindView(R.id.device_add_dot_0)
    protected ImageView dot0;
    @BindView(R.id.device_add_dot_1)
    protected ImageView dot1;

    @BindView(R.id.device_add_dot_2)
    protected ImageView dot2;

    @BindView(R.id.device_tutorial_button)
    protected Button button;

    @BindView(R.id.device_add_page_0)
    protected ConstraintLayout pageTutorial;
    @BindView(R.id.device_add_page_1)
    protected ConstraintLayout pageBluetooth;
    @BindView(R.id.device_add_page_2)
    protected ConstraintLayout pageSync;
    @BindView(R.id.device_add_page_3)
    protected ConstraintLayout pageComplete;

    // la "page" de tutorial affichée
    int page;

    /**
     * Renvoi une nvelle instance du framgent
     *
     * @return une nvelle instance du fragment
     */
    public static HelpDialogFragment newInstance() {
        return new HelpDialogFragment();
    }

    /**
     * Constructeur vide
     */
    public HelpDialogFragment() {
        if (getActivity() != null)
            RidePlusApplication.firebaseAnalytics.setCurrentScreen(getActivity(), Constants.SCREEN_HELP, null);
        this.setStyle(STYLE_NO_TITLE, getTheme());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_device, container, true);
        ButterKnife.bind(this, view);

        configureView();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (getDialog() != null && getDialog().getWindow() != null) {
            //on redimentionne le dialogue pr occuper tout l'écran
            getDialog().getWindow().setLayout(AndroidUtils.getScreenWidth() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp16)), AndroidUtils.getScreenHeight() - AndroidUtils.dp(getContext().getResources().getDimension(R.dimen.dp32)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                getDialog().getWindow().setElevation(5f);
            }

           int transparentColor = ContextCompat.getColor(getContext(), android.R.color.transparent);
            getDialog().getWindow().setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{transparentColor, transparentColor}));
        }

        //par défaut, on affiche la première page
        page = DEVICE_ON;
    }

    /**
     * Configure la vue selon la page courante
     */
    public void configureView() {

        dot0.setEnabled(false);
        dot1.setEnabled(false);
        dot0.setVisibility(View.VISIBLE);
        dot1.setVisibility(View.VISIBLE);
        dot2.setVisibility(View.GONE);
        button.setEnabled(true);

        pageSync.setVisibility(View.GONE);
        pageComplete.setVisibility(View.GONE);

        switch (page) {
            case DEVICE_ON:
                dot0.setEnabled(true);
                pageTutorial.setAlpha(1f);
                pageBluetooth.setAlpha(0f);
                button.setText(R.string.next);
                break;
            case PERMISSION:
                dot1.setEnabled(true);
                pageTutorial.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(0f);
                pageBluetooth.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(1f);
                button.setText(R.string.close);
                break;
        }

    }

    @OnClick(R.id.device_tutorial_button)
    public void dialogButtonAction() {
        if (page < PERMISSION) {
            page += 1;
            configureView();
        } else {
            dismiss();
        }
    }

    @OnClick(R.id.device_tutorial_cancel)
    public void dialogButtonCancel() {
        dismiss();
    }

}
