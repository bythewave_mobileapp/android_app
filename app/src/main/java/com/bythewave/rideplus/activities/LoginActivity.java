package com.bythewave.rideplus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.common.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * fr.reseauscet.scet_app_android.activities.MailLoginActivity
 * <p/>
 * Activité permettant à l'utilisateur de s'autentifier par mail
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 19/01/2018
 */
public class LoginActivity extends BaseAppCompatActivity {

    @BindView(R.id.login_mail)
    protected Button buttonMail;

    public LoginActivity() {
        screen = Constants.SCREEN_LOGIN;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }
    
    @OnClick(R.id.login_mail)
    public void displayMailLogin(View view) {
        //on affiche l'activité de saisie de l'email
        Intent intent = new Intent(LoginActivity.this, MailLoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

}
