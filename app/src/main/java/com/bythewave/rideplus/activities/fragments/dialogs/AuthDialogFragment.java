package com.bythewave.rideplus.activities.fragments.dialogs;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.socks.library.KLog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

/**
 * com.by_the_wave.ride_plus_android.activities.fragments.dialogs.AddDeviceDialogFragment
 * <p/>
 * Affiche une fenêtre surgissante permettant à l'utilisateur de confirmer son mot de passe
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 02/02/2018
 */
public class AuthDialogFragment extends BaseDialogFragment {

    /**
     * Interface utilisée par l'activité ou le fragment parent pour obtenir le résultat de l'authentification
     */
    public interface OnAuthResultListener {
        /**
         * Méthode appelée lorsque la réauthentification a été terminée.
         *
         * @param authResult le résultat de l'authentification : vrai si authentifié, faux sinon
         */
        void setOnAuthResult(boolean authResult);

    }

    @BindView(R.id.auth_password_field)
    protected EditText password;

    @BindView(R.id.auth_password_label)
    protected TextView passwordLabel;

    @BindView(R.id.auth_submit)
    protected Button submitButton;

    @BindView(R.id.auth_forgotten_password)
    protected Button forgottenPasswordButton;

    private OnAuthResultListener onAuthResultListener; // listener pour obtenir les résultats de l'authentification

    private String text; // le texte à afficher au dessus du champ mot de passe

    /**
     * Renvoi une nvelle instance du framgent
     *
     * @param text le texte à afficher au dessus du champ mot de passe
     * @return une nvelle instance du fragment
     */
    public static AuthDialogFragment newInstance(String text) {
        AuthDialogFragment fragment = new AuthDialogFragment();
        Bundle args = new Bundle();
        args.putString(Constants.INTENT_LOADING_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Constructeur vide
     */
    public AuthDialogFragment() {
        this.setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.text = getArguments().getString(Constants.INTENT_LOADING_TEXT);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_auth, container, true);
        ButterKnife.bind(this, view);

        //ajout d'un controle de saisie pour activer le bouton me connecter
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //on active le bouton Me connecter si le login et le mdp sont renseignés
                submitButton.setEnabled(!TextUtils.isEmpty(password.getText()));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        password.addTextChangedListener(textWatcher);

        //prise en compte du tap sur le bouton "go" du clavier lorsque l'on édite le mot de passe
        password.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_GO) {
                checkPassword(submitButton);
                return true;
            } else {
                return false;
            }
        });

        passwordLabel.setText(text);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getDialog() != null && getDialog().getWindow() != null && getContext() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                getDialog().getWindow().setElevation(5f);
            }

           int transparentColor = ContextCompat.getColor(getContext(), android.R.color.transparent);
            getDialog().getWindow().setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{transparentColor, transparentColor}));

            password.requestFocus();
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        }

    }

    /**
     * Méthode permettant d'activer ou désactiver les interactions de l'utilisateur avec l'ensemble des boutons de la vue.
     * En cas de désactivation, affiche l'indicateur d'activité
     *
     * @param enable vrai pour activer les boutons, faux pour les désactiver et afficher l'indicateur d'activité
     */
    private void enableButtons(boolean enable) {
        submitButton.animate().setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).setInterpolator(new LinearInterpolator()).alpha(enable ? 1f : 0f);
        password.setEnabled(enable);
        forgottenPasswordButton.setEnabled(enable);
    }


    /**
     * Masque le clavier
     *
     * @param view la vue demandant à masquer le clavier
     */
    public void dismissKeyboard(View view) {
        if (getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @OnClick(R.id.auth_submit)
    public void checkPassword(Button button) {
        enableButtons(false);

        dismissKeyboard(button);

        //on réauthentifie l'utilisateur
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null && !TextUtils.isEmpty(user.getEmail())) {
            AuthCredential credential = EmailAuthProvider
                    .getCredential(user.getEmail(), password.getText().toString());
            user.reauthenticate(credential)
                    .addOnCompleteListener(task -> {
                        //on informe le listener du résultat
                        if (onAuthResultListener != null)
                            onAuthResultListener.setOnAuthResult(task.isSuccessful());
                        enableButtons(true);
                        if (task.isSuccessful()) {
                            //auth ok, on ferme le dialogue
                            dismiss();
                        } else {
                            //on affiche un message d'erreur si la tache échoue
                            if (task.getException() instanceof FirebaseNetworkException) {
                                BusMessage.post(BusMessageType.ERROR_OFFLINE);
                            } else if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                BusMessage.post(BusMessageType.ERROR_WRONG_CREDENTIAL);
                            } else {
                                BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                            }
                        }
                    });
        }
    }

    @OnClick(R.id.auth_forgotten_password)
    public void resendPassword(Button button) {
        enableButtons(false);

        dismissKeyboard(button);

        //on envoi un nouveau mot de passe à l'utilisateur
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null && !TextUtils.isEmpty(user.getEmail())) {
            FirebaseAuth.getInstance().sendPasswordResetEmail(user.getEmail())
                    .addOnCompleteListener(task -> {
                        enableButtons(true);
                        if (task.isSuccessful()) {
                            //on affiche un message d'information
                            if (getActivity() != null)
                                Toasty.error(getActivity(), getString(R.string.password_forgotten_toast), Toast.LENGTH_LONG, true).show();
                        } else {
                            //on réactive les boutons
                            enableButtons(true);
                            if (task.getException() != null) {
                                KLog.e("sendPasswordResetEmail:failure", task.getException());
                                if (task.getException() instanceof FirebaseNetworkException) {
                                    BusMessage.post(BusMessageType.ERROR_OFFLINE);
                                } else {
                                    BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                                }
                            }
                        }
                    });
        }
    }

    @Override
    public void dismiss() {
        //on efface le champ mot de passe
        password.setText(null);

        super.dismiss();
    }

    @OnClick(R.id.auth_cancel)
    public void cancel(Button button) {
        dismissKeyboard(button);

        dismiss();
    }

    /**
     * Configure le listener sur les résultat d'authentification
     *
     * @param listener le listener sur les résultat d'authentification
     */
    public void setOnAuthResultListener(OnAuthResultListener listener) {
        this.onAuthResultListener = listener;
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        super.onMessageEvent(message);
        if (getActivity() != null) {
            switch (message.type) {
                case ERROR_WRONG_CREDENTIAL:
                    Toasty.error(getActivity(), getString(R.string.error_message_invalid_credential), Toast.LENGTH_LONG, true).show();
                    break;
            }
        }
        super.onMessageEvent(message);
    }

}
