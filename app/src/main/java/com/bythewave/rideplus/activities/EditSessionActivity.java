package com.bythewave.rideplus.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Session;
import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * com.by_the_wave.ride_plus_android.activities.EditSessionActivity
 * <p/>
 * Activité permettant d'éditer les informations d'une session
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 21/02/2018
 */
public class EditSessionActivity extends BaseAppCompatActivity {

    @BindView(R.id.edit_session_toolbar)
    protected Toolbar actionBar;

    @BindView(R.id.edit_session_spot_name)
    protected EditText customName;

    @BindView(R.id.edit_session_comment)
    protected EditText comment;

    @InjectExtra
    public Session session;

    /**
     * Constructeur vide
     */
    public EditSessionActivity() {
        screen = Constants.SCREEN_DEVICE_EDIT;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_session);
        ButterKnife.bind(this);
        Dart.inject(this);

        //configuration de l'action bar
        setSupportActionBar(actionBar);
        ActionBar supportActionBar = getSupportActionBar();

        if (supportActionBar != null) {
            supportActionBar.setDisplayShowCustomEnabled(true);
            supportActionBar.setDisplayShowTitleEnabled(false);

            supportActionBar.setDisplayHomeAsUpEnabled(true);
            @SuppressLint("PrivateResource") final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(ContextCompat.getColor(this,R.color.maya_blue), PorterDuff.Mode.SRC_ATOP);
            supportActionBar.setHomeAsUpIndicator(upArrow);
        }

        //on configure les éléments de la vue
        configureView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED, new Intent());
                finish();
                return true;
            case R.id.menu_save:
                setResult(Activity.RESULT_OK, new Intent());
                saveSession();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu != null) {
            getMenuInflater().inflate(R.menu.save, menu);
        }
        return true;
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_SESSION, session);
        setResult(RESULT_OK, intent);
        super.finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    /**
     * Configure la vue avec l'appareil courant de l'utilisateur
     */
    private void configureView() {
        if (session != null) {
            customName.setText(session.spotName);
            comment.setText(session.comment);
            customName.requestFocus();
        }
    }


    /**
     * Enregistre les changements sur l'appareil et retourne à l'écran précédent
     */
    private void saveSession() {
        if (session != null) {
            //on enregistre les changement sur l'appareil
            session.spotName = customName.getText().toString();
            session.comment = comment.getText().toString();

            //on envoi les informations au boitier
            RidePlusApplication.sessionManager.saveSession(session);

            // Firebase log
            RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_SESSION_MODIFIED, null);

            finish();
        }
    }

    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type){
            case SESSION_UPDATED:
                //on recharge la session
                if(message.content instanceof Session) {
                    session = (Session) message.content;
                    configureView();
                }
                break;
        }
        super.onMessageEvent(message);
    }
}
