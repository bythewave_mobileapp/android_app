package com.bythewave.rideplus.activities.fragments;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bythewave.rideplus.BuildConfig;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.activities.BaseAppCompatActivity;
import com.bythewave.rideplus.activities.EditDeviceActivity;
import com.bythewave.rideplus.activities.NewSessionActivity;
import com.bythewave.rideplus.activities.fragments.dialogs.AddDeviceDialogFragment;
import com.bythewave.rideplus.activities.fragments.dialogs.HelpDialogFragment;
import com.bythewave.rideplus.activities.fragments.dialogs.SyncDeviceDialogFragment;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Device;
import com.polidea.rxandroidble.RxBleConnection;
import com.socks.library.KLog;

import org.joda.time.DateTime;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import me.tankery.permission.PermissionRequestActivity;

/**
 * fr.reseauscet.scet_app_android.activities.fragments.DeviceFragment
 * <p/>
 * Fragment affichant un appareil Wavecatcher ou permettant d'en ajouter un
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 24/01/2018
 */
public class DeviceFragment extends BaseFragment {

    private static final int NO_DEVICE = 0;
    private static final int DEVICE_DISCONNECTED = 1;
    private static final int DEVICE_CONNECTED = 2;

    @BindView(R.id.device_layout)
    protected ConstraintLayout layout;

    @BindView(R.id.device_settings)
    protected ImageButton buttonSettings;

    @BindView(R.id.device_image)
    protected ImageView deviceImage;

    @BindView(R.id.device_name)
    protected TextView deviceName;

    @BindView(R.id.device_empty)
    protected TextView deviceEmpty;

    @BindView(R.id.device_sync)
    protected TextView deviceSync;

    @BindView(R.id.device_button)
    protected Button deviceButton;

    @BindView(R.id.device_disconnected)
    protected TextView disconnected;

    @BindView(R.id.device_sport)
    protected LinearLayout sportLayout;

    @BindView(R.id.device_sport_icon)
    protected ImageView sportIcon;

    @BindView(R.id.device_sport_label)
    protected TextView sportLabel;

    @BindView(R.id.device_toolbar)
    protected Toolbar actionBar;

    private Device device;

    private AddDeviceDialogFragment addDeviceDialogFragment;
    private SyncDeviceDialogFragment syncDeviceDialogFragment;

    /**
     * Renvoi une instance de ce fragment
     *
     * @return l'instance du fragment
     */
    public static Fragment newInstance() {
        return new DeviceFragment();
    }

    /**
     * Constructeur vide
     */
    public DeviceFragment() {
        screen = Constants.SCREEN_DEVICE;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //on désactive le menu de l'action Bar sur ce fragment
//        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_device, container, false);
        ButterKnife.bind(this, view);

        //configuration de l'action bar
        if (getActivity() != null) {
            ((BaseAppCompatActivity) getActivity()).setSupportActionBar(actionBar);
            ActionBar supportActionBar = ((BaseAppCompatActivity) getActivity()).getSupportActionBar();

            if (supportActionBar != null) {
                supportActionBar.setDisplayShowCustomEnabled(true);
                supportActionBar.setDisplayShowTitleEnabled(false);
            }
        }

        configureView();

        if (this.getActivity() != null)
            PermissionRequestActivity.start(this.getActivity(), new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.ACCESS_FINE_LOCATION},
                    getString(R.string.error_message_ble_location_permission_missing), getString(R.string.error_message_ble_location_permission_missing))
                    ;

        return view;
    }

    /**
     * Configure la vue selon le page d'affichage (appareil non enregistré, appareil déconnecté, appareil connecté)
     */
    private void configureView() {

        retrieveDevice();

        //on configure le page d'affichage selon le sport de présentation de la vue :
        int mode = device == null ? NO_DEVICE : RidePlusApplication.bluetoothDeviceManager.getConnectionState(device) == RxBleConnection.RxBleConnectionState.CONNECTED ? DEVICE_CONNECTED : DEVICE_DISCONNECTED;

        if (getActivity() != null)
            deviceImage.setImageDrawable(getActivity().getDrawable(mode == 0 ? R.drawable.image_wavecatcher_empty : R.drawable.image_wavecatcher_normal));

        deviceImage.setAlpha(mode == DEVICE_DISCONNECTED ? 0.5f : 1f);
        deviceName.setVisibility(mode == NO_DEVICE ? View.GONE : View.VISIBLE);

        //on modifie la contrainte d'alignement vertical de l'image
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(layout);
        constraintSet.setVerticalBias(R.id.device_image, mode == NO_DEVICE ? 0.3f : 0.1f);
        constraintSet.applyTo(layout);

        deviceEmpty.setVisibility(mode == NO_DEVICE ? View.VISIBLE : View.GONE);
        disconnected.setVisibility(mode == DEVICE_DISCONNECTED ? View.VISIBLE : View.GONE);
        deviceSync.setVisibility(mode == NO_DEVICE ? View.INVISIBLE : View.VISIBLE);
        deviceButton.setEnabled(mode != DEVICE_DISCONNECTED);
        deviceButton.setText(mode == NO_DEVICE ? R.string.device_add : R.string.device_sync);
        buttonSettings.setVisibility(mode == NO_DEVICE ? View.GONE : View.VISIBLE);

        sportLayout.setVisibility(mode == NO_DEVICE ? View.GONE : View.VISIBLE);

        if (device != null && getContext() != null) {

            //on affiche le sport de fonctionnement du boitier
            sportLabel.setText(
                    AndroidUtils.getSportString("_sport", device.sport));

            Drawable icon = AndroidUtils.getSportDrawable("_sport", device.sport);
            if (icon != null)
                icon.setTint(ContextCompat.getColor(getContext(), R.color.maya_blue));
            sportIcon.setImageDrawable(icon);

            if (TextUtils.isEmpty(device.customName)) {
                deviceName.setText(getString(R.string.default_device_name));
            } else {
                deviceName.setText(device.customName);
            }

            if (device.lastSyncAt != null)
                deviceSync.setText(String.format(getString(R.string.last_sync), AndroidUtils.getFormatedDateString(device.lastSyncAt, false)));
        }
    }

    /**
     * Récupère l'appareil à afficher
     */
    private void retrieveDevice() {
        //à modifier ultérieurement pour gérer plusieurs appareils
        if (RidePlusApplication.currentUserManager.hasDevices()) {
            device = RidePlusApplication.currentUserManager.getCurrentUserDevices().get(0);
        } else {
            device = null;
            //on annule les connexions en cours sauf si en cours de synchro ou d'appairage
            if ((syncDeviceDialogFragment == null || syncDeviceDialogFragment.isHidden()) && (addDeviceDialogFragment == null || addDeviceDialogFragment.isHidden()))
                RidePlusApplication.bluetoothDeviceManager.cancelSubscriptions();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();

        retrieveDevice();

        //si l'utilisateur a appareil enregistré, on cherche à s'y connecter
        startScan();

    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onMessageEvent(BusMessage message) {
        switch (message.type) {
            case REFRESH_CONTENT:
            case DEVICE_ADDED:
            case DEVICE_UPDATED:
            case DEVICE_DELETED:
            case USER_CURRENT_UPDATED:
                configureView();
                break;
            case DEVICE_CONNECTION_CHANGED:
                //on récupère l'état de la connexion
                if (message.content instanceof RxBleConnection.RxBleConnectionState) {
                    KLog.d(message.content.toString());
                    // si l'appareil est déconnecté, on relance le scann
                    if (message.content.equals(RxBleConnection.RxBleConnectionState.DISCONNECTED)) {
                        startScan();
                    }
                    configureView();
                }

                break;
            case DEVICE_NOT_RESPONDING:
                if (getActivity() != null)
                    Toasty.error(getActivity(), getString(R.string.error_message_device_not_responding), Toast.LENGTH_LONG, true).show();
                startScan();
                break;
            case ERROR_BLE_CORE:
            case ERROR_BLE_COMMAND_TIMEOUT:
            case ERROR_DEVICE_DISCONNECTED:
                startScan();
                configureView();
                break;
            case ERROR_BLE_STOP:
                configureView();
                break;
            case RESTART_SCAN:
                startScan();
                break;
            case DEVICE_STATE_UPDATED:
            case SESSION_ADDED:
                //on enregistre la date de dernière synchronisation du device
                if (device != null) {
                    device.lastSyncAt = DateTime.now().toDate();
                    RidePlusApplication.currentUserManager.setDevice(device);
                }
                break;
        }
        super.onMessageEvent(message);
    }

    /**
     * Démarre la connexion à l'appareil si ajouté
     */
    @SuppressWarnings("ConstantConditions")
    public void startScan() {
        if (!BuildConfig.BUILD_TYPE.equals("simulator")) {
            if (BluetoothAdapter.getDefaultAdapter() != null) {
                if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                    if (!RidePlusApplication.bluetoothDeviceManager.isScanning() && RidePlusApplication.bluetoothDeviceManager.getConnectionState(device) == RxBleConnection.RxBleConnectionState.DISCONNECTED) {
                        RidePlusApplication.bluetoothDeviceManager.startScan(device);
                    }
                } else {
                    if (getActivity() != null)
                        Toasty.error(getActivity(), getString(R.string.error_message_ble_disabled), Toast.LENGTH_LONG, true).show();
                }
            } else {
                if (getActivity() != null)
                    Toasty.error(getActivity(), getString(R.string.error_message_ble_not_avalaible), Toast.LENGTH_LONG, true).show();
            }
            configureView();
        }
    }

    @OnClick(R.id.device_button)
    public void synchDevice() {
        //selon le page de la vue, on effectue une action différente : synchroniser ou ajouter un appareil
        if (getActivity() != null) {
            if (device != null) {
                syncDeviceDialogFragment = SyncDeviceDialogFragment.newInstance();
                syncDeviceDialogFragment.show(getActivity().getSupportFragmentManager(), "");

                RidePlusApplication.bluetoothDeviceManager.getSessions();
            } else {
                addDeviceDialogFragment = AddDeviceDialogFragment.newInstance();
                addDeviceDialogFragment.show(getActivity().getSupportFragmentManager(), "");
            }
        }
    }

    @OnClick(R.id.device_new_session)
    public void startNewSession() {
        if (getActivity() != null) {
            getActivity().startActivityForResult(new Intent(getActivity(), NewSessionActivity.class), Constants.REQUEST_SESSION_ADDED);
            getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        }
    }

    @OnClick(R.id.device_disconnected)
    public void displayHelp() {
        //selon le page de la vue, on effectue une action différente : synchroniser ou ajouter un appareil
        if (getActivity() != null) {
            HelpDialogFragment.newInstance().show(getActivity().getSupportFragmentManager(), "");
        }
    }

    @OnClick(R.id.device_settings)
    public void editDevice() {
        if (getActivity() != null) {
            getActivity().startActivityForResult(new Intent(getActivity(), EditDeviceActivity.class), Constants.REQUEST_CODE_REFRESH_NEEDED);
            getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        }
    }


}
