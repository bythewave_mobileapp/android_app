package com.bythewave.rideplus.tasks;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.Nullable;

import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.Moment;
import com.bythewave.rideplus.models.Session;
import com.bythewave.rideplus.models.Wave;
import com.bythewave.rideplus.models.deserializers.GeoPointDeserializer;
import com.google.firebase.firestore.GeoPoint;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.socks.library.KLog;

import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * com.bythewave.rideplus.tasks.ParseLocationsTask
 * <p/>
 * <p>
 * Tâche chargée d'analyser les positions GPS enregistrées par le téléphone en mode tracking GPS.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 07/02/2019
 */
public class ParseLocationsTask extends AsyncTask<Void, Void, Boolean> {

    // flag indiquant si une nvelle session a bien été créé
    public boolean sessionAdded;

    // paramètres utilisés par l'algorithme
    private Session newSession;
    private Wave newWave;

    private WeakReference<Context> contextWeakReference;
    private OutputStreamWriter logStreamWriter;
    private List<Moment> locationSamples;
    private FileOutputStream logStream;
    private Gson gson;

    /**
     * Constructeur paramétré
     */
    public ParseLocationsTask(WeakReference<Context> contextWeakReference) {
        this.contextWeakReference = contextWeakReference;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        boolean success = true;
        try {
            Context context = contextWeakReference.get();
            newSession = Session.createWithPhone();
            newWave = null;
            File locationFile = new File(context.getFilesDir(), Constants.NEW_SESSION_FILE);
            sessionAdded = false;
            locationSamples = new ArrayList<>(RidePlusApplication.configManager.sampleSize + 1);

            String logFileName = String.format(Locale.FRANCE, Constants.LOG_FILE_NAME, RidePlusApplication.currentUserManager.getCurrentUserId(), DateTime.now().toDate().getTime());
            File logDirectory = new File(context.getFilesDir(), Constants.DIR_GEO_LOG);
            boolean directoryCreated = logDirectory.mkdirs();
            if (directoryCreated)
                KLog.d("GPS", "log directory created");

            File logFile = new File(logDirectory, logFileName);
            logStream = new FileOutputStream(logFile, true);
            logStreamWriter = new OutputStreamWriter(logStream);

            // parseur GSON spécifique pour éviter des soucis avec les serialisations de dates
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(GeoPoint.class, new GeoPointDeserializer());
            gson = gsonBuilder.create();

            writeLog("ANDROID DEVICE");

            writeLog("Brand: " + Build.BRAND);
            writeLog("Model: " + Build.MODEL);
            writeLog("Android Version Code: " + Build.VERSION.RELEASE);
            if (context.getPackageManager() != null && context.getPackageName() != null) {
                PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);
                if (info != null) {
                    writeLog("App version: " + info.versionName);
                    if (!ArrayUtils.isEmpty(info.permissions)) {
                        writeLog("App permissions: ");
                        for (PermissionInfo permission : info.permissions)
                            writeLog(permission.name);
                    }
                    if (!ArrayUtils.isEmpty(info.requestedPermissions)) {
                        writeLog("App requested permissions: ");
                        for (String requestedPermission : info.requestedPermissions)
                            writeLog(requestedPermission);
                    }

                }
            }


            writeLog("");

            writeLog("ALGORITHM PARAMETERS");
            writeLog(String.format(Locale.FRANCE, "Sample size : %d", RidePlusApplication.configManager.sampleSize));
            writeLog(String.format(Locale.FRANCE, "Horizontal speed threshold down : %f", RidePlusApplication.configManager.horizontalSpeedDown));
            writeLog(String.format(Locale.FRANCE, "Horizontal speed threshold up : %f", RidePlusApplication.configManager.horizontalSpeedUp));
            writeLog(String.format(Locale.FRANCE, "Vertical speed threshold down : %f", RidePlusApplication.configManager.verticalSpeedDown));
            writeLog(String.format(Locale.FRANCE, "Vertical speed threshold up : %f", RidePlusApplication.configManager.verticalSpeedUp));
            writeLog(String.format(Locale.FRANCE, "Date : %s", DateTime.now().toString()));

            writeLog("");

            writeLog("START PARSING");

            //lecture du fichier ligne par ligne
            if (locationFile.exists()) {

                InputStream inputStream = context.openFileInput(locationFile.getName());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                do {

                    //on lit la ligne suivante
                    line = reader.readLine();

                    if (line != null) {

                        writeLog("Line from file : " + line);
                        //on désérialise le moment
                        Moment moment = gson.fromJson(line, Moment.class);
                        if (moment != null && !Double.isNaN(moment.position.getLatitude()) && moment.position.getLatitude() >= -90.0D && moment.position.getLatitude() <= 90.0D &&
                                !Double.isNaN(moment.position.getLongitude()) && moment.position.getLongitude() >= -180.0D && moment.position.getLongitude() <= 180.0D) {
                            //si on a un échantillon de moment suffisant, on va calculer la vitesse médiane sur cet échantillon
                            if (locationSamples.size() == RidePlusApplication.configManager.sampleSize + 1) {
                                writeLog("sample full");
                                evaluateMoment();
                            }

                            writeLog("moment valid - adding moment");
                            // on ajoute la position courante à l'horizon des moments
                            locationSamples.add(moment);
                        } else {
                            writeLog("location null - skipping moment");
                        }
                    }

                }
                while (line != null);

                //on ferme les streams
                reader.close();
                inputStream.close();


                //on parse les derniers moments de l'échantillon
                while (!locationSamples.isEmpty()) {
                    evaluateMoment();
                }


                // si on a un ride en cours, on l'enregistre
                // on enregistre la nvelle session
                if (newWave != null && newWave.moments.size() > 1) {
                    newWave.computeStatistics();
                }
                if (newSession != null && !newSession.waves.isEmpty()) {
                    //on ajoute le dernier ride si il a des points
                    if (newWave != null && newWave.moments.size() > 1)
                        newSession.waves.add(newWave);

                    newSession.computeStatistics();

                    //on change le sport par défaut
                    RidePlusApplication.configManager.setCurrentSport(newSession.sport);

                    //on sauvegarde la nvelle session
                    RidePlusApplication.sessionManager.saveSessions(Collections.singletonList(newSession));

                    sessionAdded = true;

                }

            } else {
                writeLog("no positions recorded.");
            }

            writeLog("END PARSING");

            //on updloade le fichier de log
            logStreamWriter.close();
            logStream.close();

        } catch (Exception exception) {
            KLog.e(exception);
            success = false;
        } finally {
            //on supprime le fichier
            contextWeakReference.get().deleteFile(Constants.NEW_SESSION_FILE);
        }
        return success;
    }


    @SuppressLint("DefaultLocale")
    @Nullable
    private void evaluateMoment() throws IOException {
        writeLog("Evaluating moments");

        if (locationSamples == null || locationSamples.isEmpty())
            return;

        float distance;
        float altitudeDelta;
        float[] horizontalSpeeds = new float[RidePlusApplication.configManager.sampleSize];
        float[] verticalSpeeds = new float[RidePlusApplication.configManager.sampleSize];

        float medianVerticalSpeed;
        float medianHorizontalSpeed;

        Moment oldMoment = null;
        int sampleIndex = -1;

        for (Moment moment : locationSamples) {

            writeLog(String.format("moment : %s", gson.toJson(moment)));

            if (oldMoment != null) {
                writeLog(String.format("old moment : %s", gson.toJson(oldMoment)));

                //on calcule la distance parcourue
                float[] result = new float[1];
                Location.distanceBetween(oldMoment.position.getLatitude(), oldMoment.position.getLongitude(), moment.position.getLatitude(), moment.position.getLongitude(), result);
                distance = result[0];
                long elapsedTime = (moment.loggedAt - oldMoment.loggedAt) / 1000L;
                horizontalSpeeds[sampleIndex] = distance / Long.valueOf(elapsedTime).floatValue();
                altitudeDelta = Double.valueOf(moment.altitude - oldMoment.altitude).floatValue();
                verticalSpeeds[sampleIndex] = altitudeDelta / Long.valueOf(elapsedTime).floatValue();
                writeLog(String.format("index %d distance %f time elapsed %d ms speed %f altitude delta %f v speed %f", sampleIndex, distance, elapsedTime, horizontalSpeeds[sampleIndex], altitudeDelta, verticalSpeeds[sampleIndex]));
            }

            sampleIndex += 1;
            oldMoment = moment;
        }

        //on calcule les vitesses médiannes
        Arrays.sort(horizontalSpeeds);
        if (horizontalSpeeds.length % 2 == 0)
            medianHorizontalSpeed = (horizontalSpeeds[horizontalSpeeds.length / 2] + horizontalSpeeds[horizontalSpeeds.length / 2 - 1]) / 2f;
        else
            medianHorizontalSpeed = horizontalSpeeds[horizontalSpeeds.length / 2];
        writeLog(String.format("h speeds %s median %f", Arrays.toString(horizontalSpeeds), medianHorizontalSpeed));

        Arrays.sort(verticalSpeeds);
        if (verticalSpeeds.length % 2 == 0)
            medianVerticalSpeed = (verticalSpeeds[verticalSpeeds.length / 2] + verticalSpeeds[verticalSpeeds.length / 2 - 1]) / 2f;
        else
            medianVerticalSpeed = verticalSpeeds[verticalSpeeds.length / 2];
        writeLog(String.format("v speeds %s median %f", Arrays.toString(verticalSpeeds), medianVerticalSpeed));

        //on sort le premier élément de l'échantillons
        Moment momentToAdd = locationSamples.get(0);
        locationSamples.remove(0);

        // on verifie si :
        // • la vitesse horizontale médianne est suffisante
        // • la vitesse de descente médianne est suffisante
        if (medianHorizontalSpeed > RidePlusApplication.configManager.horizontalSpeedDown && medianVerticalSpeed < RidePlusApplication.configManager.verticalSpeedDown) {
            writeLog("going down");
            //si on a pas de ride créé, on en initialise un
            if (newWave == null) newWave = Wave.createWithPhone();

            //si oui, on ajoute la position précédente si au ride en cours est vide de positions
            newWave.moments.add(momentToAdd);
            newWave.position = momentToAdd.position;

            //on ajoute également le premier point d'une session comme point de référence de cette dernière.
            if (newSession.waves.isEmpty()) {
                newSession.position = momentToAdd.position;
            }

        }
        // sinon les conditions pour ajouter une position ne sont pas requises, on termine le ride.
        else if (medianHorizontalSpeed > RidePlusApplication.configManager.horizontalSpeedUp && medianVerticalSpeed > RidePlusApplication.configManager.verticalSpeedUp) {
            writeLog("UP UP UP");
            writeLog("going up");
            // si le ride courant a plus d'un 1 position, on l'enregistre et on calcule les statistiques du ride
            if (newWave != null && newWave.moments.size() > 1) {
                newWave.computeStatistics();
                newSession.waves.add(newWave);
                writeLog(String.format("registring ride %s", gson.toJson(newWave)));
                writeLog("\n\n");
            }

            writeLog("reseting ride");
            //on créé un nouveau ride
            newWave = null;

        } else {
            writeLog("stalled");
        }

    }

    /**
     * Ecrit une ligne dans le fichier de log
     *
     * @param line la ligne a écrire
     * @throws IOException exception levée en cas de problème avec le flux
     */
    private void writeLog(String line) throws IOException {
        KLog.d("GPS", line);
        logStreamWriter.write(line);
        logStreamWriter.write("\n");
    }
}
