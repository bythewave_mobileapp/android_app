package com.bythewave.rideplus.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.models.enums.Sport;

/**
 * com.bythewave.rideplus.ui.SportButton
 * <p/>
 * <p>
 * Boutton permettant de sélectionner le sport courant.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 14/11/2018
 */
public class SportButton extends ConstraintLayout {

    protected ImageView sportIcon;
    protected TextView sportLabel;
    protected ImageView sportTriangle;

    /**
     * Constructeur simple
     */
    public SportButton(Context context) {
        super(context);
        init();
    }

    public SportButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SportButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Initialise la vue
     */
    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_sport, this, true);

        this.sportLabel = this.findViewById(R.id.sport_title);
        this.sportIcon = this.findViewById(R.id.sport_icon);
        this.sportTriangle = this.findViewById(R.id.sport_triangle);

        setEnabled(false);
        invalidate();
    }


    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        Sport currentSport = RidePlusApplication.configManager.getCurrentSport();
        if (sportIcon != null) {
            sportIcon.setImageDrawable(AndroidUtils.getSportDrawable("_sport"));
            sportIcon.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), enabled ? R.color.maya_blue : R.color.manatee)));
        }
        if (sportLabel != null) {
            sportLabel.setText(AndroidUtils.getSportString("_sport"));
            sportLabel.setTextColor(ColorStateList.valueOf(ContextCompat.getColor(getContext(), enabled ? R.color.maya_blue : R.color.manatee)));
        }
        if (sportTriangle != null)
            sportTriangle.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), enabled ? R.color.corn : R.color.manatee)));
        invalidate();
    }
}
