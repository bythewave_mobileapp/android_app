package com.bythewave.rideplus.ui;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import static com.bythewave.rideplus.common.AndroidUtils.getViewWeight;
import static com.bythewave.rideplus.common.AndroidUtils.setViewWeight;

/**
 * com.by_the_wave.ride_plus_android.ui
 * <p/>
 * Animation pour le swipe to delete
 * <p/>
 * @link https://github.com/alexandrius/accordion-swipe-layout/
 *
 * @author L'Apptelier SARL
 * @date 18/04/2018
 */
public class WeightAnimation extends Animation {

    private float startWeight = -1;
    private float deltaWeight = -1;
    private final float endWeight;
    private View view;

    WeightAnimation(float endWeight, View view) {
        this.endWeight = endWeight;
        this.view = view;
        setDuration(200);
    }

    public View getView() {
        return view;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        if (startWeight < 0) {
            startWeight = getViewWeight(view);
            deltaWeight = endWeight - startWeight;
        }

        setViewWeight(view, (startWeight + (deltaWeight * interpolatedTime)));
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}