package com.bythewave.rideplus.common;

import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.R;

import java.util.UUID;

/**
 * com.by_the_wave.ride_plus_android.common.Constants
 * <p/>
 * Constantes globales à l'application.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 19/01/2018
 */
@SuppressWarnings("HardCodedStringLiteral")
public class Constants {

    public static final int SPLASH_TIME_OUT = 3000; // temps d'affichage du splash screen


    // Id de préférences
    public static final String PREFERENCES_ID = "com.bythewave.rideplusandroid";
    public static final String PREF_DISPLAY_TUTORIAL = "displayTutorial";
    public static final String PREF_1ST_LAUNCH_DATE = "firstLaunchDate";
    public static final String PREF_LOCAL_CONFIG_VERSION = "localConfigVersion";
    public static final String PREF_DEFAULT_SPORT = "defaultSport";
    public static final String PREF_SHOULD_MIGRATE_OLD_SESSIONS = "shouldMigrateOldSessions";

    public static final String DEFAULT_TYPEFACE = "fonts/montserrat_regular.otf";
    public static final String BOLD_TYPEFACE = "fonts/montserrat_bold.otf";

    // nombre d'élément par défaut demandé aux API paginées
    public static final int DEFAULT_API_ELEMENT_COUNT = 10;

    // délai d'attente avant de réafficher un message d'erreur similaire
    public static final int SAME_ERROR_TIMEOUT = 60;

    // codes de retour utilisés pour communiquer entre activités
    public static final int REQUEST_CODE_RELOAD_NEEDED = 900;
    public static final int REQUEST_CODE_AVATAR_PICKER = 901;
    public static final int REQUEST_CODE_REFRESH_NEEDED = 903;
    public static final int REQUEST_ENABLE_BLE = 905;
    public static final int REQUEST_CODE_PERMISSIONS = 906;
    public static final int REQUEST_CODE_UPDATED_SESSION = 907;
    public static final int REQUEST_CHECK_SETTINGS = 908;
    public static final int REQUEST_SESSION_ADDED = 909;

    // délai d'attente par défaut. Cette constante est utilisée par exemple pour envoyer un message
    // à une vue après sa création, avec un délai de 200ms pour lui laisser le temps de se construire
    public static final int DEFAULT_UI_DELAY = 200;

    // Constantes utilisées pour Firebase Analytics
    public static final String EVENT_SCAN_DEVICE = "scan_device";
    public static final String EVENT_DEVICE_ADDED = "device_added";
    public static final String EVENT_DEVICE_SYNC = "device_synchronized";
    public static final String EVENT_DEVICE_MODIFIED = "device_modified";
    public static final String EVENT_DEVICE_DELETED = "device_deleted";
    public static final String EVENT_SESSION_SELECTED = "session_selected";
    public static final String EVENT_SESSION_MODIFIED = "session_modified";
    public static final String EVENT_SESSION_DELETED = "session_deleted";
    public static final String EVENT_SESSION_STARRED = "session_starred";
    public static final String EVENT_WAVE_DELETED = "session_wave_deleted";
    public static final String EVENT_PROFILE_MODIFIED = "profile_modified";
    public static final String EVENT_PROFILE_EMAIL_CHANGED = "profile_email_changed";
    public static final String EVENT_PROFILE_PASSWORD_CHANGED = "profile_password_changed";
    public static final String EVENT_PROFILE_DELETED = "profile_deleted";

    public static final String SCREEN_LOGIN = "Connexion";
    public static final String SCREEN_FORGOTTEN_PASSWORD = "Connexion par Mail - Mot de passe oublié";
    public static final String SCREEN_LOGIN_MAIL = "Connexion par Mail - Saisie email & mot de passe";
    public static final String SCREEN_DEVICE = "Appareils - Liste";
    public static final String SCREEN_DEVICE_EDIT = "Appareils - Edition";
    public static final String SCREEN_ADD_DEVICE = "Appareils - Ajouter un appareil";
    public static final String SCREEN_PASSCODE = "Appareils - Saisi du code d'accès";
    public static final String SCREEN_SYNC_DEVICE = "Appareils - Synchroniser un appareil";
    public static final String SCREEN_HELP = "Appareils - Aide";
    public static final String SCREEN_SESSIONS = "Sessions - Liste";
    public static final String SCREEN_NEW_SESSION = "Sessions - Nouvelle session";
    public static final String SCREEN_SESSION = "Session - Consultation";
    public static final String SCREEN_PROFILE = "Profil";
    public static final String SCREEN_PROFILE_EDIT = "Profil - Edition";
    public static final String SCREEN_CHANGE_MAIL = "Profil - Modification de l'email";
    public static final String SCREEN_CHANGE_PASSWORD = "Profil - Modification du mot de passe";
    public static final String SCREEN_SESSION_WAVES = "Session - Liste des vagues";
    public static final String SCREEN_SPORT = "Choix du sport";

    // Liste des intents
    public static final String PREFIX_INTENT = "com.bythewave.rideplus_android.";
    public static final String INTENT_TAB_ID = PREFIX_INTENT + "intent_tab_id";
    public static final String INTENT_LOADING_TEXT = PREFIX_INTENT + "intent_loading_text";
    public static final String INTENT_SESSION = PREFIX_INTENT + "intent_session";
    public static final String INTENT_SELECTED_WAVE_INDEX = PREFIX_INTENT + "intent_selected_wave";
    public static final String INTENT_PROCESS_LOCATION = PREFIX_INTENT + "intent_process_location";
    // Collections
    public static final String COLLECTIONS_USERS = "users";
    public static final String COLLECTIONS_DEVICES = "devices";
    public static final String COLLECTIONS_USER_DEVICE_JOINS = "user_device_joins";
    public static final String COLLECTIONS_SESSIONS = "sessions";
    public static final String COLLECTIONS_CONFIGS = "configs";

    // Buckets Cloud Storage
    public static final String BUCKET_AVATARS = "profile_pictures/";
    public static final String BUCKET_COVERS = "cover_pictures/";
    public static final String BUCKET_GEO_LOG = "Geologs/%s/%s";
    public static final String DIR_GEO_LOG = "Geologs/";
    public static final String REF_PREFIX = "gs://" + RidePlusApplication.applicationContext.getString(R.string.google_storage_bucket) + "/";
    public static final String DEFAULT_COVER_FILE = REF_PREFIX + BUCKET_COVERS + "cover_%s.jpg";
    public static final String NEW_SESSION_FILE = "tempSession.txt";
    public static final String LOG_FILE_NAME = "%s_%d_android.txt";

    public static final int DEFAULT_AVATAR_IMAGE_SIZE = 512;
    public static final int DEFAULT_COVER_IMAGE_WIDTH = 1128;
    public static final int DEFAULT_COVER_IMAGE_HEIGHT = 480;

    // nom du device sur la session de test
    public static final String TEST_SESSION_DEVICE_NAME = "DefaultSession";

    // Taille minimale d'un mot de passe
    public static final int MIN_PASSWORD_LENGHT = 5;

    // Moyen de connexion
    public static final String AUTH_EMAIL = "password";

    // Service UUID des appareils By The Wave
    public static final String DEFAULT_DEVICE_NAME = "BTW_WC";
    //id de device des sessions faites avec le téléphone
    public static final String PHONE_DEVICE_NAME = "phone";
    public static final String UUID_AUTH = "3a7a1c84-d8ee-483b-85b2-0d349d66248f";
    public static final UUID UUID_CHARACTERISTIC_DATA = UUID.fromString("00035b03-58e6-07dd-021a-08123a000301");
    public static final UUID UUID_CHARACTERISTIC_MODEL = UUID.fromString("00002A24-0000-1000-8000-00805F9B34FB");
    public static final UUID UUID_CHARACTERISTIC_SERIAL = UUID.fromString("00002A25-0000-1000-8000-00805F9B34FB");
    public static final UUID UUID_CHARACTERISTIC_FIRMWARE_REV = UUID.fromString("00002A26-0000-1000-8000-00805F9B34FB");
    public static final UUID UUID_CHARACTERISTIC_HARDWARE_REV = UUID.fromString("00002A27-0000-1000-8000-00805F9B34FB");
    public static final UUID UUID_CHARACTERISTIC_MANUFACTURER = UUID.fromString("00002A29-0000-1000-8000-00805F9B34FB");

    // Temps d'expiration d'une recherche d'appareil BLE, en millisecondes
    public static final int TIMEOUT_BLE_COMMAND = 3000;

    // Temps d'affichage d'un toast
    public static final int TOAST_LENGTH_LONG = 3;

    // Commandes échangées avec le boitier
    public static final String COMMAND_GET_STATE = "state?\n";
    public static final String COMMAND_SET_STATE = "state %s\n";
    public static final String COMMAND_GET_DATA = "data?\n";
    public static final String COMMAND_CLEAR_DATA = "clear?\n";

    // Expression régulière pour détecter le type de réponse du boitier
    public static final String REGEX_STATE = "<STATE>(.*?)<\\/STATE>";
    public static final String REGEX_DATA = "<DATA>(.*?)<\\/DATA>";
    public static final String FIELD_USER_ID = "userId";
    public static final String FIELD_CREATED_AT = "startedAt";
    public static final String FIELD_TO_DELETE = "toDelete";
    public static final String FIELD_SPORT = "sport";

    // Nombre maximal de niveau de notation d'un ride
    public static final int MAX_LEVEL = 6;

    // Taille maximale d'une commande BLE, en nombre de charactères
    public static final int MAX_COMMAND_SIZE = 15;
    // Nombre d'essai consécutifs avant d'envoyer une erreur BLE
    public static final int MAX_RETRY = 3;
    // délai d'attente imposé sur les connexions BT
    public static final long DEFAULT_BT_DELAY = 300;


    public static final float HORIZONTAL_SPEED_DOWN_THRESHOLD = 1.0f; // vitesse horizontale minimale à atteindre pour enregistrer une position, exprimée en m/s.
    public static final float HORIZONTAL_SPEED_UP_THRESHOLD = 1.0f; // vitesse horizontale minimale à atteindre pour enregistrer une position, exprimée en m/s.
    public static final float VERTICAL_SPEED_DOWN_THRESHOLD = -0.15f; // vitesse verticale minimale à atteindre pour enregistrer une position, exprimée en m/s.
    public static final float VERTICAL_SPEED_UP_THRESHOLD = 0.25f; // vitesse verticale minimale à atteindre pour arrêter un ride, exprimée en m/s.
    public static final int SPEED_SAMPLE_COUNT = 5; // nombre de vitesses utilisée pour calculer la vitesse mediane



}


