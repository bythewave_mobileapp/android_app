package com.bythewave.rideplus.common;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.StorageReference;

import java.io.InputStream;

/**
 * com.by_the_wave.ride_plus_android.common.FirebaseGlideModule
 * <p/>
 * Module pour Glide + Firebase Cloud Storage
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 01/02/2018
 */
@GlideModule
public class FirebaseGlideModule extends AppGlideModule {

    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {
        registry.append(StorageReference.class, InputStream.class,
                new FirebaseImageLoader.Factory());
    }

    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }
}
