package com.bythewave.rideplus.common;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import android.text.Layout;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.models.enums.Sport;
import com.google.firebase.storage.FirebaseStorage;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socks.library.KLog;

import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.bythewave.rideplus.RidePlusApplication.applicationContext;


/**
 * fr.reseauscet.scet_app_android.common.AndroidUtils
 * <p/>
 * Collection de méthodes utilitaires
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 06/09/2017
 */
public class AndroidUtils {

    private static final int MINUTE = 60;
    private static final int HOUR = 3600;
    private static final int DAY = 86400;
    private static final int TWO_DAYS = 172800;
    private static final int WEEK = 604800;
    private static final int HALF_YEAR = 15724800;

    /**
     * Affiche une image distante dans une imageView
     *
     * @param imageUri    l'uri de l'image à afficher
     * @param imageView   l'imageView où afficher l'image distante
     * @param placeholder l'id du place holder à afficher en cas d'erreur
     */
    public static void displayImage(String imageUri, ImageView imageView, int placeholder) {
        RequestOptions options = new RequestOptions()
                .placeholder(placeholder)
                .error(placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL);

        KLog.d("Display image url " + imageUri);
        GlideApp.with(applicationContext)
                // si l'url correspond à une référence vers Google Cloud Storage, on exploite un sport spécifique de Glide
                .load(imageUri != null && imageUri.startsWith(Constants.REF_PREFIX) ? FirebaseStorage.getInstance().getReferenceFromUrl(imageUri) : imageUri)
                .transition(withCrossFade(500))
                .apply(options)
                .into(imageView);
    }


    /**
     * Affiche l'image dont l'url est fournie en paramètre dans la vue ronde donnée elle aussi en paramètre
     *
     * @param imageUri    l'url de l'image à afficher
     * @param view        la vue dans laquelle afficher l'image
     * @param placeholder l'id du place holder à afficher en cas d'erreur
     */
    public static void displayRoundedImage(String imageUri, RoundedImageView view, int placeholder) {
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .error(placeholder)
                .placeholder(placeholder);

        KLog.d("Display image url " + imageUri);
        GlideApp.with(applicationContext)
                // si l'url correspond à une référence vers Google Cloud Storage, on exploite un sport spécifique de Glide
                .load(imageUri != null && imageUri.startsWith(Constants.REF_PREFIX) ? FirebaseStorage.getInstance().getReferenceFromUrl(imageUri) : imageUri)
                .transition(withCrossFade(500))
                .apply(options)
                .into(view);
    }


    /**
     * Renvoi la date du jour à T00:00.0000
     *
     * @return la date du jour
     */
    public static DateTime today() {
        return DateTime.now().withMillisOfDay(0);
    }

    /**
     * Formatte la date donné en paramètre pour produire un libellé de la forme suivante :
     * • 0min < x < 1min : "à l'instant"
     * • 1min < x < 59min : "il y a x minutes"
     * • 1h < x < 23h : "il y a x heures"
     * • 24j < x < 47h : "hier, à hh:mm"
     * • 2j < x < 6j : "<jour>, à hh:mm"
     * • x > 6j : "jj mmmm, à hh:mm"
     * • x > 365j : "jj mmmm aaaa"
     *
     * @param date        la date à formatter
     * @param shortString si vrai, renvoi une version raccourcit de la date formattée
     * @return la date formattée
     */
    public static String getFormatedDateString(Date date, boolean shortString) {
        if (date != null) {
            long seconds = (new Date().getTime() - date.getTime()) / 1000;
            long secondsSinceYesterday = (long) (Math.floor(new Date().getTime() / DAY) * DAY - DAY);

            if (seconds < MINUTE) {
                if (shortString)
                    return applicationContext.getString(R.string.date_under_minute_short);
                else
                    return applicationContext.getString(R.string.date_under_minute);
            } else if (seconds < HOUR) {
                if (shortString) {
                    return String.format(applicationContext.getString(R.string.date_under_hour_short), seconds / MINUTE);
                } else {
                    return String.format(applicationContext.getString(R.string.date_under_hour), seconds / MINUTE, seconds / MINUTE > 1 ? applicationContext.getString(R.string.plural) : "");
                }
            }
            if (seconds < DAY) {
                if (shortString) {
                    return String.format(applicationContext.getString(R.string.date_under_day_short), seconds / HOUR);
                } else {
                    return String.format(applicationContext.getString(R.string.date_under_day), seconds / HOUR, seconds / HOUR > 1 ? applicationContext.getString(R.string.plural) : "");
                }
            }
            if (seconds < TWO_DAYS - secondsSinceYesterday) {
                if (shortString) {
                    return applicationContext.getString(R.string.date_under_two_days_short);
                } else {
                    SimpleDateFormat simpleFormat = new SimpleDateFormat(applicationContext.getString(R.string.hours_pattern), Locale.FRENCH);
                    simpleFormat.format(date);
                    return String.format(applicationContext.getString(R.string.date_under_two_days), simpleFormat);
                }
            }
            if (seconds < WEEK) {
                SimpleDateFormat simpleFormat = new SimpleDateFormat(applicationContext.getString(shortString ? R.string.date_under_week_short : R.string.date_under_week), Locale.FRENCH);
                return simpleFormat.format(date);
            }
            if (seconds < HALF_YEAR) {
                SimpleDateFormat simpleFormat = new SimpleDateFormat(applicationContext.getString(shortString ? R.string.date_under_half_year_short : R.string.date_under_half_year), Locale.FRENCH);
                return simpleFormat.format(date);
            } else {
                SimpleDateFormat simpleFormat = new SimpleDateFormat(applicationContext.getString(R.string.date_complete), Locale.FRENCH);
                return simpleFormat.format(date);
            }
        }
        return null;
    }

    /**
     * Lis un fichier de ressource en paramètre
     *
     * @param assetName le nom du fichier de ressource à lire
     * @return le contenu du fichier de ressources
     */
    public static String loadFileFromAsset(String assetName) {
        String fileContent;
        try {
            InputStream is = RidePlusApplication.applicationContext.getAssets().open(assetName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            fileContent = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return fileContent;
    }

    /**
     * @return la largeur de l'écran
     */

    public static int getScreenWidth() {
        Display display = ((WindowManager) applicationContext
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;

    }

    /**
     * @return la hauteur de l'écran
     */
    public static int getScreenHeight() {
        Display display = ((WindowManager) applicationContext
                .getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    /**
     * Renvoi la taille demandé en dp
     *
     * @param size la taille à convertir
     * @return la taille en dp
     */
    public static int dp(float size) {
        final float scale = applicationContext.getResources().getDisplayMetrics().density;
        return (int) (size * scale + 0.5f);
    }

    /**
     * Indique si la zone de texte en paramètre est tronquée
     *
     * @param textView la zone de texte a évaluée
     * @return vrai si la zone est tronquée, faux sinon
     */
    public static boolean isEllipsized(TextView textView) {
        Layout descriptionLayout = textView.getLayout();
        if (descriptionLayout != null) {
            int lines = descriptionLayout.getLineCount();
            if (lines > 0) {
                int ellipsisCount = descriptionLayout.getEllipsisCount(lines - 1);
                return ellipsisCount > 0;
            }
        }
        return false;
    }

    /**
     * Indique si l'appareil peut se connecter à Internet
     *
     * @return vrai si l'appareil peut se connecter à Internet, faux sinon
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) RidePlusApplication.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null;
    }

    public static float getViewWeight(View view) {
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
        return lp.weight;
    }

    public static void setViewWeight(View view, float weight) {
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
        lp.weight = weight;
        view.setLayoutParams(lp);
    }

    public static void setViewWidth(View view, int width) {
        view.getLayoutParams().width = width;
        view.requestLayout();
    }

    public static void setViewHeight(View view, int height) {
        view.getLayoutParams().height = height;
        view.requestLayout();
    }

    public static Drawable setTint(Drawable drawable, int color) {
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        return drawable.mutate();
    }

    /**
     * Lit le contenu d'un fichier en paramètre et le transpose en chaine de caractères.
     *
     * @param file le fichier à lire
     * @return le contenu du fichier
     */
    public static String readFile(File file) {
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
            KLog.e(e);
        }
        return text.toString();
    }

    /**
     * Renvoi la chaine de caractère correspondant à une chaine de caractère variable selon le sport courant.
     *
     * @param stringSuffix l'identifiant invariant de la chaine de caractère variable à récupérer.
     * @return la valeur de la chaine
     */
    public static String getSportString(String stringSuffix) {
        return getSportString(stringSuffix, RidePlusApplication.configManager.getCurrentSport());
    }

    /**
     * Renvoi la chaine de caractère correspondant à une chaine de caractère variable selon le sport courant.
     *
     * @param stringSuffix l'identifiant invariant de la chaine de caractère variable à récupérer.
     * @param sport        le sport à utiliser
     * @return la valeur de la chaine
     */
    public static String getSportString(String stringSuffix, Sport sport) {
        return applicationContext.getString(
                applicationContext.getResources().getIdentifier(
                        sport.value() + stringSuffix,
                        "string",
                        applicationContext.getPackageName()));
    }

    /**
     * Renvoi l'image correspondant à une image variable selon le sport courant.
     *
     * @param stringSuffix l'identifiant invariant de l'image variable à récupérer.
     * @return l'image
     */
    public static Drawable getSportDrawable(String stringSuffix) {
        return getSportDrawable(stringSuffix, RidePlusApplication.configManager.getCurrentSport());
    }

    /**
     * Renvoi l'image correspondant à une image variable selon le sport courant.
     *
     * @param stringSuffix l'identifiant invariant de l'image variable à récupérer.
     * @param sport        le sport à utiliser
     * @return l'image
     */
    public static Drawable getSportDrawable(String stringSuffix, Sport sport) {
        return ContextCompat.getDrawable(applicationContext,
                applicationContext.getResources().getIdentifier(
                        "ic_" + sport.value() + stringSuffix,
                        "drawable",
                        applicationContext.getPackageName()));
    }

    /**
     * Renvoi l'icône illustrant la note d'un ride
     *
     * @param grade la note à illustrer
     * @param index l'index de l'icône dans le layout de note
     * @return l'image à afficher
     */
    public static Drawable getRideDrawable(double grade, int index) {
        Drawable drawable = AndroidUtils.getSportDrawable("_grade");
        if (drawable != null)
            drawable.setTint(ContextCompat.getColor(applicationContext, index + 1 <= grade ? grade == Constants.MAX_LEVEL ? R.color.corn : R.color.maya_blue : R.color.anti_flash_white));
        return drawable;
    }

    /**
     * Renvoi une chaine de caractère exprimant une durée.
     * <p>
     * Si la durée est supérieure à 60 min, elle est exprimée en heure+min.
     * Si la durée est supérieure à 60 s, elle est exprimée en min+sec.
     * Sinon elle est exprimée en secondes seulement
     *
     * @param duration  la durée à exprimer
     * @param precision vrai pour afficher le décompte des secondes en plus des minutes, faux pour juste afficher les minutes
     * @return la chaine de caractère exprimant la durée en paramètre
     */
    public static String getDurationText(double duration, boolean precision) {
        if (duration > 3600) {
            return String.format(applicationContext.getString(R.string.duration_hours_minutes), Math.floor(duration / 3600), duration % 3600 / 60);
        } else if (duration > 60) {
            if (precision)
                return String.format(applicationContext.getString(R.string.duration_minutes_seconds), Math.floor(duration / 60), duration % 60);
            else
                return String.format(applicationContext.getString(R.string.duration_minutes), Math.floor(duration / 60));
        } else {
            return String.format(applicationContext.getString(R.string.duration_seconds), duration);
        }
    }

}
