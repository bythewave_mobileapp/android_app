package com.bythewave.rideplus.common;

import android.widget.ImageView;

import com.bythewave.rideplus.R;
import com.esafirm.imagepicker.features.imageloader.ImageLoader;
import com.esafirm.imagepicker.features.imageloader.ImageType;

/**
 * com.by_the_wave.ride_plus_android.common.CustomImageLoader
 * <p/>
 * Implémentation utilisant Glide d'un imageLoader pour la librairie permettant de sélectionner une image
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 31/01/2018
 */
public class CustomImageLoader implements ImageLoader {
    @Override
    public void loadImage(String path, ImageView imageView, ImageType imageType) {
        AndroidUtils.displayImage(path, imageView, R.color.manatee);
    }
}
