package com.bythewave.rideplus;

import android.content.Context;
import androidx.multidex.MultiDexApplication;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatDelegate;

import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.manager.ActivityLifecycleManager;
import com.bythewave.rideplus.manager.BluetoothDeviceManager;
import com.bythewave.rideplus.manager.ConfigManager;
import com.bythewave.rideplus.manager.CurrentUserManager;
import com.bythewave.rideplus.manager.PreferencesManager;
import com.bythewave.rideplus.manager.SessionManager;
import com.bythewave.rideplus.models.enums.Sport;
import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.internal.RxBleLog;

import net.danlew.android.joda.JodaTimeAndroid;

import es.dmoral.toasty.Toasty;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * com.by_the_wave.ride_plus_android.RidePlusApplication
 * <p/>
 * Classe de l'application. Définit les singletons et configure les plugins de bas niveau
 * <p>
 * Sous classe de {@link MultiDexApplication}. Le sport MultiDex est requis pour accélérer les compilations et permettre l'instant run.
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 19/01/2018
 */
public class RidePlusApplication extends MultiDexApplication {

    public static Context applicationContext; // current application wide context
    public static ConfigManager configManager; // singleton gérant le fichier de configuration et les fichiers de conseils distants
    public static PreferencesManager preferencesManager; // singleton gérant les préférences partagées
    public static CurrentUserManager currentUserManager; // singleton gérant l'utilisateur courant
    public static SessionManager sessionManager; // singleton gérant certaines fonctions autour des sessions
    public static RxBleClient rxBleClient; // client Bluetooth
    public static BluetoothDeviceManager bluetoothDeviceManager; // singleton gérant les connexion BLE
    public static FirebaseAnalytics firebaseAnalytics; // object gérant les envois d'analyses

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Config de Firestore
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder().build();
        firestore.setFirestoreSettings(settings);

        //ATTENTION à l'ordre de lancement !
        applicationContext = getApplicationContext();
        preferencesManager = PreferencesManager.getInstance();
        configManager = ConfigManager.getInstance();
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        currentUserManager = CurrentUserManager.getInstance();
        sessionManager = SessionManager.getInstance();
        if (!BuildConfig.BUILD_TYPE.equals("simulator")) {
            rxBleClient = RxBleClient.create(applicationContext);
            RxBleClient.setLogLevel(RxBleLog.DEBUG);
        }
        bluetoothDeviceManager = BluetoothDeviceManager.getInstance();

        JodaTimeAndroid.init(this);

        //outil de debug
        Stetho.initializeWithDefaults(this);

        super.onCreate();
        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());

        //on sette la date de lancement de l'app si besoin
        preferencesManager.setFirstLaunchDate();

        // configuration de la police par défaut
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(Constants.DEFAULT_TYPEFACE)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Toasty.Config.getInstance()
                .setErrorColor(ContextCompat.getColor(this, R.color.flame))
                .setInfoColor(ContextCompat.getColor(this, R.color.lapis_lazuli))
                .setSuccessColor(ContextCompat.getColor(this, R.color.cadet_blue))
                .setWarningColor(ContextCompat.getColor(this, R.color.flame))
                .setTextColor(ContextCompat.getColor(this, R.color.white))
                .tintIcon(true)
                .apply();

        //on ajoute une session de test
        if (BuildConfig.BUILD_TYPE.equals("debug") || BuildConfig.BUILD_TYPE.equals("staging")) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    for (Sport sport : Sport.values())
                        RidePlusApplication.sessionManager.addExampleSession(sport);

                }
            };
            thread.start();
        }


        RxBleLog.setLogLevel(RxBleLog.VERBOSE);

        //on ajote un listener sur le cycle de vie des activités
        registerActivityLifecycleCallbacks(new ActivityLifecycleManager());
    }


    @Override
    public void onTerminate() {
        bluetoothDeviceManager.cancelSubscriptions();
        super.onTerminate();
    }


}
