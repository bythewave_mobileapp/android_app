package com.bythewave.rideplus.adapters.viewholders;

import android.view.View;

/**
 * com.bythewave.rideplus.adapters.viewholders.LockedWaveViewHolder
 * <p/>
 * <p>
 * ViewHolder d'une vague ou d'un ride fait avec le téléphone
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 15/11/2018
 */
public class LockedWaveViewHolder extends WaveViewHolder {

    public LockedWaveViewHolder(View view) {
        super(view);
        displayMask = true;
    }
}
