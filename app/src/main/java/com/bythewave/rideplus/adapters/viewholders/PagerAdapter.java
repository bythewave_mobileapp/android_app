package com.bythewave.rideplus.adapters.viewholders;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * com.by_the_wave.ride_plus_android.adapters.viewholders
 * <p/>
 * Adapter générique
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 29/01/2018
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

    List<Fragment> fragments; // Liste des mFragments utilisés

    List<String> titles; // liste des titres utilisés

    public PagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
        this.titles = null;
    }

    public PagerAdapter(FragmentManager fm, List<Fragment> fragments, List<String> titles) {
        super(fm);
        this.fragments = fragments;
        this.titles = titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public void setFragments(List<Fragment> fragments) {
        this.fragments = fragments;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (titles != null && position < titles.size()) {
            return titles.get(position);
        } else {
            return "";
        }
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }

}
