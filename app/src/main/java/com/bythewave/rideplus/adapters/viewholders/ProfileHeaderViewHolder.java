package com.bythewave.rideplus.adapters.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.User;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.models.enums.Stance;
import com.bythewave.rideplus.ui.SportButton;
import com.lapptelier.smartrecyclerview.SmartViewHolder;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;

/**
 * com.by_the_wave.ride_plus_android.adapters.viewholders.PerkViewHolder
 * <p/>
 * Classe gérant l'affichage du cartouche de sélection d'horizon de statistiques
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 17/02/2018
 */
public class ProfileHeaderViewHolder extends SmartViewHolder<User> {

    @BindView(R.id.perks_range)
    protected SegmentedButtonGroup range;

    @BindView(R.id.header_avatar)
    protected RoundedImageView headerAvatar;

    @BindView(R.id.header_cover)
    protected ImageView headerCover;

    @BindView(R.id.header_name)
    protected TextView headerName;

    @BindView(R.id.header_stance_text)
    protected TextView headerStanceText;

    @BindView(R.id.header_stance_goofy)
    protected ImageView headerStanceGoofy;

    @BindView(R.id.header_stance_regular)
    protected ImageView headerStanceRegular;

    @BindView(R.id.header_sport)
    protected SportButton headerSport;


    public ProfileHeaderViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, itemView);

        //on recharge la liste des statistiques au changement d'horizon
        range.setOnPositionChangedListener(position ->
                BusMessage.post(BusMessageType.PERK_RANGE_CHANGE, position)
        );

        //par défaut, on affiche les dernières 24h
        range.setPosition(0, false);
    }

    @Override
    public void setItem(final User currentUser, final ViewHolderInteractionListener viewHolderInteractionListener) {
        if (currentUser != null) {
            headerName.setText(currentUser.name);

            headerSport.setEnabled(true);

            if (currentUser.stance == Stance.REGULAR) {
                headerStanceRegular.setVisibility(View.VISIBLE);
                headerStanceGoofy.setVisibility(View.GONE);
                headerStanceText.setText(itemView.getContext().getString(R.string.stance_regular));
            } else {
                headerStanceRegular.setVisibility(View.GONE);
                headerStanceGoofy.setVisibility(View.VISIBLE);
                headerStanceText.setText(itemView.getContext().getString(R.string.stance_goofy));
            }
            AndroidUtils.displayRoundedImage(currentUser.avatarUrl, headerAvatar, R.drawable.ic_logo_btw);
            AndroidUtils.displayImage(String.format(Constants.DEFAULT_COVER_FILE, RidePlusApplication.configManager.getCurrentSport().value()), headerCover, R.color.corn);
        }
    }

    @OnClick(R.id.header_sport)
    protected void changeSport() {
        int[] location = new int[2];
        headerSport.getLocationOnScreen(location);
        BusMessage.post(BusMessageType.CHANGE_SPORT_REQUEST, location);
    }
}
