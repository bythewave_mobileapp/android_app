package com.bythewave.rideplus.adapters.viewholders;

import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.activities.SessionActivity;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.common.OnSwipeListener;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Wave;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.models.enums.Stance;
import com.bythewave.rideplus.ui.CustomSwipeLayout;
import com.lapptelier.smartrecyclerview.SmartViewHolder;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * com.by_the_wave.ride_plus_android.adapters.viewholders.WaveViewHolder
 * <p/>
 * Classe gérant l'affichage du cartouche d'une vague
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 20/02/2018
 */
public class WaveViewHolder extends SmartViewHolder<Wave> {

    private static final int MAX_PRO_CONS_FIELDS = 10;
    private static final int MAX_ADVISES = 3;
    private static final int MAX_PROS = 2;
    private static final int MAX_CONS = 1;

    @BindView(R.id.wave_layout)
    @Nullable
    protected ConstraintLayout waveLayout;

    @BindView(R.id.wave_frame)
    @Nullable
    protected FrameLayout frameLayout;

    @BindView(R.id.wave_fling_receiver)
    @Nullable
    protected View flingReceiver;

    @BindView(R.id.wave_scrollview)
    @Nullable
    protected NestedScrollView scrollView;

    @BindView(R.id.wave_delete)
    @Nullable
    protected ImageButton deleteButton;

    @BindView(R.id.wave_distance)
    @Nullable
    protected TextView distance;

    @BindView(R.id.wave_detail_layout)
    @Nullable
    protected LinearLayout detailLayout;

    @BindView(R.id.wave_level_label)
    @Nullable
    protected TextView levelLabel;

    @BindView(R.id.wave_swipe_layout)
    @Nullable
    protected CustomSwipeLayout swipeLayout;

    @BindView(R.id.wave_level)
    protected ConstraintLayout waveLevelLayout;

    @BindView(R.id.wave_perk_pair)
    @Nullable
    protected LinearLayout topPerkLayout;

    @BindView(R.id.wave_speed_perk_pair)
    @Nullable
    protected LinearLayout bottomPerkLayout;

    protected ConstraintLayout topLeftPerkLayout;
    protected ConstraintLayout topRightPerkLayout;
    protected TextView topLeftPerkName;
    protected TextView topLeftPerkValue;
    protected ConstraintLayout topLeftPerkLevel;
    protected TextView topRightPerkName;
    protected TextView topRightPerkValue;
    protected ConstraintLayout topRightPerkLevel;

    protected ConstraintLayout bottomLeftPerkLayout;
    protected ConstraintLayout bottomRightPerkLayout;
    protected TextView bottomLeftPerkName;
    protected TextView bottomLeftPerkValue;
    protected ConstraintLayout bottomLeftPerkLevel;
    protected TextView bottomRightPerkName;
    protected TextView bottomRightPerkValue;
    protected ConstraintLayout bottomRightPerkLevel;

    @BindView(R.id.wave_coach_advices)
    @Nullable
    protected TextView advices;

    @BindView(R.id.wave_image)
    @Nullable
    protected ImageView coachingImage;

    @BindView(R.id.wave_mask)
    @Nullable
    protected LinearLayout mask;

    @BindView(R.id.wave_shop_button)
    @Nullable
    protected Button shopButton;

    protected List<ImageView> waves;
    protected List<TextView> proConsTextViews;

    // Flag indiquant si il faut afficher le masque d'une vague "verrouillée" (faite au téléphone)
    protected boolean displayMask = false;


    public WaveViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, itemView);

        waves = new ArrayList<>();
        for (int index = 0; index < Constants.MAX_LEVEL - 1; index++) {
            String imageViewId = "wave_" + index;
            int imageViewResId = waveLevelLayout.getResources().getIdentifier(imageViewId, "id", RidePlusApplication.applicationContext.getPackageName());
            waves.add(view.findViewById(imageViewResId));
        }

        if (topPerkLayout != null) {
            topLeftPerkLayout = topPerkLayout.findViewById(R.id.perk_left);
            topRightPerkLayout = topPerkLayout.findViewById(R.id.perk_right);
            if (topLeftPerkLayout != null && topRightPerkLayout != null) {
                topLeftPerkName = topLeftPerkLayout.findViewById(R.id.perk_name);
                topLeftPerkValue = topLeftPerkLayout.findViewById(R.id.perk_value);
                topLeftPerkLevel = topLeftPerkLayout.findViewById(R.id.perk_level);

                topRightPerkName = topRightPerkLayout.findViewById(R.id.perk_name);
                topRightPerkValue = topRightPerkLayout.findViewById(R.id.perk_value);
                topRightPerkLevel = topRightPerkLayout.findViewById(R.id.perk_level);
            }
        }

        if (bottomPerkLayout != null) {
            bottomLeftPerkLayout = bottomPerkLayout.findViewById(R.id.perk_left);
            bottomRightPerkLayout = bottomPerkLayout.findViewById(R.id.perk_right);
            if (bottomLeftPerkLayout != null && bottomRightPerkLayout != null) {
                bottomLeftPerkName = bottomLeftPerkLayout.findViewById(R.id.perk_name);
                bottomLeftPerkValue = bottomLeftPerkLayout.findViewById(R.id.perk_value);
                bottomLeftPerkLevel = bottomLeftPerkLayout.findViewById(R.id.perk_level);

                bottomRightPerkName = bottomRightPerkLayout.findViewById(R.id.perk_name);
                bottomRightPerkValue = bottomRightPerkLayout.findViewById(R.id.perk_value);
                bottomRightPerkLevel = bottomRightPerkLayout.findViewById(R.id.perk_level);
            }
        }


        if (detailLayout != null) {
            proConsTextViews = new ArrayList<>();
            for (int index = 0; index < MAX_PRO_CONS_FIELDS; index++) {
                String viewName = "wave_pro_con_" + index;
                int viewId = view.getResources().getIdentifier(viewName, "id", RidePlusApplication.applicationContext.getPackageName());
                proConsTextViews.add(detailLayout.findViewById(viewId));
            }
        }

        //on configure manuellement les éléments de la swipe view
        if (swipeLayout != null) {
            swipeLayout.setRightColors(new int[]{ContextCompat.getColor(itemView.getContext(), R.color.flame)});
            swipeLayout.setRightIconColors(new int[]{ContextCompat.getColor(itemView.getContext(), R.color.white)});
            swipeLayout.setRightIcons(new int[]{R.drawable.ic_delete});
            swipeLayout.invalidateSwipeItems();
        }

    }

    @Override
    public void setItem(final Wave wave, final ViewHolderInteractionListener viewHolderInteractionListener) {
        if (mask != null)
            mask.setVisibility(displayMask ? View.VISIBLE : View.GONE);

        if (shopButton != null) {
            shopButton.setVisibility(displayMask ? View.VISIBLE : View.GONE);
            shopButton.setOnClickListener(listener -> {
                // on ouvre la page web de la boutique BTW
                if (displayMask) {
                    BusMessage.post(BusMessageType.OPEN_URL, Uri.parse(itemView.getContext().getString(R.string.shop_url)));
                }
            });
        }

        if (wave != null) {
            if (wave.distance > 0 && distance != null)
                distance.setText(String.format(itemView.getContext().getString(R.string.distance), wave.distance));

            if (levelLabel != null) {
                levelLabel.setText(AndroidUtils.getSportString("_ride_level"));
                levelLabel.setVisibility(displayMask ? View.GONE : View.VISIBLE);
            }

            //si session verrouillée, on n'affiche pas les vagues
            if (displayMask) {
                for (int index = 0; index < waves.size(); index++) {
                    waves.get(index).setVisibility(View.GONE);
                }

            } else {
                //configuration de l'indicateur de vague
                for (int index = 0; index < waves.size(); index++) {
                    waves.get(index).setVisibility(View.VISIBLE);
                    waves.get(index).setImageDrawable(AndroidUtils.getRideDrawable(wave.level, index).mutate());
                }
            }

            if (topLeftPerkLayout != null && topRightPerkLayout != null) {
                topLeftPerkName.setText(itemView.getContext().getString(R.string.distance_label));
                topLeftPerkValue.setText(String.format(itemView.getContext().getString(R.string.distance), wave.distance));
                topLeftPerkValue.setVisibility(View.VISIBLE);
                topLeftPerkLevel.setVisibility(View.INVISIBLE);

                topRightPerkName.setText(itemView.getContext().getString(R.string.wave_duration_label));
                topRightPerkValue.setText(Html.fromHtml(AndroidUtils.getDurationText(wave.duration, true)));
                topRightPerkValue.setVisibility(View.VISIBLE);
                topRightPerkLevel.setVisibility(View.INVISIBLE);
            }
            if (bottomLeftPerkLayout != null && bottomRightPerkLayout != null) {
                bottomLeftPerkName.setText(itemView.getContext().getString(R.string.average_speed_label));
                bottomLeftPerkValue.setText(String.format(itemView.getContext().getString(R.string.unit_kph), wave.getAverageSpeed()));
                bottomLeftPerkValue.setVisibility(View.VISIBLE);
                bottomLeftPerkLevel.setVisibility(View.INVISIBLE);

                bottomRightPerkName.setText(itemView.getContext().getString(R.string.max_speed_label));
                bottomRightPerkValue.setText(String.format(itemView.getContext().getString(R.string.unit_kph), wave.getMaxSpeed()));
                bottomRightPerkValue.setVisibility(View.VISIBLE);
                bottomRightPerkLevel.setVisibility(View.INVISIBLE);
            }


            // si vague ou ride fait avec un smartphone, on pioche des conseils au pifs parmis les niveaux
            int level = wave.level;
            if (displayMask) {
                level = Math.abs(Long.valueOf(wave.startedAt.getTime() / 1000).intValue()) % (Constants.MAX_LEVEL - 1);
            }

            //on ajoute les points positifs / négatifs
            if (detailLayout != null) {
                int proCount = (Math.abs(Long.valueOf(wave.startedAt.getTime() / 1000).intValue()) % MAX_PROS) + 1;

                //on pioche 1 ou 2 pro au hasard et 1 con
                List<String> prosString = RidePlusApplication.configManager.getPros(level, proCount, wave.startedAt.getTime());
                List<String> consString = RidePlusApplication.configManager.getCons(level, MAX_CONS, wave.startedAt.getTime());
                ArrayList<String> proConsArray = new ArrayList<>();
                proConsArray.addAll(prosString);
                proConsArray.addAll(consString);
                for (int i = 0; i < MAX_PRO_CONS_FIELDS; i++) {
                    TextView textView = proConsTextViews.get(i);
                    if (i < proConsArray.size()) {
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(proConsArray.get(i));
                        textView.setCompoundDrawablesWithIntrinsicBounds(itemView.getContext().getDrawable(i > prosString.size() - 1 ? R.drawable.ic_shaka_negative : R.drawable.ic_shaka_positive), null, null, null);
                        textView.setTextColor(ContextCompat.getColor(itemView.getContext(), i > prosString.size() - 1 ? R.color.redwood : R.color.cadet_blue));
                    } else {
                        textView.setVisibility(View.GONE);
                    }
                }
            }


            if (RidePlusApplication.currentUserManager.hasCurrentUser()) {
                boolean isFrontside = wave.isFrontside(RidePlusApplication.currentUserManager.getCurrentUser().stance);
                if (advices != null) {
                    //on pioche aléatoirement 3 conseils
                    advices.setText(TextUtils.join("\n\n", RidePlusApplication.configManager.getAdvices(level, isFrontside, MAX_ADVISES, wave.startedAt.getTime())));
                }
                if (coachingImage != null) {
                    String imageUrl = RidePlusApplication.configManager.getLevelImage(level, isFrontside, wave.startedAt.getTime());
                    AndroidUtils.displayImage(imageUrl, coachingImage, R.color.white);
                    //si l'utilisateur est goofy, on retourne l'image
                    if (RidePlusApplication.currentUserManager.hasCurrentUser() && RidePlusApplication.currentUserManager.getCurrentUser().stance == Stance.GOOFY)
                        coachingImage.setScaleX(-1f);
                }
            }


            if (deleteButton != null) {
                //on affiche le bouton pour sélectionner la cellule en fonction du marqueur sur le WaveManager
                if (RidePlusApplication.sessionManager != null && RidePlusApplication.sessionManager.isSelectingWaves())
                    deleteButton.setVisibility(View.VISIBLE);
                else
                    deleteButton.setVisibility(View.GONE);

                deleteButton.setSelected(RidePlusApplication.sessionManager.isMarkedToDelete(wave));
            }

            if (swipeLayout != null) {
                // gestion du click sur l'intégralité de la cellule

                swipeLayout.setOnClickListener(view -> {
                    //on ferme le swipe si on click sur la cellule
                    if (swipeLayout.isExpanded()) swipeLayout.collapseAll(true);

                    //on gère les clics uniquement si la vue est en sport suppression
                    if (RidePlusApplication.sessionManager.isSelectingWaves()) {
                        deleteButton.setSelected(!deleteButton.isSelected());
                        configureSelectionButton(wave, deleteButton.isSelected());
                        //on indique au singleton gérant les waves que la wave est marqué à suppression
                        if (deleteButton.isSelected())
                            RidePlusApplication.sessionManager.addWaveToDelete(wave);
                        else
                            RidePlusApplication.sessionManager.removeWaveToDelete(wave);
                    } else {
                        //on vérifie que le layout n'est pas ouvert par un swipe
                        if (!swipeLayout.isExpanded() && !swipeLayout.isExpanding()) {
                            waveLayout.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.white_smoke));
                            BusMessage.post(BusMessageType.ITEM_MARKED, wave);
                        }
                    }
                });

                //gestion action sur swipe
                swipeLayout.setOnSwipeItemClickListener((left, index) -> {
                    if (!left) {
                        switch (index) {
                            // suppression
                            case 0:
                                BusMessage.post(BusMessageType.DELETE_ELEMENT, wave);
                                break;

                        }
                        swipeLayout.collapseAll(true);
                    }
                });

                //par défaut, la cellule est non marqué à suppression
                configureSelectionButton(wave, false);

            }

            if (waveLayout != null)
                waveLayout.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.white));

            if (frameLayout != null) {
                ViewGroup.LayoutParams params = frameLayout.getLayoutParams();
                params.width = (int) (AndroidUtils.getScreenWidth() - itemView.getContext().getResources().getDimension(R.dimen.dp32));
                frameLayout.setLayoutParams(params);
            }

//            par défaut, la liste des sessions/vague est masquée. Par conséquent, la vue flingReceiver doit prendre la main sur la scrollview
            if (flingReceiver != null) {
                flingReceiver.setOnTouchListener((view, motionEvent) -> {
                    //si l'utilisateur swipe vers le bas avec la liste des vagues/session réduite, on appelle l'activité parente pour afficher la taille maximale de la liste des vagues/session
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP && itemView.getContext() instanceof SessionActivity && ((SessionActivity) itemView.getContext()).getListExpansionState() == SessionActivity.ViewState.LIST_COLLAPSED) {
                        ((SessionActivity) itemView.getContext()).displayWaveList(true);
                        return true;
                    }
                    return itemView.getContext() instanceof SessionActivity && ((SessionActivity) itemView.getContext()).getListExpansionState() == SessionActivity.ViewState.LIST_COLLAPSED;
                });
            }

            if (scrollView != null) {
                scrollView.scrollTo(0, 0);
                scrollView.setOnTouchListener(new OnSwipeListener(itemView.getContext()) {
                    @Override
                    public boolean onSwipeDown() {
                        //si l'utilisateur est au sommet de la vue, avec la liste des vagues/session affichée en grand, et swipe vers le bas, on appelle l'activité parente pour réduire la taille de la liste des vagues/session
                        if (scrollView.getScrollY() == 0 && itemView.getContext() instanceof SessionActivity && ((SessionActivity) itemView.getContext()).getListExpansionState() == SessionActivity.ViewState.LIST_EXPANDED) {
                            ((SessionActivity) itemView.getContext()).displayWaveList(false);
                        }
                        return false;
                    }

                });
            }

        }

    }

    private void configureSelectionButton(Wave wave, boolean toDelete) {
        if (waveLayout != null) {
            waveLayout.setBackgroundResource(toDelete ? R.drawable.ripple_white_blue_smoke : R.drawable.ripple_white);
            BusMessage.post(toDelete ? BusMessageType.MARK_TO_DELETE : BusMessageType.UNMARK_TO_DELETE, wave);
        }
    }

}
