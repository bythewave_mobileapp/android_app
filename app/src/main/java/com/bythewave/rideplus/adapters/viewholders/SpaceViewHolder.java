package com.bythewave.rideplus.adapters.viewholders;

import android.view.View;

import com.lapptelier.smartrecyclerview.SmartViewHolder;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;

/**
 * com.by_the_wave.ride_plus_android.adapters.viewholders.SpaceViewHolder
 * <p/>
 * Cellule présentant un espace en fin de liste de session, pour centrer la dernière vague affichée
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 28/02/2018
 */
public class SpaceViewHolder extends SmartViewHolder<Boolean> {

    /**
     * Constructeur vide
     *
     * @param itemView la vue de la cellule
     */
    public SpaceViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setItem(Boolean dummy, final ViewHolderInteractionListener viewHolderInteractionListener) {
    }
}
