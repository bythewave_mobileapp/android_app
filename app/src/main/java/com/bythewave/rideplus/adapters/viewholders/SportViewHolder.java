package com.bythewave.rideplus.adapters.viewholders;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.models.enums.Sport;
import com.lapptelier.smartrecyclerview.SmartViewHolder;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * com.by_the_wave.ride_plus_android.adapters.viewholders.PerkViewHolder
 * <p/>
 * Classe gérant l'affichage du cartouche de sélection d'un sport
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 13/11/2018
 */
public class SportViewHolder extends SmartViewHolder<Sport> {

    @BindView(R.id.sport_title)
    protected TextView sportLabel;

    @BindView(R.id.sport_icon)
    protected ImageView sportImage;

    public SportViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setItem(final Sport sport, final ViewHolderInteractionListener viewHolderInteractionListener) {
        if (sport != null) {
            int color;
            if (sport.equals(RidePlusApplication.configManager.getCurrentSport())) {
                color = ContextCompat.getColor(itemView.getContext(), R.color.maya_blue);
            } else {
                color = ContextCompat.getColor(itemView.getContext(), R.color.manatee);
            }

            Drawable sportIcon = AndroidUtils.getSportDrawable("_sport", sport);
            if (sportIcon != null)
                sportIcon.setTint(color);
            sportImage.setImageDrawable(sportIcon);

            sportLabel.setText(AndroidUtils.getSportString("_sport", sport));
            sportLabel.setTextColor(color);

            sportLabel.setOnClickListener(v -> RidePlusApplication.configManager.setCurrentSport(sport));
            sportImage.setOnClickListener(v -> RidePlusApplication.configManager.setCurrentSport(sport));
            itemView.setOnClickListener(v -> RidePlusApplication.configManager.setCurrentSport(sport));
        }
    }
}
