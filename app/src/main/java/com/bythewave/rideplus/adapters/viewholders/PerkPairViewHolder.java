package com.bythewave.rideplus.adapters.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.models.PerkPair;
import com.lapptelier.smartrecyclerview.SmartViewHolder;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;

import java.util.ArrayList;
import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * com.by_the_wave.ride_plus_android.adapters.viewholders.PerkViewHolder
 * <p/>
 * Classe gérant l'affichage du cartouche d'une paire de statistiques
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 17/02/2018
 */
public class PerkPairViewHolder extends SmartViewHolder<PerkPair> {

    @BindView(R.id.perk_left)
    protected ConstraintLayout leftPerkLayout;

    @BindView(R.id.perk_right)
    protected ConstraintLayout rightPerkLayout;

    protected TextView leftPerkName;
    protected TextView leftPerkValue;
    protected ConstraintLayout leftPerkLevel;

    protected TextView rightPerkName;
    protected TextView rightPerkValue;
    protected ConstraintLayout rightPerkLevel;

    protected List<ImageView> wavesLeft;
    protected List<ImageView> wavesRight;

    public PerkPairViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, itemView);

        leftPerkName = leftPerkLayout.findViewById(R.id.perk_name);
        leftPerkValue = leftPerkLayout.findViewById(R.id.perk_value);
        leftPerkLevel = leftPerkLayout.findViewById(R.id.perk_level);

        rightPerkName = rightPerkLayout.findViewById(R.id.perk_name);
        rightPerkValue = rightPerkLayout.findViewById(R.id.perk_value);
        rightPerkLevel = rightPerkLayout.findViewById(R.id.perk_level);

        wavesLeft = new ArrayList<>();
        wavesRight = new ArrayList<>();
        for (int index = 0; index < Constants.MAX_LEVEL - 1; index++) {
            String imageViewId = "wave_" + index;
            wavesLeft.add(leftPerkLayout.findViewById(view.getResources().getIdentifier(imageViewId, "id", RidePlusApplication.applicationContext.getPackageName())));
            wavesRight.add(rightPerkLayout.findViewById(view.getResources().getIdentifier(imageViewId, "id", RidePlusApplication.applicationContext.getPackageName())));
        }
    }

    @Override
    public void setItem(final PerkPair perkPair, final ViewHolderInteractionListener viewHolderInteractionListener) {
        if (perkPair != null) {
            leftPerkName.setText(perkPair.leftPair.name);
            leftPerkValue.setText(perkPair.leftPair.value);

            if (perkPair.leftPair.isLevel) {
                leftPerkValue.setVisibility(View.INVISIBLE);
                leftPerkLevel.setVisibility(View.VISIBLE);
                //configuration de l'indicateur de vague
                for (int index = 0; index < wavesLeft.size(); index++) {
                    if (RidePlusApplication.applicationContext != null)
                        wavesLeft.get(index).setImageDrawable(AndroidUtils.getRideDrawable(Double.valueOf(perkPair.leftPair.value).intValue(), index).mutate());
                }

            } else {
                leftPerkLevel.setVisibility(View.INVISIBLE);
                leftPerkValue.setVisibility(View.VISIBLE);
            }

            rightPerkName.setText(perkPair.rightPair.name);
            rightPerkValue.setText(perkPair.rightPair.value);

            if (perkPair.rightPair.isLevel) {
                rightPerkValue.setVisibility(View.INVISIBLE);
                rightPerkLevel.setVisibility(View.VISIBLE);
                //configuration de l'indicateur de vague
                for (int index = 0; index < wavesRight.size(); index++) {
                    if (RidePlusApplication.applicationContext != null)
                        wavesRight.get(index).setImageDrawable(AndroidUtils.getRideDrawable(Double.valueOf(perkPair.rightPair.value).intValue(), index).mutate());
                }

            } else {
                rightPerkLevel.setVisibility(View.INVISIBLE);
                rightPerkValue.setVisibility(View.VISIBLE);
            }
        }
    }


}
