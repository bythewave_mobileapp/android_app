package com.bythewave.rideplus.adapters.viewholders;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bythewave.rideplus.R;
import com.bythewave.rideplus.RidePlusApplication;
import com.bythewave.rideplus.activities.SessionActivity;
import com.bythewave.rideplus.common.AndroidUtils;
import com.bythewave.rideplus.common.Constants;
import com.bythewave.rideplus.common.OnSwipeListener;
import com.bythewave.rideplus.models.BusMessage;
import com.bythewave.rideplus.models.Session;
import com.bythewave.rideplus.models.enums.BusMessageType;
import com.bythewave.rideplus.ui.CustomSwipeLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lapptelier.smartrecyclerview.SmartViewHolder;
import com.lapptelier.smartrecyclerview.ViewHolderInteractionListener;
import com.socks.library.KLog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bythewave.rideplus.RidePlusApplication.applicationContext;

/**
 * com.by_the_wave.ride_plus_android.adapters.viewholders.SessionViewHolder
 * <p/>
 * Classe gérant l'affichage du cartouche d'une session
 * <p/>
 *
 * @author L'Apptelier SARL
 * @date 24/01/2018
 */
public class SessionViewHolder extends SmartViewHolder<Session> {

    @BindView(R.id.session_frame)
    @Nullable
    protected FrameLayout frameLayout;

    @BindView(R.id.session_fling_receiver)
    @Nullable
    protected View flingReceiver;

    @BindView(R.id.session_layout)
    @Nullable
    protected ConstraintLayout sessionLayout;

    @BindView(R.id.session_scrollview)
    @Nullable
    protected NestedScrollView scrollView;

    @BindView(R.id.session_delete)
    @Nullable
    protected ImageButton deleteButton;

    @BindView(R.id.session_spot_name)
    protected TextView spotName;

    @BindView(R.id.session_level_label)
    @Nullable
    protected TextView levelLabel;

    @BindView(R.id.session_separator_1)
    @Nullable
    protected View separator;

    @BindView(R.id.session_city_country)
    @Nullable
    protected TextView cityCountry;

    @BindView(R.id.session_comment)
    @Nullable
    protected TextView comment;

    @BindView(R.id.session_edit)
    @Nullable
    protected Button editButton;

    @BindView(R.id.session_date)
    protected TextView date;

    @BindView(R.id.session_duration)
    protected TextView duration;

    @BindView(R.id.session_wave_count)
    protected TextView waveCount;

    @BindView(R.id.session_swipe_layout)
    @Nullable
    protected CustomSwipeLayout swipeLayout;

    protected List<ImageView> waves;


    public SessionViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, itemView);

        waves = new ArrayList<>();
        for (int index = 0; index < Constants.MAX_LEVEL - 1; index++) {
            String viewName = "wave_" + index;
            int viewId = view.getResources().getIdentifier(viewName, "id", RidePlusApplication.applicationContext.getPackageName());
            waves.add(view.findViewById(viewId));
        }

    }


    @Override
    public void setItem(final Session session, final ViewHolderInteractionListener viewHolderInteractionListener) {
        if (session != null) {
            //on modifie le poid de la police selon si la session a été vue ou non
            Typeface boldTypeface = Typeface.createFromAsset(RidePlusApplication.applicationContext.getAssets(), session.isViewed ? Constants.DEFAULT_TYPEFACE : Constants.BOLD_TYPEFACE);
            spotName.setTypeface(boldTypeface);

            //on modifie la couleur de la police selon si la session a été vue ou non
            spotName.setTextColor(ContextCompat.getColor(itemView.getContext(), session.isFavorite ? R.color.maya_blue : R.color.black));

            if (TextUtils.isEmpty(session.getName())) {
                //on affiche un nom temporaire
                spotName.setText(itemView.getContext().getString(R.string.default_spot_name));
            } else {
                spotName.setText(session.getName());
            }

            SimpleDateFormat simpleFormat = new SimpleDateFormat(applicationContext.getString(comment != null ? R.string.date_time_complete : R.string.date_complete), Locale.FRENCH);
            date.setText(simpleFormat.format(session.startedAt));
            if (session.getDuration() > -1) {
                //on affiche la durée en h+min si plus d'une heure
                duration.setText(Html.fromHtml(AndroidUtils.getDurationText(session.getDuration(), false)));
            } else {
                duration.setText("");
            }

            if (session.getVisibleWaves() != null)
                waveCount.setText(String.format(AndroidUtils.getSportString("_ride_count"), session.getVisibleWaves().size(), session.getVisibleWaves().size() > 1 ? itemView.getContext().getString(R.string.plural) : ""));

            if (cityCountry != null) {
                cityCountry.setText(session.getCityCountry());
            }

            if (comment != null) {
                comment.setText(TextUtils.isEmpty(session.comment) ? itemView.getContext().getString(R.string.empty_comment) : session.comment);
                comment.setTextColor(ContextCompat.getColor(itemView.getContext(), TextUtils.isEmpty(session.comment) ? R.color.lavender_gray : R.color.black));
            }

            if (levelLabel != null) {
                levelLabel.setVisibility(session.isPhoneSession() ? View.GONE : View.VISIBLE);
            }

            if (separator != null) {
                separator.setVisibility(session.isPhoneSession() ? View.GONE : View.VISIBLE);
            }

            //si session verrouillée, on n'affiche pas les vagues
            if (session.isPhoneSession()) {
                for (int index = 0; index < waves.size(); index++) {
                    waves.get(index).setVisibility(View.INVISIBLE);
                }
            } else {
                //configuration de l'indicateur de vague
                int avgLevel = session.getAverageLevel();
                for (int index = 0; index < waves.size(); index++) {
                    waves.get(index).setVisibility(View.VISIBLE);
                    waves.get(index).setImageDrawable(AndroidUtils.getRideDrawable(avgLevel, index).mutate());
                }
            }

            //on affiche le bouton pour sélectionner la cellule en fonction du marqueur sur le SessionManager
            if (deleteButton != null) {
                if (RidePlusApplication.sessionManager != null && RidePlusApplication.sessionManager.isSelectingSessions())
                    deleteButton.setVisibility(View.VISIBLE);
                else
                    deleteButton.setVisibility(View.GONE);
                deleteButton.setSelected(RidePlusApplication.sessionManager.isMarkedToDelete(session));

                //par défaut, la cellule est non marqué à suppression
                configureSelectionButton(session, false);
            }

            if (editButton != null) {
                editButton.setOnClickListener(view -> AsyncTask.execute(() -> BusMessage.post(BusMessageType.OPEN_EDIT_SESSION, session)));
            }

            if (frameLayout != null) {
                ViewGroup.LayoutParams params = frameLayout.getLayoutParams();
                params.width = (int) (AndroidUtils.getScreenWidth() - itemView.getContext().getResources().getDimension(R.dimen.dp32));
                frameLayout.setLayoutParams(params);
            }

            // par défaut, la liste des sessions/vague est masquée. Par conséquent, la vue flingReceiver doit prendre la main sur la scrollview
            if (flingReceiver != null) {
                flingReceiver.setOnTouchListener((view, motionEvent) -> {
                    //si l'utilisateur swipe vers le bas avec la liste des vagues/session réduite, on appelle l'activité parente pour afficher la taille maximale de la liste des vagues/session
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP && itemView.getContext() instanceof SessionActivity && ((SessionActivity) itemView.getContext()).getListExpansionState() == SessionActivity.ViewState.LIST_COLLAPSED) {
                        ((SessionActivity) itemView.getContext()).displayWaveList(true);
                        return true;
                    }
                    return itemView.getContext() instanceof SessionActivity && ((SessionActivity) itemView.getContext()).getListExpansionState() == SessionActivity.ViewState.LIST_COLLAPSED;
                });
            }

            if (scrollView != null) {
                scrollView.scrollTo(0, 0);
                scrollView.setOnTouchListener(new OnSwipeListener(itemView.getContext()) {
                    @Override
                    public boolean onSwipeDown() {
                        //si l'utilisateur est au sommet de la vue, avec la liste des vagues/session affichée en grand, et swipe vers le bas, on appelle l'activité parente pour réduire la taille de la liste des vagues/session
                        if (scrollView.getScrollY() == 0 && itemView.getContext() instanceof SessionActivity && ((SessionActivity) itemView.getContext()).getListExpansionState() == SessionActivity.ViewState.LIST_EXPANDED) {
                            ((SessionActivity) itemView.getContext()).displayWaveList(false);
                        }
                        return false;
                    }

                });
            }


            //gestion action sur swipe
            if (swipeLayout != null) {
                swipeLayout.setOnSwipeItemClickListener((left, index) -> {
                    if (!left) {
                        switch (index) {
                            //favori
                            case 0:
                                session.isFavorite = !session.isFavorite;
                                setItem(session, viewHolderInteractionListener);

                                // Firebase log
                                RidePlusApplication.firebaseAnalytics.logEvent(Constants.EVENT_SESSION_STARRED, null);

                                FirebaseFirestore.getInstance().collection(Constants.COLLECTIONS_SESSIONS)
                                        .document(session.getSessionId())
                                        .set(session)
                                        .addOnFailureListener(exception -> {
                                            KLog.e(exception);
                                            BusMessage.post(BusMessageType.ERROR_GENERIC_SERVER);
                                            session.isFavorite = !session.isFavorite;
                                            setItem(session,viewHolderInteractionListener);
                                        });
                                break;
                            // suppression
                            case 1:
                                BusMessage.post(BusMessageType.DELETE_ELEMENT, session);
                                break;

                        }
                        swipeLayout.collapseAll(true);
                    }
                });
                swipeLayout.setOnlyOneSwipe(true);
                swipeLayout.setOnClickListener(view -> {
                    //on ferme le swipe si on click sur la cellule
                    if (swipeLayout.isExpanded()) swipeLayout.collapseAll(true);

                    //on gère les clics uniquement si la vue est en sport suppression
                    if (RidePlusApplication.sessionManager.isSelectingSessions()) {
                        deleteButton.setSelected(!deleteButton.isSelected());
                        configureSelectionButton(session, deleteButton.isSelected());
                        //on indique au singleton gérant les sessions que la session est marqué à suppression
                        if (deleteButton.isSelected())
                            RidePlusApplication.sessionManager.addSessionToDelete(session);
                        else
                            RidePlusApplication.sessionManager.removeSessionToDelete(session);
                    } else {
                        //on vérifie que le layout n'est pas ouvert par un swipe
                        if (!swipeLayout.isExpanded() && !swipeLayout.isExpanding()) {
                            sessionLayout.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.white_smoke));
                            BusMessage.post(BusMessageType.OPEN_SESSION, session);
                        }
                    }
                });

                if (sessionLayout != null)
                    sessionLayout.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.white));
            }
        }
    }

    private void configureSelectionButton(Session session, boolean toDelete) {
        if (swipeLayout != null) {
            swipeLayout.setBackgroundResource(toDelete ? R.drawable.ripple_white_blue_smoke : R.drawable.ripple_white);
            BusMessage.post(toDelete ? BusMessageType.MARK_TO_DELETE : BusMessageType.UNMARK_TO_DELETE, session);
        }
    }

}
