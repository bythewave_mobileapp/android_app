# If you do not use RxJava:
-dontwarn rx.**

### OKHttp
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

### com.roughike.bottombar
-dontwarn com.roughike.bottombar.**

### Eventbus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

## Glide
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep class com.bumptech.glide.GeneratedAppGlideModuleImpl
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

### Fabric

-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

# GMaps
-keep class com.google.android.gms.maps.** { *; }
-keep interface com.google.android.gms.maps.** { *; }

### Firebase

-keepattributes Signature
-keepattributes *Annotation*

### Custom
-keep class com.bythewave.** {*;}
-keep interface com.bythewave.** {*;}
-keep enum com.bythewave.** {*;}

-keep class com.lapptelier.** {*;}
-keep enum com.lapptelier.** {*;}
-keep interface com.lapptelier.** {*;}
-dontwarn com.lapptelier.**
-keep class android.support.v7.widget.** {*;}

